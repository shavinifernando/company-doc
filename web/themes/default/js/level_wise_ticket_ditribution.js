// Chart
var ctx = document.getElementById("distribution_grf").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Lantam", "Asia", "East Asia"],
        datasets: [{
            label: 'L1',
            data: [12, 19, 3],
            backgroundColor: [
				'rgba(54, 162, 235, 0.2)',
				'rgba(54, 162, 235, 0.2)',
				'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
				'rgba(54, 162, 235, 1)',
				'rgba(54, 162, 235, 1)',
				'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
		}, {
            label: 'L2',
            data: [5, 2, 7],
            backgroundColor: [
				'rgba(255, 99, 132, 0.2)',
				'rgba(255, 99, 132, 0.2)',
				'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: [
				'rgba(255,99,132,1)',
				'rgba(255,99,132,1)',
				'rgba(255,99,132,1)'
            ],
            borderWidth: 1
		}, {
            label: 'L3',
            data: [10, 5, 8],
            backgroundColor: [
				'rgba(255, 206, 86, 0.2)',
				'rgba(255, 206, 86, 0.2)',
				'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: [
				'rgba(255, 206, 86, 1)',
				'rgba(255, 206, 86, 1)',
				'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
		}
	
		]
		


    },
    options: {
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Level'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Value'
                },
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});



jQuery(document).ready(function () {

	$('#select_month').datetimepicker({
		viewMode: 'months',
		format: 'YYYY/MMMM'
	});

});
