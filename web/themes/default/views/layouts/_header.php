<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Operations Assistance Portal | Wavenet</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<img src="<?=Yii::$app->view->theme->baseUrl?>/images/favicon.ico" type="image/x-icon">

<!-- jQuery 2.2.3 -->
<script src="js/files/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- <script src="js/extensions/jquery-1.8.3.min.js" type="text/javascript"></script>
<script>jQuery.noConflict(true);</script> -->
	
<!-- Jquery validation engine -->

<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="js/files/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="js/files/dist/css/font-awesome_4.5.0/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="js/files/dist/css/ionicons_2.0.1/ionicons.min.css">

<!-- DataTables -->
<link rel="stylesheet" href="js/files/plugins/datatables/dataTables.bootstrap.css">

<!-- Bootstrap datepicker -->
<link rel="stylesheet" href="js/files/plugins/datepicker/datepicker3.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="js/files/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- daterange picker -->
<link rel="stylesheet" href="js/files/plugins/daterangepicker/daterangepicker.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="js/files/plugins/iCheck/all.css">
<!-- Select2 -->
<link rel="stylesheet" href="js/files/plugins/select2/select2.min.css">

<!-- Theme style -->
<link rel="stylesheet" href="js/files/dist/css/AdminLTE.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
 folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="js/files/dist/css/skins/_all-skins.min.css">

<!-- Toaster Notifications -->
<link href="js/files/toaster/toastr.css" rel="stylesheet" type="text/css" />

<!-- DateTImePicker -->
<link href="js/files/plugins/datetimepicker/bootstrap-datetimepicker-new.css" rel="stylesheet" type="text/css" />

<!-- Toggle Button -->
<link href="js/files/plugins/toggle/bootstrap-toggle-2.2.2.min.css" rel="stylesheet" type="text/css" />

<!-- NEW Theme Style by DAN -->
<link rel="stylesheet" href="css/newtheme.css">


<!-- Bootstrap 3.3.6 -->
<script src="js/files/bootstrap/js/bootstrap.min.js"></script>

<!-- Select2 -->
<script src="js/files/plugins/select2/select2.full.min.js"></script>

<!-- date-range-picker -->
<script src="js/files/dist/js/moment-2.11.2.min.js"></script>
<script src="js/files/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="js/files/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap time picker -->
<script src="js/files/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<!-- DateTime picker -->
<script src="js/files/plugins/datetimepicker/bootstrap-datetimepicker-new.js"></script>

<!-- DataTables -->
<script src="js/files/plugins/datatables/jquery.dataTables.js"></script>
<script src="js/files/plugins/datatables/dataTables.bootstrap.js"></script>

<!-- iCheck 1.0.1 -->
<script src="js/files/plugins/iCheck/icheck.min.js"></script>

<!-- SlimScroll 1.3.0 -->
<script src="js/files/plugins/slimScroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="js/files/plugins/fastclick/fastclick.js"></script>

<!-- AdminLTE App -->
<script src="js/files/dist/js/app.min.js"></script>

<!-- ChartJS 2.7.0 -->
<script src="js/files/plugins/chartjs/2.7.0/Chart.min.js"></script>
<script src="js/files/plugins/chartjs/2.7.0/chartjs-plugin-annotation.min.js"></script>
<script src="js/files/plugins/chartjs/2.7.0/Chart.PieceLabel.js"></script>

<!-- Toaster Notifications -->
<script src="js/files/toaster/toastr.js"></script>

<!-- Toggle Button -->
<script src="js/files/plugins/toggle/bootstrap-toggle-2.2.2.min.js"></script>

<!-- Zing Chart -->
<script src="js/files/plugins/ZingChart/zingchart.min.js"></script>

<!-- Radial Chart -->
<script src="js/files/plugins/radial/radialIndicator.js"></script>
