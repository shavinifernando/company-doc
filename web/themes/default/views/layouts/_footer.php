<footer class="main-footer">
	<div class="pull-right hidden-xs"> Operations Assistance Portal 3.0.0.3 </div>
	<strong>Copyright &copy; 2019 <a href="http://www.globalwavenet.com/" target="_blank">Globalwavenet</a>.</strong> All rights reserved.
</footer>
