<?php
use \yii\bootstrap\Html;
use yii\helpers\Url;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Admin;

/*$name = trim($fullname);
$last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
$first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );*/	
?>

<header class="main-header">
	<!-- Logo -->
	<a href="#" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini">OpAs</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>OPERATION</b>assistance</span>
	</a>
	<nav class="navbar navbar-static-top">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<?php echo Nav::widget([
        'items' => [
           /* Yii::$app->user->isGuest ? (
				
				"<div class='pull-right'>
				<a href='#' class='btn btn-default btn-flat'>"
				. Html::beginForm(['/site/login'], 'post')
				. Html::submitButton(
					'Login',
					['class' => 'btn btn-link logout']
				)
				. Html::endForm()
				."</a>
			</div>"
            ) : (*/
				"<div class='navbar-custom-menu'>
				<ul class='nav navbar-nav'>
				<li>".
				//$username!=Yii::$app->params['adminEmail'] ?
				//(Html::a('Register new User', ['/site/register'])).
				"</li>
				<!-- User Account: style can be found in dropdown.less -->
				<li class='dropdown user user-menu'>
					<a href='#' class='dropdown-toggle' data-toggle='dropdown'>
						<img src='".Yii::$app->view->theme->baseUrl."/images/user2-160x160.jpg' class='user-image' alt='User Image'>
						<span class='hidden-xs'>". Yii::$app->user->identity->firstname ."</span>
					</a>
					<ul class='dropdown-menu'>
						<!-- User image -->
						<li class='user-header'>
							<img src='".Yii::$app->view->theme->baseUrl."/images/user2-160x160.jpg' class='img-circle' alt='User Image'>
							<p>"
							.Yii::$app->user->identity->username
							."	
							<small>User Type -".(Yii::$app->user->identity->role=='admin'? (' Admin'):(' User'))."</small>
							</p>
						</li>
						<!-- Menu Footer-->
						<li class='user-footer'>
							<!-- <div class='pull-left'>
							<a href='#' class='btn btn-default btn-flat'>Profile</a>
							</div> -->
							<div class='pull-right'>"
								. Html::beginForm(['/site/logout'], 'post')
								. Html::submitButton(
									'Logout',
									['class' => 'btn btn-primary btn-block btn-flat']
								)
								. Html::endForm()
								."
							</div>
						</li>
					</ul>
				</li>
			</ul>
			</div>"
                
           // )
        ],
    ]);
    ?>
			
	</nav>
</header>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<!-- <li class="header"><center>powered by Wavenet</center></li> -->
			<li class="treeview">
				<?= Html::a('<i class="fa fa-dashboard"></i><span>Dashboard</span>', ['dashboard/dashboard'], ['class' => 'nav-link']) ?>
			</li>
			<li class="treeview">
				<?php 
					//if($username==Yii::$app->params['adminEmail'])
					if(Yii::$app->user->identity->role=='admin')
						echo Html::a('<i class="fa fa-user-plus"></i><span>Register new User</span>', ['/site/register'], ['class' => 'nav-link'])
				?>
			</li>
			<li class="active treeview">
				<a href="#">
					<i class="fa fa- laptop"></i>
					<span>Performance</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li>
						
					
					</li>
					<li>
				<?php
					if(Yii::$app->user->identity->role=='admin')
						echo  Html::a('<i class="fa fa-circle-o"></i> File Import', ['import/import'], ['class' => 'nav-link'])
				?>				
	</li>
					
					<li class="active ">
						<a href="#"><i class="fa fa-circle-o"></i> Monthly Reports (Tickets)
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="active ">
								<?= Html::a('<i class="fa fa-circle-o"></i> Level wise', ['ticketinfo/leveldistribution']) ?>
							</li>
							<li class="active ">
								<?= Html::a('<i class="fa fa-circle-o"></i> Working Time Based', ['ticketinfo/workingtimedistribution']) ?>
							</li>
							<li class="active ">
								<?= Html::a('<i class="fa fa-circle-o"></i> Product &amp; Severity Based', ['ticketinfo/productdistribution']) ?>
						    </li>
						</ul>
					</li>
					<li>
						<a href="#"><i class="fa fa-circle-o"></i> Trend Graphs (Tickets)
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Severity wise', ['ticketinfo/severitytrend']) ?></li>
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Level wise', ['ticketinfo/leveltrend']) ?></li>
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Priority wise', ['ticketinfo/prioritytrend']) ?></li>
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Deployment wise', ['ticketinfo/deploymenttrend']) ?></li>
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Working Time Based', ['ticketinfo/workingtimetrend']) ?></li>
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Issue Category wise', ['ticketinfo/issuecategorytrend']) ?></li>
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Ticket Volume Trend', ['ticketinfo/ticketvolume']) ?></li>
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Level &amp; Severity Based', ['ticketinfo/levelseveritytrend']) ?></li>
						</ul>
					</li>
					<li>
						<a href="#"><i class="fa fa-circle-o"></i> Ticket Lifecycle
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Ticket Status Flow', ['tickettiming/ticketdistribution']) ?></li>
							<li><?= Html::a('<i class="fa fa-circle-o"></i> Ticket Flow', ['tickettiming/ticketflow']) ?></li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="active treeview">
				<a href="#">
					<i class="fa fa-laptop"></i>
					<span>Resolution and Activities</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="">
						<a href="#"><i class="fa fa-circle-o"></i> RCA
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="">
								<?= Html::a('<i class="fa fa-circle-o"></i> Operations', ['rca/rcaoperation']) ?>
							</li>
							<li class="">
							<a href="#"><i class="fa fa-circle-o"></i> Reports
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
							</a>
							<ul class="treeview-menu">
							<li class="">
								<?= Html::a('<i class="fa fa-circle-o"></i> RCA distribution', ['rca/rcadistribution']) ?>
							</li>
							</ul>
							</li>
						</ul>
					</li>
					<li class="">
						<a href="#"><i class="fa fa-circle-o"></i> MOP
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="">
								<?= Html::a('<i class="fa fa-circle-o"></i> Operations', ['mop/mopoperation']) ?>
							</li>
							<li class="">
								<a href="#"><i class="fa fa-circle-o"></i> Reports
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
							</a>
							<ul class="treeview-menu">
							<li class="">
								<?= Html::a('<i class="fa fa-circle-o"></i> MOP distribution', ['mop/mopdistribution']) ?>
							</li>
							</ul>
							</li>
						</ul>
					</li>
					<li class="">
						<a href="#"><i class="fa fa-circle-o"></i> Maintenance Activity 
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="">
								<?= Html::a('<i class="fa fa-circle-o"></i> Operations', ['createactivity/activityoperation']) ?>
							</li>
							<li class="">
								<a href="#"><i class="fa fa-circle-o"></i> Reports
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
							</a>
							<ul class="treeview-menu">
							<li class="">
								<?= Html::a('<i class="fa fa-circle-o"></i> Activity Creation', ['createactivity/activitycreation']) ?>
							</li>
							<li class="">
								<li class="">
									<a href="#"><i class="fa fa-circle-o"></i> Activity Completion
										<span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
									<ul class="treeview-menu">
									<li class="">
									<?= Html::a('<i class="fa fa-circle-o"></i> Completion Distribution', ['createactivity/completiondistribution']) ?>
									</li>
									<li class="">
									<?= Html::a('<i class="fa fa-circle-o"></i> Completion Trend', ['createactivity/completiontrend']) ?>
									</li>
									</ul>
								</li>
							</li>
							</ul>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="active treeview">
				<a href="#">
					<i class="fa fa-user"></i>
					<span>CDR Analyser</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="">
						<?= Html::a('<i class="fa fa-circle-o"></i> Analyser', ['cdr/cdranalyzer']) ?>	
						
					</li>
					<li class="">
						<?= Html::a('<i class="fa fa-circle-o"></i> Configuration', ['cdr/cdrconfiguration']) ?>
					</li>
					
					
				</ul>
			</li>
			<li class="active treeview">
				<a href="#">
				<i class="fa fa-user"></i>
					<span>Flow Diagrams</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="">
						<?= Html::a('<i class="fa fa-circle-o"></i> USSD', ['diagram/ussd']) ?>	
					</li>					
				</ul>
			</li> 

			</ul>
	</section>
	<!-- /.sidebar -->
</aside>
