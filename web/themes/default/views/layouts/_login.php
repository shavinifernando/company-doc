<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title><?= Html::encode($this->title) ?></title>
     <!-- Tell the browser to be responsive to screen width -->
     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>

<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href="login.php"><b>OPERATIONS</b>assistance</a>
  </div>
  
  
	<!-- /.login-logo -->
	<div class="login-box-body">
  
        <?= $content ?>
	</div>
	<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<div class="lockscreen-footer text-center">
	Copyright &copy; 2019 <b><a href="https://www.globalwavenet.com" class="text-black">Wavenet International</a></b> All rights reserved
</div>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
