<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use  yii\web\Session;
use yii\web\Cookie;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ForgotForm;
use app\models\RegisterForm;
use app\models\User;
use app\models\ResetPasswordForm;
use app\models\ContactForm;
use app\models\Backendusers;


class SiteController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
           /* 'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    'allow'=>true,
                    'roles'=>['@'],
                ],
            ],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    /*public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }*/

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $id=Yii::$app->user->identity->id;
        //$user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
        if ($id==NULL) {
        //if (!$session->isActive) {  

            return $this->redirect(['site/login']); 
        }
        $session['user_id'] = Yii::$app->user->identity->id;
        $session['user_name'] =Yii::$app->user->identity->username;
        $session['firstname'] =Yii::$app->user->identity->firstname;
        $session['cdr_val'] ='';
	return $this->render('index');
    }



    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $cookies = Yii::$app->response->cookies; 
        $session = Yii::$app->session;
        //$cookie=Yii::$app->$_COOKIE;
       // $session->open();
        //print_r(Yii::$app->params['adminEmail']);
        $user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
        //$user_id_1 = isset($_COOKIE['user_id']) ? $_COOKIE['user_id'] : null;
        //if(!Yii::$app->user->isGuest){
        if ($user_id!=NULL || !Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = '_login';
       // $model = new User();
        //print_r($cookies['prajwal@globalwavenet.com']->value);
        //print_r(Yii::$app->user->identity->username);
        //if ($model->validateAuthKey($cookies['']) && $model->login()) {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $session['user_id'] = Yii::$app->user->identity->id;
            $session['user_name'] =Yii::$app->user->identity->username;
            $session['firstname'] =Yii::$app->user->identity->firstname;
            return $this->goBack(); 
        } 
       
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionRegister()
    {
        $session = Yii::$app->session;
        $username = $session['user_name'];

        if(Yii::$app->user->identity->role!='admin'){
        //if($username!=Yii::$app->params['adminEmail']){
            return $this->goBack();
        }
        $this->layout = '_login';
        $model   = new User();
        if ($model->load(Yii::$app->request->post())){
            $newId = User::find()->max('id') + 1;
            $model->id=$newId;
            $model->password='test123';
            $model->status='Pending';
            $model->accessToken='';
            $model->password_reset_token='';
            $hash = md5($model->password);
            $model->password=$hash;
        
            if($model->save()) {
                
                
                if ($model->sendEmail($model->username)) {
                    Yii::$app->session->setFlash('success', '<B>New User Created:</B> '
                    .'<ul><li>First Name: <i>'.$model->firstname.'</i></li>' 
                    .'<li>Last Name: <i>'.$model->lastname.'</i></li>' 
                    .'<li>Username:  <i>'.$model->username.'</i></li>'
                    .'<li>Department:  <i>'.$model->department.'</i></li></ul>'
                    .'<p><strong>[A mail has been sent to the username] </strong></p>');
                    $model->id='';
                    $model->username='';
                    $model->password='';
                    $model->firstname='';
                    $model->lastname='';
                    $model->department='';
                    return $this->render('register', [
                        'model' => $model,
                    ]);
                    
                } else {
                    Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
                }
                
                return $this->render('register', [
                    'model' => $model,
                ]);
            }
        }
        $model->id='';
        $model->username='';
        $model->password='';
        $model->firstname='';
        $model->lastname='';
        $model->department='';
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->close();
        $session->destroy();
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionForgot()
    {
        $this->layout = 'popup';
        //return $this->render('forgot');

        $model = new ForgotForm();
        if ($model->load(Yii::$app->request->post()) ) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.'.
                    '<p>Reset Password link sent to '.$model->username.'</p>');
                return $this->render('forgot', [
                    'model' => $model,
                ]);
                
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
 
        }
        
        return $this->render('forgot', [
            'model' => $model,
        ]);
    }

    public function actionAfterforgot()
    {
        //$this->layout = 'popup';
        return $this->render('afterforgot');
    }

  /*public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
 
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
 
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
 
        }
 
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }*/
 
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetpassword($token)
    {
        $this->layout = '_login';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
 //
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }
 
        return $this->render('resetpassword', [
            'model' => $model,
            ]);
      }
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    /*public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }*/

    /**
     * Displays about page.
     *
     * @return string
     */
    /*public function actionAbout()
    {
        return $this->render('about');
    }*/
    //Cookies
    /*public function actionReadCookies() { 
        // get cookies from the "request" component 
        $cookies = Yii::$app->request->cookies; 
        // get the "language" cookie value 
        // if the cookie does not exist, return "ru" as the default value 
        $language = $cookies->getValue('language', 'ru'); 
        // an alternative way of getting the "language" cookie value 
        if (($cookie = $cookies->get('language')) !== null) { 
           $language = $cookie->value; 
        } 
        // you may also use $cookies like an array 
        if (isset($cookies['language'])) { 
           $language = $cookies['language']->value; 
        } 
        // check if there is a "language" cookie 
        if ($cookies->has('language')) echo "Current language: $language"; 
     } 

     public function actionSendCookies() { 
        // get cookies from the "response" component 
        $cookies = Yii::$app->response->cookies; 
        // add a new cookie to the response to be sent 
        $cookies->add(new \yii\web\Cookie([ 
           'name' => 'language', 
           'value' => 'ru-RU', 
        ])); 
        $cookies->add(new \yii\web\Cookie([
           'name' => 'username', 
           'value' => 'John', 
        ])); 
        $cookies->add(new \yii\web\Cookie([ 
           'name' => 'country', 
           'value' => 'USA', 
        ])); 
     } */
     /*public function beforeSave($insert)
    {
        $model   = new User();
        if (parent::beforeSave($insert)) {
            if ($model->isNewRecord) {
                $model->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }*/
}
