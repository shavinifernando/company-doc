<?php
namespace app\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\helpers\BaseArrayHelper;
use app\controllers\ArrayHelper;
use yii\helpers\StringHelper;
use app\components\TicketLifeCycle;
use yii\web\NotFoundHttpException;
//use yii\console\Controller;
use yii\console\ExitCode;
use yii\web\Session;
use yii\db\Command;
use app\models\Ussd;
use app\models\Smsc;
use yii\web\UploadedFile;
use phpDocumentor\Reflection\Types\Boolean;

class DiagramController extends Controller
{
    public function behaviors()
    {
        return [];
    }
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }
    public function actionUssd(){
        $model= new Ussd();
		$session = Yii::$app->session;
		$session['ussd']='';
		$sesid3=array();$final1=array();$final2=array();$seszero=array();
		if (Yii::$app->request->post()) {
			if(isset($_POST['Ussd'])){
				$model->load(Yii::$app->request->post());
				
				if ($model->Ussd != '') {
					$ea = explode(PHP_EOL, $model->Ussd);
					foreach ($ea as $key) {
                        if ($session['ussd']==''){
                            $session['ussd'] .= $key;

                        }
                        else{
                            $session['ussd'] .= '\r\n'.$key;  
                        }
					}
				$key = '';
				$model->Ussd = '';
				}
				$final1= explode('\r\n', $session['ussd']) ;
				foreach ($final1 as $key) {
					$dew=explode(',',$key);
					$final2[] = $dew;
				}
				$sesid=array();
				foreach($final2 as $key){
					if(isset($key['19'])){
						if($key['19']=='0'){
							$seszero[]=$key;
						}
						else{
							$sesid[]=$key['19'];
						}
					}else{
						Yii::$app->session->setFlash('error', 'Check the Input again');
						return $this->render('ussd', ['model' => $model,'sesid3'=>$sesid3]);
					}
				}
					
				$sesid2=array_unique($sesid);
				foreach($sesid2 as $key){
					$sesid3[]=$key;
				}
				
				
			}
			else{
				Yii::$app->session->setFlash('error', 'Check the input again.');
			}
			
			
		}
		
	
        return $this->render('ussd', ['model' => $model,'sesid3'=>$sesid3,'final1'=>$final1,'final2'=>$final2,'seszero'=>$seszero]);
    }
	public function actionDisplay($id){
		$session = Yii::$app->session;
        $this->layout = 'popup';
		$model= new Ussd();
		
		return $this->render('display', ['model' => $model,'id'=>$id]);
	}
    public function actionSmsc(){
        $model= new Smsc();
		$session=Yii::$app->session;
		
        return $this->render('smsc', ['model' => $model]);
    }
}
?>
