<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\NotFoundHttpException;
use app\models\Rca;
use app\models\RcaSearch;
use app\controllers\AppController;
use app\models\createform;
use yii\helpers\Url;
$session = Yii::$app->session;
$username = $session['user_name'];
$firstname = $session['first_name'];

class RcaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
   public function behaviors()
    {
        return [
            
    
        ];
    }

    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        
        return parent::beforeAction($action);
    }

    public function actionRcaoperation()
    {
        
        $searchModel = new RcaSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('rcaoperation', ['searchModel'=>$searchModel,'dataProvider' => $dataProvider]);


    }

    public function actionCreate(){

        $this->layout = 'popup';

        $model = new Rca();
        
        if ($model->load(Yii::$app->request->post()) ) {
            //$model->RCACreator=Yii::$app->user->identity->id;
            //if( $model->save()){
                $session = Yii::$app->session;
                $userid = $session['user_id'];
                $model->RCACreator=(string)$userid;
                $model->save();
                Yii::$app->readWriteLog->readWriteLog("app\models\Rca RCA Record created successfully",['TicketId'=>$model->ticketId,'RCA Name'=>$model->RCAName,'RCA Creator'=>$model->creatorsName->firstname],2);

                return $this->render('createform',['model' => $model]);
           // }
	   

        } else{

            return $this->render('createform',['model' => $model]);
        }
    }

    public function actionView($id)
    {
        $this->layout = 'popup';

        $model = Rca::findOne($id);

        return $this->render('viewform',['model'=> $model]);
    
    }

    public function actionUpdate($id){

        $this->layout = 'popup';

        $model = Rca::findOne($id);

    if($model->load(Yii::$app->request->post()) && $model->save()){

	Yii::$app->readWriteLog->readWriteLog("app\models\Rca RCA Record updated successfully",['TicketId'=>$model->ticketId,'RCA Name'=>$model->RCAName,'RCA Creator'=>$model->creatorsName->firstname],2);

        return $this->render('updateform',['model'=>$model]);

    }
    else{
        return $this->render('updateform',['model'=>$model]);
    }

    }

   public function actionDelete($id)
    {
        
        $model = Rca::findOne($id);

        if ($model->delete()){

	Yii::$app->readWriteLog->readWriteLog("app\models\Rca RCA Record deleted",['Ticket ID'=>$model->ticketId,'RCA Name'=>$model->RCAName],2);

        return $this->redirect(['rcaoperation']);

        }

        
    }

    public function actionRcadistribution()
    {
  
        $model = new rca();

        return $this->render('rcadistribution', ['model'=>$model]);
    }
       
    public function actionShowrcadistribution()
    {

        $model = new rca();
        
        $model->scenario = rca::SCENARIO_DIST_RCA;
      
         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'startDate');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'endDate');
            $filter = \yii\helpers\ArrayHelper::getValue($_POST, 'filter');

            /*echo $smonth;
            echo $emonth;
            echo $filter;
            exit;*/
            $sql="SELECT ";

            /*$sql="SELECT DATE_FORMAT(openDate,'%Y-%M') AS Month,COUNT(rcaId) AS Count from rca WHERE DATE_FORMAT(openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month";

            echo $sql;
            exit;*/

            if ($filter=='')
                    $sql .= "DATE_FORMAT(openDate,'%Y-%M') AS Category,COUNT(rcaId) AS Count from rca WHERE DATE_FORMAT(openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Category";

            if ($filter=='Month')
                    $sql .= "DATE_FORMAT(openDate,'%Y-%M') AS Category,COUNT(rcaId) AS Count from rca WHERE DATE_FORMAT(openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Category";

            if ($filter=='operator')
                    $sql .= "o.operatorName AS Category,COUNT(r.rcaId) AS Count from rca r, operator o WHERE DATE_FORMAT(r.openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and r.operator = o.operatorId GROUP BY r.operator";

	    if ($filter=='RCACreator')
                    //Pxx $sql .= "c.createrName AS Category,COUNT(r.rcaId) AS Count from rca r, creator c WHERE DATE_FORMAT(r.openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and r.RCACreator = c.createrId GROUP BY r.RCACreator";
                     $sql .= "c.firstname AS Category,COUNT(r.rcaId) AS Count from rca r, user c WHERE DATE_FORMAT(r.openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and r.RCACreator = c.id GROUP BY r.RCACreator";
            $connection = Yii::$app->getDb();
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();
                    foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

    
}


