<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\helpers\BaseArrayHelper;
use app\controllers\ArrayHelper;
use yii\helpers\StringHelper;
use app\components\TicketLifeCycle;
use yii\web\NotFoundHttpException;
//use yii\console\Controller;
use yii\console\ExitCode;
use yii\web\Session;
use yii\db\Command;
use yii\web\UploadedFile;
use app\models\CdrAllinone;
use app\models\CdrDivision;
use app\models\Cdranalyzer;
use app\models\Cdrconfiguration;
use app\models\Cdr_nfe;
use phpDocumentor\Reflection\Types\Boolean;

class CdrController extends Controller
{
    public function behaviors()
    {
        return [];
    }
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }
    public function actionCompare(){
	$model= new CdrAllinone();
	$session= Yii::$app->session;
	$this->layout = 'popup';
        $array = explode('\r\n', $session['cdr_val']);
        $printlabel = array();
        $valuex = array();
        $valuex1 = array();
	$valuex2 = array();
	 $dew=explode(',',($_GET['items']));
        $ddd=array();
        array_pop($dew);
        if(sizeof($dew)==2){
            foreach($dew as $key){
                $ddd[]=$array[(integer)$key];
            }
            $cdr1=$ddd[0];
            $cdr=$ddd[1];

        $deployment = $session['deployment'];
        $cdrtype= $session['cdrtype'];
        //$cdr= '6, 2019/09/17 12:49:45, Cellcard4U, Cellcard4U, 85592744520, 85592744520, , 2019/09/16 19:40:07, 0, 0, 45, 8, 1509939638441721863, 140, 2019/09/17 19:40:07, 0, 8, 5, 0, 1, 0, 0, 0, , , , , , , , , 81, 4, 1, 0, 0, 3, 27, 0, , 1, 1, , , 0, smsevolving, 0, , 5, 24,'; 
        //$cdr1='9, 2019/09/17 12:49:45, 85511360964, 85511360964, 123456, 123456, , 2019/09/17 12:49:29, 85512000024, 456010133674683, , 5, 1510192615767646211, 140, 2019/09/19 12:49:30, 0, 8, 1, 1, 0, 1, 85512000101, 85512000024, , , , , , , , , 197, 2, 1, , 0, 3, 32, 5, , 1, 1, , 0, , , 0, , 3, 0,';
        $cdr_array=explode(",", $cdr);
        $cdr_array1=explode(",", $cdr1);
        

        if (sizeof($cdr_array)==1) {
            $cdr_array=explode("|", $cdr);
        }
        
        if (sizeof($cdr_array1)==1) {
            $cdr_array1=explode("|", $cdr1);
        }
        

        $conn = mysqli_connect('localhost', 'root', 'Ramal@123', 'ticket_system');

        if (!$conn) {
            die("cannot connect");
        }

        $label = "SELECT * FROM `cdr_division` WHERE deployment='$deployment' AND cdrtype='$cdrtype' AND mode='0'";
        $refer = "SELECT * FROM `cdr_division` WHERE deployment='$deployment' AND cdrtype='$cdrtype' AND mode='1'";
        $tableno = "SELECT * FROM `cdr_division` WHERE deployment='$deployment' AND cdrtype='$cdrtype' AND mode='2'";

        $connection = Yii::$app->getDb();

        try {
            $result1 = $connection->createCommand($label)->queryAll();
            $result2 = $connection->createCommand($refer)->queryAll();
            $result3 = $connection->createCommand($tableno)->queryAll();

            $label_array = array();
            $refer_array = array();
            $tableno_array = array();

            foreach ($result1 as $key) {
                $label_array[] = $key;
            }

            foreach ($result2 as $key) {
                $refer_array[] = $key;
            }

            foreach ($result3 as $key) {
                $tableno_array[] = $key;
            }
        } catch (Exception $e) {
        }
        //$data=array_reverse($data);

        //$data=sort($data);
        $label_array1 = array();
        for ($i = 1; $i < 72; $i++) {
            if (isset($label_array['0']["f$i"])) {
                if ($label_array['0']["f$i"] != "null") {
                    array_push($label_array1, $label_array['0']["f$i"]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Check the Deployment and CDR types again');
            }
        }

        $refer_array1 = array();

        for ($i = 1; $i < 72; $i++) {
            if (isset($label_array['0']["f$i"])) {
                if ($refer_array['0']["f$i"] != "null") {
                    array_push($refer_array1, $refer_array['0']["f$i"]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Check the Deployment and CDR types again');
            }
        }
        $r = sizeof($label_array1);
        $tableno_array1 = array();
        for ($i = 1; $i < $r + 1; $i++) {
            array_push($tableno_array1, $tableno_array['0']["f$i"]);
        }

        if ($cdr_array["0"]=='3') {
            $tableno_array1["35"]='16';
        }
        else if ($cdr_array1["0"]=='3') {
            $tableno_array1["35"]='16';
        }
        $check=sizeof($cdr_array)-$r;
        $check1=sizeof($cdr_array1)-$r;
        if ($check==0 || $check==1) {
            for ($i = 0; $i < $r; $i++) {
                array_push($printlabel, $label_array1["$i"]);
                //print_r($label_array["$i"]);
                //print_r(' : ');
                if ($refer_array1["$i"] == "1") {
                    array_push($valuex, $cdr_array["$i"]);
                    //print_r($cdr_array["$i"]);
                }
                if ($refer_array1["$i"] == "0") {
                    if ($cdr_array["$i"]==' undefined') {
                        array_push($valuex, $cdr_array["$i"]);
                    } elseif ($cdr_array["$i"] != " ") {
                        $value = (int) $cdr_array["$i"];
                        //print_r($value);
                        $code = (int) $tableno_array1["$i"];
                        if ($code == 100 || $code == 103) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NET_des FROM `cdr_nfe` WHERE NETid=$code AND NET=$value"));
                            array_push($valuex, $cdr_array["$i"] . '-' . $ref['NET_des']);
                            //print_r($ref['NET_des']);
                            $NETid = $code;
                            $NET = $value;
                        } elseif ($code == 101) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NER_des FROM `cdr_nfe` WHERE NERid=$code AND NER=$value AND NETid=$NETid AND NET=$NET"));
                            array_push($valuex, $cdr_array["$i"] . '-' . $ref['NER_des']);
                            //print_r($ref['NER_des']);
                            $NERid = 101;
                            $NER = $value;
                        } elseif ($code == 102) {
                            if (isset($NER)) {
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex, $cdr_array["$i"] . '-' . $ref['NFC_des']);
                            //print_r($ref['NFC_des']);
                            } else {
                                $NER = 200;
                                $NERid = 101;
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex, $cdr_array["$i"] . '-' . $ref['NFC_des']);
                                //print_r($ref['NFC_des']);
                            }
                        } else {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT event FROM `cdr_allinone` WHERE Code=$code AND Value=$value"));
                            array_push($valuex, $cdr_array["$i"] . '-' . $ref['event']);
                            //print_r($ref['event']);
                        }
                    } else {
                        array_push($valuex, $cdr_array["$i"]);
                    }
                }
            }
            $printlabel;
            $valuex;
            $valuex1;
        
        }
        if ($check1==0 || $check1==1) {
            for ($i = 0; $i < $r; $i++) {
                array_push($printlabel, $label_array1["$i"]);
                //print_r($label_array["$i"]);
                //print_r(' : ');
                if ($refer_array1["$i"] == "1") {
                    array_push($valuex1, $cdr_array1["$i"]);
                    //print_r($cdr_array["$i"]);
                }
                if ($refer_array1["$i"] == "0") {
                    if ($cdr_array1["$i"]==' undefined') {
                        array_push($valuex1, $cdr_array1["$i"]);
                    } elseif ($cdr_array1["$i"] != " ") {
                        $value = (int) $cdr_array1["$i"];
                        //print_r($value);
                        $code = (int) $tableno_array1["$i"];
                        if ($code == 100 || $code == 103) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NET_des FROM `cdr_nfe` WHERE NETid=$code AND NET=$value"));
                            array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['NET_des']);
                            //print_r($ref['NET_des']);
                            $NETid = $code;
                            $NET = $value;
                        } elseif ($code == 101) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NER_des FROM `cdr_nfe` WHERE NERid=$code AND NER=$value AND NETid=$NETid AND NET=$NET"));
                            array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['NER_des']);
                            //print_r($ref['NER_des']);
                            $NERid = 101;
                            $NER = $value;
                        } elseif ($code == 102) {
                            if (isset($NER)) {
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['NFC_des']);
                            //print_r($ref['NFC_des']);
                            } else {
                                $NER = 200;
                                $NERid = 101;
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['NFC_des']);
                                //print_r($ref['NFC_des']);
                            }
                        } else {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT event FROM `cdr_allinone` WHERE Code=$code AND Value=$value"));
                            array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['event']);
                            //print_r($ref['event']);
                        }
                    } else {
                        array_push($valuex1, $cdr_array1["$i"]);
                    }
                }
            }
            $printlabel;
            $valuex;
            $valuex1;
        } 
        else {
            Yii::$app->session->setFlash('error', 'WRONG CDR TYPE');
            $printlabel=array('');
            array_push($valuex," ");
            array_push($valuex1," ");
        }
        mysqli_close($conn);
        
        }
        else if(sizeof($dew)==3){
            foreach($dew as $key){
                $ddd[]=$array[(integer)$key];
            }
            $cdr1=$ddd[0];
            $cdr=$ddd[1];
            $cdr2=$ddd[2];

        $deployment = $session['deployment'];
        $cdrtype= $session['cdrtype'];
        //$cdr= '6, 2019/09/17 12:49:45, Cellcard4U, Cellcard4U, 85592744520, 85592744520, , 2019/09/16 19:40:07, 0, 0, 45, 8, 1509939638441721863, 140, 2019/09/17 19:40:07, 0, 8, 5, 0, 1, 0, 0, 0, , , , , , , , , 81, 4, 1, 0, 0, 3, 27, 0, , 1, 1, , , 0, smsevolving, 0, , 5, 24,'; 
        //$cdr1='9, 2019/09/17 12:49:45, 85511360964, 85511360964, 123456, 123456, , 2019/09/17 12:49:29, 85512000024, 456010133674683, , 5, 1510192615767646211, 140, 2019/09/19 12:49:30, 0, 8, 1, 1, 0, 1, 85512000101, 85512000024, , , , , , , , , 197, 2, 1, , 0, 3, 32, 5, , 1, 1, , 0, , , 0, , 3, 0,';
        $cdr_array=explode(",", $cdr);
        $cdr_array1=explode(",", $cdr1);
        $cdr_array2=explode(",",$cdr2);

        if (sizeof($cdr_array)==1) {
            $cdr_array=explode("|", $cdr);
        }
        
        if (sizeof($cdr_array1)==1) {
            $cdr_array1=explode("|", $cdr1);
        }
        if (sizeof($cdr_array2)==1) {
            $cdr_array2=explode("|", $cdr2);
        }
        

        $conn = mysqli_connect('localhost', 'root', 'Ramal@123', 'ticket_system');

        if (!$conn) {
            die("cannot connect");
        }

        $label = "SELECT * FROM `cdr_division` WHERE deployment='$deployment' AND cdrtype='$cdrtype' AND mode='0'";
        $refer = "SELECT * FROM `cdr_division` WHERE deployment='$deployment' AND cdrtype='$cdrtype' AND mode='1'";
        $tableno = "SELECT * FROM `cdr_division` WHERE deployment='$deployment' AND cdrtype='$cdrtype' AND mode='2'";

        $connection = Yii::$app->getDb();

        try {
            $result1 = $connection->createCommand($label)->queryAll();
            $result2 = $connection->createCommand($refer)->queryAll();
            $result3 = $connection->createCommand($tableno)->queryAll();

            $label_array = array();
            $refer_array = array();
            $tableno_array = array();

            foreach ($result1 as $key) {
                $label_array[] = $key;
            }

            foreach ($result2 as $key) {
                $refer_array[] = $key;
            }

            foreach ($result3 as $key) {
                $tableno_array[] = $key;
            }
        } catch (Exception $e) {
        }
        //$data=array_reverse($data);

        //$data=sort($data);
        $label_array1 = array();
        for ($i = 1; $i < 72; $i++) {
            if (isset($label_array['0']["f$i"])) {
                if ($label_array['0']["f$i"] != "null") {
                    array_push($label_array1, $label_array['0']["f$i"]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Check the Deployment and CDR types again');
            }
        }

        $refer_array1 = array();

        for ($i = 1; $i < 72; $i++) {
            if (isset($label_array['0']["f$i"])) {
                if ($refer_array['0']["f$i"] != "null") {
                    array_push($refer_array1, $refer_array['0']["f$i"]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Check the Deployment and CDR types again');
            }
        }
        $r = sizeof($label_array1);
        $tableno_array1 = array();
        for ($i = 1; $i < $r + 1; $i++) {
            array_push($tableno_array1, $tableno_array['0']["f$i"]);
        }

        if ($cdr_array["0"]=='3') {
            $tableno_array1["35"]='16';
        }
        else if ($cdr_array1["0"]=='3') {
            $tableno_array1["35"]='16';
        }
        else if ($cdr_array2["0"]=='3') {
            $tableno_array2["35"]='16';
        }
        $check=sizeof($cdr_array)-$r;
        $check1=sizeof($cdr_array1)-$r;
        $check2=sizeof($cdr_array2)-$r;
        if ($check==0 || $check==1) {
            for ($i = 0; $i < $r; $i++) {
                array_push($printlabel, $label_array1["$i"]);
                //print_r($label_array["$i"]);
                //print_r(' : ');
                if ($refer_array1["$i"] == "1") {
                    array_push($valuex, $cdr_array["$i"]);
                    //print_r($cdr_array["$i"]);
                }
                if ($refer_array1["$i"] == "0") {
                    if ($cdr_array["$i"]==' undefined') {
                        array_push($valuex, $cdr_array["$i"]);
                    } elseif ($cdr_array["$i"] != " ") {
                        $value = (int) $cdr_array["$i"];
                        //print_r($value);
                        $code = (int) $tableno_array1["$i"];
                        if ($code == 100 || $code == 103) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NET_des FROM `cdr_nfe` WHERE NETid=$code AND NET=$value"));
                            array_push($valuex, $cdr_array["$i"] . '-' . $ref['NET_des']);
                            //print_r($ref['NET_des']);
                            $NETid = $code;
                            $NET = $value;
                        } elseif ($code == 101) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NER_des FROM `cdr_nfe` WHERE NERid=$code AND NER=$value AND NETid=$NETid AND NET=$NET"));
                            array_push($valuex, $cdr_array["$i"] . '-' . $ref['NER_des']);
                            //print_r($ref['NER_des']);
                            $NERid = 101;
                            $NER = $value;
                        } elseif ($code == 102) {
                            if (isset($NER)) {
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex, $cdr_array["$i"] . '-' . $ref['NFC_des']);
                            //print_r($ref['NFC_des']);
                            } else {
                                $NER = 200;
                                $NERid = 101;
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex, $cdr_array["$i"] . '-' . $ref['NFC_des']);
                                //print_r($ref['NFC_des']);
                            }
                        } else {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT event FROM `cdr_allinone` WHERE Code=$code AND Value=$value"));
                            array_push($valuex, $cdr_array["$i"] . '-' . $ref['event']);
                            //print_r($ref['event']);
                        }
                    } else {
                        array_push($valuex, $cdr_array["$i"]);
                    }
                }
            }
            $printlabel;
            $valuex;
            $valuex1;
            $valuex2;
        }
        if ($check1==0 || $check1==1) {
            for ($i = 0; $i < $r; $i++) {
                array_push($printlabel, $label_array1["$i"]);
                //print_r($label_array["$i"]);
                //print_r(' : ');
                if ($refer_array1["$i"] == "1") {
                    array_push($valuex1, $cdr_array1["$i"]);
                    //print_r($cdr_array["$i"]);
                }
                if ($refer_array1["$i"] == "0") {
                    if ($cdr_array1["$i"]==' undefined') {
                        array_push($valuex1, $cdr_array1["$i"]);
                    } elseif ($cdr_array1["$i"] != " ") {
                        $value = (int) $cdr_array1["$i"];
                        //print_r($value);
                        $code = (int) $tableno_array1["$i"];
                        if ($code == 100 || $code == 103) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NET_des FROM `cdr_nfe` WHERE NETid=$code AND NET=$value"));
                            array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['NET_des']);
                            //print_r($ref['NET_des']);
                            $NETid = $code;
                            $NET = $value;
                        } elseif ($code == 101) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NER_des FROM `cdr_nfe` WHERE NERid=$code AND NER=$value AND NETid=$NETid AND NET=$NET"));
                            array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['NER_des']);
                            //print_r($ref['NER_des']);
                            $NERid = 101;
                            $NER = $value;
                        } elseif ($code == 102) {
                            if (isset($NER)) {
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['NFC_des']);
                            //print_r($ref['NFC_des']);
                            } else {
                                $NER = 200;
                                $NERid = 101;
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['NFC_des']);
                                //print_r($ref['NFC_des']);
                            }
                        } else {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT event FROM `cdr_allinone` WHERE Code=$code AND Value=$value"));
                            array_push($valuex1, $cdr_array1["$i"] . '-' . $ref['event']);
                            //print_r($ref['event']);
                        }
                    } else {
                        array_push($valuex1, $cdr_array1["$i"]);
                    }
                }
            }
            $printlabel;
            $valuex;
            $valuex1;
            $valuex2;
        } 
        if ($check2==0 || $check2==1) {
            for ($i = 0; $i < $r; $i++) {
                array_push($printlabel, $label_array1["$i"]);
                //print_r($label_array["$i"]);
                //print_r(' : ');
                if ($refer_array1["$i"] == "1") {
                    array_push($valuex2, $cdr_array2["$i"]);
                    //print_r($cdr_array["$i"]);
                }
                if ($refer_array1["$i"] == "0") {
                    if ($cdr_array2["$i"]==' undefined') {
                        array_push($valuex2, $cdr_array2["$i"]);
                    } elseif ($cdr_array2["$i"] != " ") {
                        $value = (int) $cdr_array2["$i"];
                        //print_r($value);
                        $code = (int) $tableno_array1["$i"];
                        if ($code == 100 || $code == 103) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NET_des FROM `cdr_nfe` WHERE NETid=$code AND NET=$value"));
                            array_push($valuex2, $cdr_array2["$i"] . '-' . $ref['NET_des']);
                            //print_r($ref['NET_des']);
                            $NETid = $code;
                            $NET = $value;
                        } elseif ($code == 101) {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NER_des FROM `cdr_nfe` WHERE NERid=$code AND NER=$value AND NETid=$NETid AND NET=$NET"));
                            array_push($valuex2, $cdr_array2["$i"] . '-' . $ref['NER_des']);
                            //print_r($ref['NER_des']);
                            $NERid = 101;
                            $NER = $value;
                        } elseif ($code == 102) {
                            if (isset($NER)) {
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex2, $cdr_array2["$i"] . '-' . $ref['NFC_des']);
                            //print_r($ref['NFC_des']);
                            } else {
                                $NER = 200;
                                $NERid = 101;
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex2, $cdr_array2["$i"] . '-' . $ref['NFC_des']);
                                //print_r($ref['NFC_des']);
                            }
                        } else {
                            $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT event FROM `cdr_allinone` WHERE Code=$code AND Value=$value"));
                            array_push($valuex2, $cdr_array2["$i"] . '-' . $ref['event']);
                            //print_r($ref['event']);
                        }
                    } else {
                        array_push($valuex2, $cdr_array2["$i"]);
                    }
                }
            }
            $printlabel;
            $valuex;
            $valuex1;
            $valuex2;
        }
        else {
            Yii::$app->session->setFlash('error', 'WRONG CDR TYPE');
            $printlabel=array('');
            array_push($valuex," ");
            array_push($valuex1," ");
            array_push($valuex2," ");
        }
        mysqli_close($conn);
        
        } 
        else{
            Yii::$app->session->setFlash('error', "CHECK ONLY TWO CDR'S AT ONCE");
           
        }
        
        return $this->render('compare', ['model' => $model, 'printlabel' => $printlabel, 'valuex' => $valuex, 'valuex1'=> $valuex1, 'valuex2'=> $valuex2]);

    }
		 
       
    public function actionCdranalyzer()
    {
        $model = new Cdranalyzer();
        //$model1=new CdrAllinone();
        //$model2=new Cdr_nfe();
        
        $session = Yii::$app->session;
	$session->open();
       	$model->deployment=$session['deployment'];
       	$model->cdrtype=$session['cdrtype'];
	
         
        if (Yii::$app->request->post()) {
	    if (isset($_POST['Reset'])){
                unset($session['deployment']);
                unset($session['cdrtype']);
                unset($session['cdr_val']);
		//print_r("post reset working");
            }
            else if(isset($_POST['Insert'])){
     	//	$this->refresh();          
                $model->load(Yii::$app->request->post());
		$model->file = UploadedFile::getInstance($model, 'file');           
		 if ($model->cdra!='' && $model->file !=null){
		Yii::$app->session->setFlash('msg', '
         <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <strong> Only one input method is allowed!   </strong>  </div>');
                return $this->refresh() ;
                }

                $session['deployment'] = $model->deployment;
                $session['cdrtype'] = $model->cdrtype;
          
		$sqlsh= "SELECT * FROM cdr_division WHERE deployment='$model->deployment' AND cdrtype='$model->cdrtype'";
		$connection = Yii::$app->getDb();
                $done = $connection->createCommand($sqlsh)->queryAll();
                $dawn=array();
                foreach ($done as $key) {
                    $dawn[] = $key;
                }
                if(sizeof($dawn)==0){
                    unset($session['deployment']);
                    unset($session['cdrtype']);
		    
		     Yii::$app->session->setFlash('msg', '
    	 <div class="alert alert-warning alert-dismissable">
     	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
     	<strong> CDR Type does not exist!   </strong> Check whether the deployment has the selected CDR type. </div>'
  	);
                    //$this->refresh();
                    return $this->render('cdranalyzer', ['model' => $model]);
                    //print_r(sizeof($dawn));
                    //exit;  
                }
  
       	    
 
            
            if ($model->cdra != '') {
                $ea = explode(PHP_EOL, $model->cdra);
                foreach ($ea as $key) {
			if ($session['cdr_val']==''){
                            $session['cdr_val'] .= $key;
 
                        }
                        else{
                            $session['cdr_val'] .= '\r\n'.$key;  
                        }
                   // $session['cdr_val'] .= $key . '\r\n';
                }
		//print_r('Hello');
                $key = '';
                $model->cdra = '';
 
            } else {
                $model->file = UploadedFile::getInstance($model, 'file');
                if ($model->file != null) {
                    $filePath = $model->file->tempName;
                } else {
                 //   return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
			//unset($session['deployment']);
			//unset($session['cdrtype']);
			 Yii::$app->session->setFlash('msg', '
         <div class="alert alert-danger alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <strong> Data Not Found! Enter necessary data to insert   </strong>  </div>');
			return $this->refresh();
                }
                if ($model->file) {
                    $model->file->saveAs('uploads/' . $model->file->baseName . '.' . $model->file->extension);
                }
		  
                $savedPath = Yii::getAlias('@webroot') . '/uploads/' . $model->file->baseName . '.' . $model->file->extension;

                $name = $model->file->baseName . '.' . $model->file->extension;
                
                /*$sql="LOAD DATA LOCAL INFILE '". $savedPath ."'
                    INTO TABLE `cdr_temp`
                    OPTIONALLY ENCLOSED BY '\"'
                    LINES TERMINATED BY '\\n'
                    IGNORE 1 LINES
                    (`id`)
                    ";*/
                $sql="LOAD DATA LOCAL INFILE '". $savedPath ."' 
                    INTO TABLE `cdr_temp`
                    FIELDS TERMINATED BY '\r\n'
                    ENCLOSED BY '\"'
                    LINES TERMINATED BY '\r\n'
                    (`tempo`)
                    ";
                $connection = Yii::$app->getDb();
                $transaction = $connection->beginTransaction();
                try {
                    $connection->createCommand($sql)->execute();
                    $transaction->commit();
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
                $sql1 = "SELECT tempo as tempo FROM cdr_temp";
                $connection = Yii::$app->getDb();
                try {
                    $result = $connection->createCommand($sql1)->queryAll();

                    $data = array();

                    foreach ($result as $row) {
			if ($session['cdr_val']==''){
				$session['cdr_val'].=$row['tempo'];
			}
			else{
                        	$session['cdr_val'] .= '\r\n'.$row['tempo'];
                    	}	
			}
                    
                } catch (Exception $e) {
                }
		$sql2 = "DELETE FROM cdr_temp";
                $connection = Yii::$app->getDb();
                try {
                    $connection->createCommand($sql2)->execute();
                } catch (Exception $e) {
                }	
		//$model->cdra='';
            }
            }
          //  $this->refresh();
           return $this->render('cdranalyzer', ['model' => $model]);
        }

        return $this->render('cdranalyzer', ['model' => $model]);
    }

    public function actionCdrconfiguration()
    {
        $model = new Cdrconfiguration();
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->deployment=='' && $model->cdrtype=='') {
                $dep1 = "SELECT deployment FROM cdr_division WHERE mode='0'";
                $cdr1 = "SELECT cdrtype FROM cdr_division WHERE mode='0'";
            }
            else if($model->deployment==''){
                $dep1 = "SELECT deployment FROM cdr_division WHERE cdrtype='$model->cdrtype' and mode='0'";
                $cdr1 ="SELECT cdrtype FROM cdr_division WHERE cdrtype='$model->cdrtype'and mode='0'";
            }
            else if($model->cdrtype==''){
                $dep1 = "SELECT deployment FROM cdr_division WHERE deployment='$model->deployment' and mode='0'";
                $cdr1 ="SELECT cdrtype FROM cdr_division WHERE deployment='$model->deployment'and mode='0'";
            }
            else{
                $dep1="SELECT deployment FROM cdr_division WHERE deployment='$model->deployment' and cdrtype='$model->cdrtype' and mode='0'";
                $cdr1 ="SELECT cdrtype FROM cdr_division WHERE deployment='$model->deployment' and cdrtype='$model->cdrtype'and mode='0'";
            }
        }
        else{
            $dep1 = "SELECT deployment FROM cdr_division WHERE mode='0'";
            $cdr1 = "SELECT cdrtype FROM cdr_division WHERE mode='0'";
        }
        $connection = Yii::$app->getDb();

        try {
            $result1 = $connection->createCommand($dep1)->queryAll();
            $result2 = $connection->createCommand($cdr1)->queryAll();

            $depz = array();
            $depz1 = array();
            $cdrz = array();
            $cdrz1 = array();

            foreach ($result1 as $key) {
                $depz[] = $key;
            }

            foreach ($result2 as $key) {
                $cdrz[] = $key;
            }
        } catch (Exception $e) { }
        for ($i = 0; $i < sizeof($depz); $i++) {
            array_push($depz1, $depz["$i"]["deployment"]);
        }
        for ($i = 0; $i < sizeof($cdrz); $i++) {
            array_push($cdrz1, $cdrz["$i"]["cdrtype"]);
        }
        
        return $this->render('cdrconfiguration', ['model' => $model, 'depz1' => $depz1, 'cdrz1' => $cdrz1]);
    }
    public function actionConfigure($id)
    {


        $this->layout = 'popup';
        $model = new Cdrconfiguration();
        $cdx = explode("|", $id);
        $f = $cdx['0'];
        $ff = $cdx['1'];
       // $conn = mysqli_connect('localhost', 'root', 'Ramal@123', 'ticket_system');
        $grip = "SELECT * FROM `cdr_division` WHERE deployment='$f' AND cdrtype='$ff' AND mode='0'";
        $connection = Yii::$app->getDb();

        try {
            $result13 = $connection->createCommand($grip)->queryAll();

            $lavels = array();

            foreach ($result13 as $key) {
                $lavels[] = $key;
            }
        } catch (Exception $e) { }
        $lavels2 = array();
        for ($i = 1; $i < 71; $i++) {
            if ($lavels['0']["f$i"] != "null") {
                array_push($lavels2, $lavels['0']["f$i"]);
            }
        }

       // mysqli_close($conn);
        return $this->render('configure', ['model' => $model, 'cdx' => $cdx, 'lavels2' => $lavels2]);
    }

    public function actionCdrcon($id)
    {
        $this->layout = 'popup';
        $model = new CdrConfiguration();
        $ep = explode("|", $id);
        $field = $ep['0'];
        $dep = $ep['1'];
        $cdr = $ep['2'];
    //    $conn = mysqli_connect('localhost', 'root', 'Ramal@123', 'ticket_system');
      /*  if (!$conn) {
            die("cannot connect");
        }*/
        $connection = Yii::$app->getDb();

        $mode0 = "SELECT * FROM `cdr_division` WHERE deployment='$dep' AND cdrtype='$cdr' AND mode='0'";
        $mode1 = "SELECT * FROM `cdr_division` WHERE deployment='$dep' AND cdrtype='$cdr' AND mode='1'";
        $mode2 = "SELECT * FROM `cdr_division` WHERE deployment='$dep' AND cdrtype='$cdr' AND mode='2'";
        try {
            $re1 = $connection->createCommand($mode0)->queryAll();
            $re2 = $connection->createCommand($mode1)->queryAll();
            $re3 = $connection->createCommand($mode2)->queryAll();

            $la = array();
            $la2 = array();
            $la3 = array();

            foreach ($re1 as $key) {
                $la[] = $key;
            }

            foreach ($re2 as $key) {
                $la2[] = $key;
            }

            foreach ($re3 as $key) {
                $la3[] = $key;
            }
        } catch (Exception $e) { }
        $laz = array();
        for ($i = 1; $i < 71; $i++) {
            if ($la['0']["f$i"] != "null") {
                array_push($laz, $la['0']["f$i"]);
            }
        }

        $laz1 = array();
        for ($i = 1; $i < 71; $i++) {
            if ($la2['0']["f$i"] != "null") {
                array_push($laz1, $la2['0']["f$i"]);
            }
        }

        $r = sizeof($laz);
        $laz2 = array();
        for ($i = 1; $i < $r + 1; $i++) {
            array_push($laz2, $la3['0']["f$i"]);
        }

        $laz_new = array_search($field, $laz);
        if ($laz1["$laz_new"] == 1) {
            $arr = "Print out the same data comes in CDR";
        } else {

            $outta = $laz2["$laz_new"];
            $arr = array();
            if ($outta > 99) {
                if ($outta == 100 or $outta == 103) {
                    $ref = "SELECT DISTINCT NET,NET_des FROM `cdr_nfe` WHERE NETid=$outta";
                    $connection = Yii::$app->getDb();

                    try {
                        $ref1 = $connection->createCommand($ref)->queryAll();
                        $ref2 = array();
                        foreach ($ref1 as $key) {
                            $ref2[] = $key;
                        }
                    } catch (Exception $e) { }

                    for ($i = 0; $i < sizeof($ref2); $i++) {
                        array_push($arr, $ref2["$i"]["NET"] . '-' . $ref2["$i"]["NET_des"]);
                    }
                } else if ($outta == 101) {
                    $rer = "SELECT DISTINCT NER,NER_des FROM `cdr_nfe` WHERE NERid=$outta";
                    $connection = Yii::$app->getDb();
                    try {
                        $ref1 = $connection->createCommand($rer)->queryAll();
                        $ref2 = array();
                        foreach ($ref1 as $key) {
                            $ref2[] = $key;
                        }
                    } catch (Exception $e) { }
                    //array_push($out1, $ref['NET'].'-'.$ref['NET_des']);
                    for ($i = 0; $i < sizeof($ref2); $i++) {
                        array_push($arr, $ref2["$i"]["NER"] . '-' . $ref2["$i"]["NER_des"]);
                    }
                } else if ($outta == 102) {
                    $ref = "SELECT DISTINCT NFC,NFC_des FROM `cdr_nfe` WHERE NFCid=$outta";
                    $connection = Yii::$app->getDb();
                    try {
                        $ref1 = $connection->createCommand($ref)->queryAll();
                        $ref2 = array();
                        foreach ($ref1 as $key) {
                            $ref2[] = $key;
                        }
                    } catch (Exception $e) { }
                    //array_push($out1, $ref['NET'].'-'.$ref['NET_des']);
                    for ($i = 0; $i < sizeof($ref2); $i++) {
                        array_push($arr, $ref2["$i"]["NFC"] . '-' . $ref2["$i"]["NFC_des"]);
                    }
                }
            } else {
                $ref = "SELECT DISTINCT Value,event FROM `cdr_allinone` WHERE Code=$outta";
                $connection = Yii::$app->getDb();
                try {
                    $ref1 = $connection->createCommand($ref)->queryAll();
                    $ref2 = array();
                    foreach ($ref1 as $key) {
                        $ref2[] = $key;
                    }
                } catch (Exception $e) { }
                for ($i = 0; $i < sizeof($ref2); $i++) {
                    array_push($arr, $ref2["$i"]["Value"] . '-' . $ref2["$i"]["event"]);
                }
            }
        }
        //print_r($out);
       // mysqli_close($conn);
        return $this->render('cdrcon', ['model' => $model, 'arr' => $arr]);
    }

    public function actionAnalyze($id)
    {
        $session = Yii::$app->session;
        $session['add'] = $id;
        $this->layout = 'popup';
        $model = new CdrAllinone();
        $deployment = $session['deployment'];
        $cdrtype = $session['cdrtype'];

        ///////////////////////////////////Copied from PROJECT.PHP///////////////////////////////////

        $printlabel = array();
        $valuex = array();

        $cdr_array = explode(",", $id);
        if (sizeof($cdr_array) == 1) {
            $cdr_array = explode("|", $id);
        }
        //array_pop($cdr_array);


        $conn = mysqli_connect('localhost', 'root', 'Ramal@123', 'ticket_system');

        if (!$conn) {
            die("cannot connect");
        }

        $label = "SELECT * FROM `cdr_division` WHERE deployment='$deployment' AND cdrtype='$cdrtype' AND mode='0'";
        $refer = "SELECT * FROM `cdr_division` WHERE deployment='$deployment' AND cdrtype='$cdrtype' AND mode='1'";
        $tableno = "SELECT * FROM `cdr_division` WHERE deployment='$deployment' AND cdrtype='$cdrtype' AND mode='2'";

        $connection = Yii::$app->getDb();

        try {
            $result1 = $connection->createCommand($label)->queryAll();
            $result2 = $connection->createCommand($refer)->queryAll();
            $result3 = $connection->createCommand($tableno)->queryAll();

            $label_array = array();
            $refer_array = array();
            $tableno_array = array();

            foreach ($result1 as $key) {
                $label_array[] = $key;
            }

            foreach ($result2 as $key) {
                $refer_array[] = $key;
            }

            foreach ($result3 as $key) {
                $tableno_array[] = $key;
            }
        } catch (Exception $e) { }
        //$data=array_reverse($data);

        //$data=sort($data);
        $label_array1 = array();
        for ($i = 1; $i < 72; $i++) {
            if (isset($label_array['0']["f$i"])) {
                if ($label_array['0']["f$i"] != "null") {
                    array_push($label_array1, $label_array['0']["f$i"]);
                }
            }
            else{
                Yii::$app->session->setFlash('error', 'Check the Deployment and CDR types again');
            }
            
        }

        $refer_array1 = array();

        for ($i = 1; $i < 72; $i++) {
            if (isset($label_array['0']["f$i"])) {
                if ($refer_array['0']["f$i"] != "null") {
                    array_push($refer_array1, $refer_array['0']["f$i"]);
                }
            }
            else{
                Yii::$app->session->setFlash('error', 'Check the Deployment and CDR types again');
            }
            
        }
        $r = sizeof($label_array1);
        $tableno_array1 = array();
        for ($i = 1; $i < $r + 1; $i++) {
            array_push($tableno_array1, $tableno_array['0']["f$i"]);
        }
        
        if ($cdr_array["0"]=='3'){
            $tableno_array1["35"]='16';
        }
        
        $check=sizeof($cdr_array)-$r;
        if ($check==0 || $check==1) {
            for ($i = 0; $i < $r; $i++) {
                array_push($printlabel, $label_array1["$i"]);
                //print_r($label_array["$i"]);
                //print_r(' : ');
                if ($refer_array1["$i"] == "1") {
                    array_push($valuex, $cdr_array["$i"]);
                    //print_r($cdr_array["$i"]);
                }
                if ($refer_array1["$i"] == "0") {
                    if ($cdr_array["$i"]==' undefined'){
                        array_push($valuex, $cdr_array["$i"]);
                    }
                    else if ($cdr_array["$i"] != " " ) { 
                            $value = (int) $cdr_array["$i"];
                            //print_r($value);
                            $code = (int) $tableno_array1["$i"];
                            if ($code == 100 || $code == 103) {

                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NET_des FROM `cdr_nfe` WHERE NETid=$code AND NET=$value"));
                                array_push($valuex, $cdr_array["$i"] . '-' . $ref['NET_des']);
                                //print_r($ref['NET_des']);
                                $NETid = $code;
                                $NET = $value;
                            } else if ($code == 101) {
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NER_des FROM `cdr_nfe` WHERE NERid=$code AND NER=$value AND NETid=$NETid AND NET=$NET"));
                                array_push($valuex, $cdr_array["$i"] . '-' . $ref['NER_des']);
                                //print_r($ref['NER_des']);
                                $NERid = 101;
                                $NER = $value;
                            } else if ($code == 102) {
                                if (isset($NER)) {
                                    $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                    array_push($valuex, $cdr_array["$i"] . '-' . $ref['NFC_des']);
                                    //print_r($ref['NFC_des']);
                                } else {
                                    $NER = 200;
                                    $NERid = 101;
                                    $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT NFC_des FROM `cdr_nfe` WHERE NFCid=$code AND NFC=$value AND NERid=$NERid AND NER=$NER AND NETid=$NETid AND NET=$NET"));
                                    array_push($valuex, $cdr_array["$i"] . '-' . $ref['NFC_des']);
                                    //print_r($ref['NFC_des']);
                                }
                            } else {
                                $ref = mysqli_fetch_assoc(mysqli_query($conn, "SELECT event FROM `cdr_allinone` WHERE Code=$code AND Value=$value"));
                                array_push($valuex, $cdr_array["$i"] . '-' . $ref['event']);
                                //print_r($ref['event']);
                            }
                    } else {
                        array_push($valuex, $cdr_array["$i"]);
                    }
                }
            }
            $printlabel;
            $valuex;
        } else {
            Yii::$app->session->setFlash('error', 'WRONG CDR TYPE');
            $printlabel=array('');
            array_push($valuex," ");
        }
        mysqli_close($conn);
        ////////////////////////////////////////////////////////////////////////////////////////////
        return $this->render('analyze', ['model' => $model, 'printlabel' => $printlabel, 'valuex' => $valuex]);
    }
}
