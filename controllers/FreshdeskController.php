<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ticketinfo;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\BaseArrayHelper;
use app\controllers\ArrayHelper;
use app\components\FreshDeskApi;
use app\components\AppLogger;


class FreshdeskController extends Controller
{
   
    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        
        return parent::beforeAction($action);
    }

    public function actionUpdatedatabase()
    {   
            $fdApi = new FreshDeskApi();

            $params = json_decode(Yii::$app->request->rawBody, true);

            //file_put_contents('/var/www/html/data.txt', print_r($params, true));

	    //file_put_contents('/var/www/html/data.txt',print_r($params['freshdesk_webhook']['triggered_event'], true));

            $result = json_decode($fdApi->getDetails($params['freshdesk_webhook']['ticket_id']),true);

            $tid = $result['id'];

            $exist = Ticketinfo::find()->where( [ 'ticketId' => $tid ] )->exists(); 

            /*$status = $result['priority'];

            switch ($status) {
                case 1:
                    $status="Low";
                break;
                case 2:
                    $status="Medium";
                break;
                case 3:
                    $status="High";
                break;
                case 4:
                    $status="Urgent";
                break;
            }

            $groupId = $result['group_id'];

            switch ($groupId) {
                case 11000003887:
                    $groupId="Asia_L1";
                break;
                case 11000003888:
                    $groupId="Asia_L2";
                break;
                case 11000003889:
                    $groupId="Asia_L3";
                break;
                case 11000003851:
                    $groupId="Latam_L1";
                break;
                case 11000003850:
                    $groupId="Latam_L2";
                break;
                case 11000003886:
                    $groupId="Latam_L3";
                break;
                case 11000003890:
                    $groupId="East Asia_L1";
                break;
                case 11000003891:
                    $groupId="East Asia_L2";
                break;
                case 11000003892:
                    $groupId="East Asia_L3";
                break;
            }*/

	    date_default_timezone_set('Asia/Colombo');
            $createTime = $result['created_at'];
	    $createTime = strtotime($createTime);
	    $create = date("Y-m-d H:i:s", $createTime);

	    $dueTime = $result['due_by'];
	    if(!empty($dueTime)){
	    	date_default_timezone_set('Asia/Colombo');
            	$dueTime = strtotime($dueTime);
            	$due = date("Y-m-d H:i:s", $dueTime);
	    }else {
		
		$due = "";
	    }

            $resolveTime = $result['stats']['resolved_at'];
	    if(!empty($resolveTime)){
		date_default_timezone_set('Asia/Colombo');
            	$resolveTime = strtotime($resolveTime);
            	$resolve = date("Y-m-d H:i:s", $resolveTime);

	    } else {

		$resolve = "";
	    }
	    
	    $closeTime = $result['stats']['closed_at'];
	    if(!empty($closeTime)){
	    	date_default_timezone_set('Asia/Colombo');
            	$closeTime = strtotime($closeTime);
            	$close = date("Y-m-d H:i:s", $closeTime);
	    } else {

                $close = "";
            }

	    $lastUpdateTime = $result['stats']['status_updated_at'];
	    if(!empty($lastUpdateTime)){
	    	date_default_timezone_set('Asia/Colombo');
            	$lastUpdateTime = strtotime($lastUpdateTime);
            	$lastUpdate = date("Y-m-d H:i:s", $lastUpdateTime);
	    } else {

                $lastUpdate = "";
            }

	    $initialResponseTime = $result['stats']['first_responded_at'];
	    if(!empty($initialResponseTime)){
	    	date_default_timezone_set('Asia/Colombo');
            	$initialResponseTime = strtotime($initialResponseTime);
            	$initialResponse = date("Y-m-d H:i:s", $initialResponseTime);
	    } else {

                $initialResponse = "";
            }

	    $renewalDate = $params['freshdesk_webhook']['ticket_requester_company_renewal_date'];
            if(!empty($renewalDate)){
                date_default_timezone_set('Asia/Colombo');
                $renewalDate = strtotime($renewalDate);
                $renewal = date("Y-m-d H:i:s", $renewalDate);
            } else {

                $renewal = "";
            }

	    $Id = $params['freshdesk_webhook']['ticket_id'];

            $event = $params['freshdesk_webhook']['triggered_event'];
		$eve=$event;
            //$createTime = date('Y-m-d h:i:s', strtotime($createTime));

            if($exist){

                $model = Ticketinfo::findOne($tid);

                $model->type = $result['type'];
                $model->createTime = $create;
		$model->dueTime = $due;
		$model->resolveTime = $resolve;
		$model->closeTime = $close;
		$model->lastUpdateTime = $lastUpdate;
		$model->initialResponseTime = $initialResponse;
                $model->priority = $params['freshdesk_webhook']['ticket_priority'];
                $model->groupName = $params['freshdesk_webhook']['ticket_group_name'];
		$model->agent = $params['freshdesk_webhook']['ticket_agent_name'];
                $model->severity = $result['custom_fields']['severity'];
		$model->subject = $params['freshdesk_webhook']['ticket_subject'];
		$model->status = $params['freshdesk_webhook']['ticket_status'];
		$model->source = $params['freshdesk_webhook']['ticket_source'];
		$model->tags = $params['freshdesk_webhook']['ticket_tags'];
		$model->acknowledger = $params['freshdesk_webhook']['ticket_acknowladger'];
		$model->productName = $params['freshdesk_webhook']['ticket_product'];
		$model->operator = $params['freshdesk_webhook']['ticket_operator'];
		$model->issueCategory = $params['freshdesk_webhook']['ticket_issue_catagory'];
		$model->workingTimeOfDay = $params['freshdesk_webhook']['ticket_level'];
		$model->neutralize = $params['freshdesk_webhook']['ticket_cf_neutralize'];
		$model->resolution = $params['freshdesk_webhook']['ticket_cf_resolution'];
		$model->uniqueExternal = $params['freshdesk_webhook']['unique_external_id'];
		$model->companyName = $params['freshdesk_webhook']['ticket_company_name'];
		$model->companyDomain = $params['freshdesk_webhook']['ticket_company_domains'];
		$model->healthScore = $params['freshdesk_webhook']['ticket_requester_company_health_score'];
		$model->accountTier = $params['freshdesk_webhook']['ticket_requester_company_account_tier'];
		//$model->AMCPeriodStart = $params['freshdesk_webhook']['ticket_requester_company_amc_period'];
		//$model->AMCPeriodEnd = $params['freshdesk_webhook']['ticket_requester_company_amc_period_end'];
		$model->renewalDate = $renewal;
		$model->SLAStatus = $params['freshdesk_webhook']['ticket_requester_company_sla_status'];
		$model->SLACusContact = $params['freshdesk_webhook']['ticket_requester_company_sla_customer_contact'];
		$model->industry = $params['freshdesk_webhook']['ticket_requester_company_industry'];
		$model->fullName = $result['requester']['name'];
		$model->email = $result['requester']['email'];
		$model->workPhone = $result['requester']['phone'];
		$model->mobilePhone = $result['requester']['mobile'];
		$model->mgmtEscalated = $result['custom_fields']['cf_management_escalated'];
		$model->effectiveFirstResponse = $result['custom_fields']['cf_effective_first_response'];
		$model->resolutionSummary = $result['custom_fields']['cf_resolution_summary'] ;
                $model->escalationSummary = $result['custom_fields']['cf_escalation_summary'];
                $model->outage_time=$result['custom_fields']['cf_outage_time'];
               /*if(isset($event['ticket_action'])){
		if($event['ticket_action']=='delete'){
                    $model->status ='deleted';
                }}*/
		if(isset($eve)){
                    if (strpos($eve, 'delete') !== false) {
                        $model->status ='Deleted';
                    }
                   }

		 $model->save();
		
                //print_r($model->errors);
		
		//Yii::$app->appLog->writeLog("app\models\Ticketinfo Record updated successfully",[$params],2);

		Yii::$app->appLog->writeLog("app\models\Ticketinfo Record updated successfully", [$params],2);

            }

            else{

                $model = new Ticketinfo();
                $model->ticketId = $tid;
                $model->type = $result['type'];
                $model->createTime = $create;
                $model->dueTime = $due;
                $model->resolveTime = $resolve;
		$model->closeTime = $close;
                $model->lastUpdateTime = $lastUpdate;
		$model->initialResponseTime = $initialResponse;
                $model->priority = $params['freshdesk_webhook']['ticket_priority'];
                $model->groupName = $params['freshdesk_webhook']['ticket_group_name'];
		$model->agent = $params['freshdesk_webhook']['ticket_agent_name'];
                $model->severity = $result['custom_fields']['severity'];
		$model->subject = $params['freshdesk_webhook']['ticket_subject'];
		$model->status = $params['freshdesk_webhook']['ticket_status'];
                $model->source = $params['freshdesk_webhook']['ticket_source'];
	        $model->tags = $params['freshdesk_webhook']['ticket_tags'];
                $model->acknowledger = $params['freshdesk_webhook']['ticket_acknowladger'];
                $model->productName = $params['freshdesk_webhook']['ticket_product'];
                $model->operator = $params['freshdesk_webhook']['ticket_operator'];
                $model->issueCategory = $params['freshdesk_webhook']['ticket_issue_catagory'];
                $model->workingTimeOfDay = $params['freshdesk_webhook']['ticket_level'];
                $model->neutralize = $params['freshdesk_webhook']['ticket_cf_neutralize'];
                $model->resolution = $params['freshdesk_webhook']['ticket_cf_resolution'];
                $model->uniqueExternal = $params['freshdesk_webhook']['unique_external_id'];
                $model->companyName = $params['freshdesk_webhook']['ticket_company_name'];
                $model->companyDomain = $params['freshdesk_webhook']['ticket_company_domains'];
                $model->healthScore = $params['freshdesk_webhook']['ticket_requester_company_health_score'];
                $model->accountTier = $params['freshdesk_webhook']['ticket_requester_company_account_tier'];
                //$model->AMCPeriodStart = $params['freshdesk_webhook']['ticket_requester_company_amc_period'];
                //$model->AMCPeriodEnd = $params['freshdesk_webhook']['ticket_requester_company_amc_period_end'];
                $model->renewalDate = $renewal;
                $model->SLAStatus = $params['freshdesk_webhook']['ticket_requester_company_sla_status'];
                $model->SLACusContact = $params['freshdesk_webhook']['ticket_requester_company_sla_customer_contact'];
                $model->industry = $params['freshdesk_webhook']['ticket_requester_company_industry'];
		$model->fullName = $result['requester']['name'];
                $model->email = $result['requester']['email'];
                $model->workPhone = $result['requester']['phone'];
                $model->mobilePhone = $result['requester']['mobile'];
		$model->mgmtEscalated = $result['custom_fields']['cf_management_escalated'];
		$model->effectiveFirstResponse = $result['custom_fields']['cf_effective_first_response'];
		$model->resolutionSummary = $result['custom_fields']['cf_resolution_summary'] ;
                $model->escalationSummary = $result['custom_fields']['cf_escalation_summary'];
		$model->outage_time=$result['custom_fields']['cf_outage_time'];
       		/*if(isset($event['ticket_action'])){
		if($event['ticket_action']=='delete'){
                    $model->status ='deleted';
                }}*/
		 if(isset($eve)){
                    if (strpos($eve, 'delete') !== false) {
                        $model->status ='Deleted';
                    }
                   }
		$model->save();

		Yii::$app->appLog->writeLog("app\models\Ticketinfo Record inserted successfully",[$params],2);

            }

    }

}
