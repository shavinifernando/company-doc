<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use app\models\Ticketinfo;
use yii\data\SqlDataProvider;
use yii\helpers\Json;
use app\controllers\AppController;
use yii\data\ArrayDataProvider;
use yii\helpers\BaseArrayHelper;


class TicketinfoController extends Controller
{
    /**
     * {@inheritdoc}
     */

    // public function behaviors()
    // {
    //     return [
            
    
    //     ];
    // }
    public function beforeAction($action) 
    {
		$this->enableCsrfValidation = false;
		
		return parent::beforeAction($action);
	}
   
    public function actionLeveldistribution()
    {

   		$model = new Ticketinfo();

    	return $this->render('Leveldistribution', ['model'=>$model]);

	}

	public function actionShowleveldistribution()
    {

    	$model = new Ticketinfo();
    	$model->scenario = Ticketinfo::SCENARIO_DIST;

    	 if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());
            $month = \yii\helpers\ArrayHelper::getValue($_POST, 'date');
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            if ($dregion == '')
                $sql="SELECT SUBSTRING(groupName, 1,(CHAR_LENGTH(groupName) - 3)) AS level, SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' THEN 1 ELSE 0 END) AS L1,SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' THEN 1 ELSE 0 END) AS L2, SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' THEN 1 ELSE 0 END) AS L3,SUM((CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' THEN 1 ELSE 0 END)) AS Total FROM `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND SUBSTRING(groupName, 1,(CHAR_LENGTH(groupName) - 3)) IN('Latam', 'East Asia','Asia') GROUP BY level";
            if ($dregion == 'Latam')
                $sql="SELECT SUBSTRING(groupName, 1,(CHAR_LENGTH(groupName) - 3)) AS level, SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' THEN 1 ELSE 0 END) AS L1,SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' THEN 1 ELSE 0 END) AS L2, SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' THEN 1 ELSE 0 END) AS L3,SUM((CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' THEN 1 ELSE 0 END)) AS Total FROM `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND SUBSTRING(groupName, 1,(CHAR_LENGTH(groupName) - 3)) IN('Latam', 'East Asia','Asia') AND groupName like 'Latam%' GROUP BY level";
            if ($dregion == 'Asia')
                $sql="SELECT SUBSTRING(groupName, 1,(CHAR_LENGTH(groupName) - 3)) AS level, SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' THEN 1 ELSE 0 END) AS L1,SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' THEN 1 ELSE 0 END) AS L2, SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' THEN 1 ELSE 0 END) AS L3,SUM((CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' THEN 1 ELSE 0 END)) AS Total FROM `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND SUBSTRING(groupName, 1,(CHAR_LENGTH(groupName) - 3)) IN('Latam', 'East Asia','Asia') AND groupName like 'Asia%' GROUP BY level";
            if ($dregion == 'SEA')
                $sql="SELECT SUBSTRING(groupName, 1,(CHAR_LENGTH(groupName) - 3)) AS level, SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' THEN 1 ELSE 0 END) AS L1,SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' THEN 1 ELSE 0 END) AS L2, SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' THEN 1 ELSE 0 END) AS L3,SUM((CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' THEN 1 ELSE 0 END)) AS Total FROM `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND SUBSTRING(groupName, 1,(CHAR_LENGTH(groupName) - 3)) IN('Latam', 'East Asia','Asia') AND groupName like 'East Asia%'GROUP BY level";
           
           	$connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();
					foreach ($result as $row) {
  					$data[] = $row;
				}

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

	}

	public function actionWorkingtimedistribution()
    {

   		$model = new Ticketinfo();

    	return $this->render('Workingtimedistribution', ['model'=>$model]);

	}

	public function actionShowworkingtimedistribution()
    {

    	$model = new Ticketinfo();
    	
    	$model->scenario = Ticketinfo::SCENARIO_DIST;

    	 if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $month = \yii\helpers\ArrayHelper::getValue($_POST, 'date');

            //$sql="SELECT severity AS severity, SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END) AS 'Office', SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END) AS 'Out',SUM((CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END)) AS Total from `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND severity IN ('Major', 'Minor','Critical') GROUP BY severity";
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            $sql="SELECT severity AS severity, SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END) AS 'Office', SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END) AS 'Out',SUM((CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END)) AS Total from `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND severity IN ('Major', 'Minor','Critical') GROUP BY severity";
           
            if ($dregion == '')
                $sql="SELECT severity AS severity, SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END) AS 'Office', SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END) AS 'Out',SUM((CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END)) AS Total from `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND severity IN ('Major', 'Minor','Critical') GROUP BY severity";
            if ($dregion == 'Latam')
                $sql="SELECT severity AS severity, SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END) AS 'Office', SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END) AS 'Out',SUM((CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END)) AS Total from `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND severity IN ('Major', 'Minor','Critical') AND groupName like 'Latam%' GROUP BY severity";
            if ($dregion == 'Asia')
                $sql="SELECT severity AS severity, SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END) AS 'Office', SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END) AS 'Out',SUM((CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END)) AS Total from `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND severity IN ('Major', 'Minor','Critical') AND groupName like 'Asia%' GROUP BY severity";
            if ($dregion == 'SEA')
                $sql="SELECT severity AS severity, SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END) AS 'Office', SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END) AS 'Out',SUM((CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' THEN 1 ELSE 0 END)) AS Total from `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' AND severity IN ('Major', 'Minor','Critical') AND groupName like 'East Asia%' GROUP BY severity";
            
           	$connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();
					foreach ($result as $row) {
  					$data[] = $row;
				}

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

	}

    	public function actionProductdistribution()
    {

        $model = new Ticketinfo();

        return $this->render('Productdistribution', ['model'=>$model]);

    }

    public function actionShowproductdistribution()
    {

        $model = new Ticketinfo();
        $model->scenario = Ticketinfo::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $month = \yii\helpers\ArrayHelper::getValue($_POST, 'date');

            //$sql="SELECT productName AS Product,SUM(severity='Minor') AS Minor,SUM(severity='Major') AS Major,SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total from `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m')='$month' GROUP BY productName";

            //$sql = "SELECT productName AS Product,SUM(severity='Minor') AS Minor,SUM(severity='Major') AS Major,SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total from `ticketinfo` WHERE productName IS NOT NULL AND DATE_FORMAT(createTime,'%Y-%m')='$month' GROUP BY productName";
            //echo $sql;
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            if ($dregion == '')
                $sql = "SELECT productName AS Product,SUM(severity='Minor') AS Minor,SUM(severity='Major') AS Major,SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total from `ticketinfo` WHERE productName IS NOT NULL AND DATE_FORMAT(createTime,'%Y-%m')='$month' GROUP BY productName";
            if ($dregion == 'Latam')
                $sql = "SELECT productName AS Product,SUM(severity='Minor') AS Minor,SUM(severity='Major') AS Major,SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total from `ticketinfo` WHERE productName IS NOT NULL AND DATE_FORMAT(createTime,'%Y-%m')='$month' AND groupName like 'Latam%' GROUP BY productName";
            if ($dregion == 'Asia')
                $sql = "SELECT productName AS Product,SUM(severity='Minor') AS Minor,SUM(severity='Major') AS Major,SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total from `ticketinfo` WHERE productName IS NOT NULL AND DATE_FORMAT(createTime,'%Y-%m')='$month' AND groupName like 'Asia%' GROUP BY productName";
            if ($dregion == 'SEA')
                $sql = "SELECT productName AS Product,SUM(severity='Minor') AS Minor,SUM(severity='Major') AS Major,SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total from `ticketinfo` WHERE productName IS NOT NULL AND DATE_FORMAT(createTime,'%Y-%m')='$month' AND groupName like 'East Asia%' GROUP BY productName";
             
            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();
                    foreach ($result as $row) {
                    $data[] = $row;
                }

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

	public function actionSeveritytrend()
    {

        $model = new Ticketinfo();

        return $this->render('Severitytrend', ['model'=>$model]);

    }

    public function actionShowseveritytrend()
    {

        $model = new Ticketinfo();
        
	$model->scenario = Ticketinfo::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            if ($dregion == '')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(severity='Minor') AS Minor, SUM(severity='Major') AS Major, SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month ";
            if ($dregion == 'Latam')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(severity='Minor') AS Minor, SUM(severity='Major') AS Major, SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Latam%' GROUP BY Month ";
            if ($dregion == 'Asia')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(severity='Minor') AS Minor, SUM(severity='Major') AS Major, SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Asia%' GROUP BY Month ;";
            if ($dregion == 'SEA')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(severity='Minor') AS Minor, SUM(severity='Major') AS Major, SUM(severity='Critical') AS Critical,SUM((severity='Minor')+(severity='Major')+(severity='Critical')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'East Asia%' GROUP BY Month ;";
            
            //echo $sql;

            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    } 

    
	public function actionLeveltrend()
    {

        $model = new Ticketinfo();

        return $this->render('Leveltrend', ['model'=>$model]);

    }

    public function actionShowleveltrend()
    {

        $model = new Ticketinfo();
        
	$model->scenario = Ticketinfo::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            //$sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month";
            if ($dregion == '')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='Delivery') AS Delivery , SUM(groupName='Presales') AS Presales ,SUM(groupName='Operations-Managers') AS OperationsManagers , SUM(groupName='Truemove_L1') AS Truemove_L1 ,  SUM(groupName='Truemove_L2') AS Truemove_L2 ,  SUM(groupName='Truemove_L3') AS Truemove_L3 ,SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month ;";
             if ($dregion == 'Latam')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Latam%' GROUP BY Month ;";
             if ($dregion == 'Asia')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,  SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM((groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Asia%' GROUP BY Month ;";
            if ($dregion == 'SEA')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'East Asia%' GROUP BY Month ;";
            //echo $sql;

            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

       public function actionPrioritytrend()
    {

        $model = new Ticketinfo();

        return $this->render('Prioritytrend', ['model'=>$model]);

    }

    public function actionShowprioritytrend()
    {

        $model = new Ticketinfo();
        
	$model->scenario = Ticketinfo::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            //AND groupName like 'Latam%'
            //$sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(priority='Low') AS Low, SUM(priority='High') AS High, SUM(priority='Medium') AS Medium, SUM(priority='Urgent') AS Urgent,SUM((priority='Low')+(priority='High')+(priority='Medium')+(priority='Urgent')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month";
            if ($dregion == '')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(priority='Low') AS Low, SUM(priority='Medium') AS Medium, SUM(priority='High') AS High, SUM(priority='Urgent') AS Urgent,SUM((priority='Low')+(priority='High')+(priority='Medium')+(priority='Urgent')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month ;";
            if ($dregion == 'Latam')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(priority='Low') AS Low,  SUM(priority='Medium') AS Medium, SUM(priority='High') AS High, SUM(priority='Urgent') AS Urgent,SUM((priority='Low')+(priority='High')+(priority='Medium')+(priority='Urgent')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Latam%' GROUP BY Month ;";
            if ($dregion == 'Asia')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(priority='Low') AS Low,  SUM(priority='Medium') AS Medium, SUM(priority='High') AS High, SUM(priority='Urgent') AS Urgent,SUM((priority='Low')+(priority='High')+(priority='Medium')+(priority='Urgent')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Asia%' GROUP BY Month ;";
            if ($dregion == 'SEA')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(priority='Low') AS Low,  SUM(priority='Medium') AS Medium, SUM(priority='High') AS High, SUM(priority='Urgent') AS Urgent,SUM((priority='Low')+(priority='High')+(priority='Medium')+(priority='Urgent')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'East Asia%' GROUP BY Month ;";
           
            //echo $sql;

            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

    public function actionDeploymenttrend()
    {

        $model = new Ticketinfo();

        return $this->render('Deploymenttrend', ['model'=>$model]);

    }

    public function actionShowdeploymenttrend()
    {

        $model = new Ticketinfo();
        
	$model->scenario = Ticketinfo::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');
	        $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');

	    //$sql="SELECT ";
        //AND groupName like 'Latam%'
            if ($dregion == '')
                $sql = "SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,
						SUM(operator='Bolivia-Tigo') AS BoliviaTigo,
						SUM(operator='Tanzania-Millicom') AS TanzaniaMillicom,
						SUM(operator='Sri Lanka-Hutchison') AS SriLankaHutchison,
						SUM(operator='Sri Lanka-Etisalat') AS SriLankaEtisalat,
						SUM(operator='Sri Lanka-Telecom') AS SriLankaTelecom,
						SUM(operator='Sri Lanka-Airtel') AS SriLankaAirtel,
						SUM(operator='Maldives-Ooredoo') AS MaldivesOoredoo,
						SUM(operator='India-IAF : ALU') AS IndiaIAF,
						SUM(operator='India-TATA') AS IndiaTATA,
						SUM(operator='Thailand-DTAC') AS ThailandDTAC,
						SUM(operator='Thailand-TrueMove') AS ThailandTrueMove,
						SUM(operator='Ghana-Millicom') AS GhanaMillicom,
						SUM(operator='Tahiti-Vini') AS TahitiVini,
						SUM(operator='Chad-Millicom') AS ChadMillicom,
						SUM(operator='Senagal-Millicom') AS SenagalMillicom,
						SUM(operator='Kenya-Safaricom') AS KenyaSafaricom,
						SUM(operator='New Zealand-Vodafone') AS NewZealandVodafone,
						SUM(operator='Honduras-Tigo') AS HondurasTigo,
						SUM(operator='Intelli-Australia') AS IntelliAustralia,
						SUM(operator='Colombia-Tigo') AS ColombiaTigo,
						SUM(operator='Cellcard-Cambodia') AS CellcardCambodia,
						SUM(operator='Salvador-Tigo') AS SalvadorTigo,
						SUM(operator='Paraguay-Tigo') AS ParaguayTigo,
						SUM(operator='Guatemala-Tigo') AS GuatemalaTigo,
						SUM(operator='Cambodia-Smart') AS SmartCambodia,
						SUM(
							(operator='Bolivia-Tigo')+
							(operator='Tanzania-Millicom')+
							(operator='Sri Lanka-Hutchison Telecommunications Lanka (Pvt) Ltd')+
							(operator='Sri Lanka-Etisalat Lanka (pvt) Ltd.')+
							(operator='Sri Lanka-Telecom PLC')+
							(operator='Sri Lanka-Airtel')+
							(operator='Maldives-Ooredoo')+
							(operator='India-IAF : ALU')+
							(operator='India-TATA')+
							(operator='Thailand-DTAC')+
							(operator='Thailand-TrueMove')+
							(operator='Ghana-Millicom')+
							(operator='Tahiti-Vini')+
							(operator='Chad-Millicom')+
							(operator='Senagal-Millicom')+
							(operator='Kenya-Safaricom')+
							(operator='New Zealand-Vodafone')+
							(operator='Honduras-Tigo')+
							(operator='Intelli-Australia')+
							(operator='Colombia-Tigo')+
							(operator='Cellcard-Cambodia')+
							(operator='Salvador-Tigo')+
							(operator='Paraguay-Tigo')+
							(operator='Guatemala-Tigo')+
							(operator='Cambodia-Smart')
						) AS Total 
						FROM ticketinfo 
						WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' 
						GROUP BY Month;";
            if ($dregion == 'SEA')
                $sql = "SELECT 
							DATE_FORMAT(createTime,'%Y-%M') AS Month,
							SUM(operator='Thailand-DTAC') AS ThailandDTAC,
							SUM(operator='Thailand-TrueMove') AS ThailandTrueMove,
							SUM(operator='Cellcard-Cambodia') AS CellcardCambodia,
							SUM(operator='Cambodia-Smart') AS SmartCambodia,
							SUM(
								(operator='Thailand-DTAC')+
								(operator='Thailand-TrueMove')+
								(operator='Cellcard-Cambodia')+
								(operator='Cambodia-Smart')
							) AS Total 
							FROM ticketinfo 
							WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' 
							GROUP BY Month;";
            if ($dregion == 'Latam')
                $sql = "SELECT 
							DATE_FORMAT(createTime,'%Y-%M') AS Month,
							SUM(operator='Bolivia-Tigo') AS BoliviaTigo,
							SUM(operator='Guatemala-Tigo') AS GuatemalaTigo,
							SUM(operator='Honduras-Tigo') AS HondurasTigo,
							SUM(operator='Colombia-Tigo') AS ColombiaTigo,
							SUM(operator='Salvador-Tigo') AS SalvadorTigo,
							SUM(operator='Paraguay-Tigo') AS ParaguayTigo,
							SUM(
								(operator='Bolivia-Tigo')+
								(operator='Honduras-Tigo')+
								(operator='Colombia-Tigo')+
								(operator='Guatemala-Tigo')+
								(operator='Salvador-Tigo')+
								(operator='Paraguay-Tigo')
							) AS Total 
							FROM ticketinfo 
							WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' 
							GROUP BY Month ;
						";
            if ($dregion == 'Asia')
                $sql = "SELECT 
							DATE_FORMAT(createTime,'%Y-%M') AS Month,
							SUM(operator='Sri Lanka-Hutchison') AS SriLankaHutchison,
							SUM(operator='Sri Lanka-Telecom') AS SriLankaTelecom,
							SUM(operator='Maldives-Ooredoo') AS MaldivesOoredoo,
							SUM(operator='India-IAF : ALU') AS IndiaIAF,
							SUM(operator='India-TATA') AS IndiaTATA,
							SUM(operator='Kenya-Safaricom') AS KenyaSafaricom,
							SUM(operator='Tanzania-Millicom') AS TanzaniaMillicom,
							SUM(operator='Tahiti-Vini') AS TahitiVini,
							SUM(
								(operator='Sri Lanka-Hutchison Telecommunications Lanka (Pvt) Ltd')+
								(operator='Sri Lanka-Etisalat Lanka (pvt) Ltd.')+
								(operator='Sri Lanka-Telecom PLC')+
								(operator='Maldives-Ooredoo')+
								(operator='India-IAF : ALU')+
								(operator='India-TATA')+
								(operator='Kenya-Safaricom')+
								(operator='Tanzania-Millicom')+
								(operator='Tahiti-Vini')) AS Total 
						FROM ticketinfo 
						WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' 
						GROUP BY Month ;";
            if ($dregion == 'Others')
                $sql = "SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(operator='Sri Lanka-Etisalat') AS SriLankaEtisalat,SUM(operator='Sri Lanka-Airtel') AS SriLankaAirtel,SUM(operator='Tanzania-Millicom') AS TanzaniaMillicom,SUM(operator='Senagal-Millicom') AS SenagalMillicom, SUM(operator='Ghana-Millicom') AS GhanaMillicom,SUM(operator='Chad-Millicom') AS ChadMillicom,SUM(operator='New Zealand-Vodafone') AS NewZealandVodafone,SUM(operator='Intelli-Australia') AS IntelliAustralia,SUM((operator='Sri Lanka-Etisalat Lanka (pvt) Ltd.')+(operator='Sri Lanka-Airtel')+(operator='Tanzania-Millicom')+(operator='Ghana-Millicom')+(operator='Chad-Millicom')+(operator='Senagal-Millicom')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month ;";

            //echo $sql;

            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

    

    public function actionWorkingtimetrend()
    {

        $model = new Ticketinfo();

        return $this->render('Workingtimetrend', ['model'=>$model]);

    }

    public function actionShowworkingtimetrend()
    {
        $model = new Ticketinfo();
        
	$model->scenario = Ticketinfo::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            //$sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END) AS OfficeMinor,SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END) AS OfficeMajor,SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END) AS OfficeCritical,SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END) AS OutMinor,SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END) AS OutMajor,SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END) AS OutCritical,SUM((CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END)) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Latam%' GROUP BY Month";
            //AND groupName like 'Latam%'
            if ($dregion == '')
                $sql="SELECT 
						DATE_FORMAT(createTime,'%Y-%M') AS Month,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END) AS OfficeMinor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END) AS OfficeMajor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END) AS OfficeCritical,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END) AS OutMinor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END) AS OutMajor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END) AS OutCritical,
						SUM(
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END)) AS Total 
						FROM ticketinfo 
						WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' 
						GROUP BY Month ;
					";
            if ($dregion == 'Latam')
                $sql="SELECT 
						DATE_FORMAT(createTime,'%Y-%M') AS Month,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END) AS OfficeMinor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END) AS OfficeMajor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END) AS OfficeCritical,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END) AS OutMinor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END) AS OutMajor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END) AS OutCritical,
						SUM(
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END)
							) AS Total 
						FROM ticketinfo 
						WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Latam%' 
						GROUP BY Month ;
					";
            if ($dregion == 'Asia')
                $sql="SELECT 
						DATE_FORMAT(createTime,'%Y-%M') AS Month,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END) AS OfficeMinor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END) AS OfficeMajor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END) AS OfficeCritical,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END) AS OutMinor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END) AS OutMajor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END) AS OutCritical,
						SUM((CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END)) AS Total 
						FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Asia%' 
						GROUP BY Month ;";
            if ($dregion == 'SEA')
                $sql="SELECT 
						DATE_FORMAT(createTime,'%Y-%M') AS Month,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END) AS OfficeMinor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END) AS OfficeMajor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END) AS OfficeCritical,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END) AS OutMinor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END) AS OutMajor,
						SUM(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END) AS OutCritical,
						SUM(
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Minor' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Major' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '8am to 6pm' AND severity='Critical' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Minor' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Major' THEN 1 ELSE 0 END)+
							(CASE WHEN SUBSTRING(workingTimeOfDay, -11, 10)= '6pm to 8am' AND severity='Critical' THEN 1 ELSE 0 END)) AS Total 
						FROM ticketinfo 
						WHERE 
							DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' 
							AND groupName like 'East Asia%' 
						GROUP BY Month ;";
          
            //echo $sql;

            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

	public function actionIssuecategorytrend()
    {

        $model = new Ticketinfo();

        return $this->render('Issuecategorytrend', ['model'=>$model]);

    }

    public function actionShowissuecategorytrend()
    {

        $model = new Ticketinfo();
        
	$model->scenario = Ticketinfo::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            //$sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(issueCategory='System Bug') AS SystemBug,SUM(issueCategory='Not an Issue') AS NoIssue,SUM(issueCategory='System design problem') AS SystemDesignProblem,SUM(issueCategory='Database issue') AS Databaseissue,SUM(issueCategory='Implementation issue') AS Implementationissue,SUM(issueCategory='Report content issue') AS Reportcontentissue,SUM(issueCategory='3rd party Application issue') AS ThirdpartyApplicationissue,SUM(issueCategory='OS issue') AS OSissue,SUM(issueCategory='Network issue') AS Networkissue,SUM(issueCategory='Fusion Call Flow issue') AS FusionCallFlowissue,SUM(issueCategory='Maintenance Issue') AS MaintenanceIssue,SUM(issueCategory='Hardware issue') AS Hardwareissue,SUM(issueCategory='Customer issue') AS Customerissue,SUM(issueCategory='Other issue') AS Otherissue,SUM((issueCategory='System Bug')+(issueCategory='Not an Issue')+(issueCategory='System design problem')+(issueCategory='Database issue')+(issueCategory='Implementation issue')+(issueCategory='Report content issue')+(issueCategory='3rd party Application issue')+(issueCategory='OS issue')+(issueCategory='Network issue')+(issueCategory='Fusion Call Flow issue')+(issueCategory='Maintenance Issue')+(issueCategory='Hardware issue')+(issueCategory='Customer issue')+(issueCategory='Other issue')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month";
            //AND groupName like 'Latam%'
            if ($dregion == '')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(issueCategory='System Bug') AS SystemBug,SUM(issueCategory='Not an Issue') AS NoIssue,SUM(issueCategory='System design problem') AS SystemDesignProblem,SUM(issueCategory='Database issue') AS Databaseissue,SUM(issueCategory='Implementation issue') AS Implementationissue,SUM(issueCategory='Report content issue') AS Reportcontentissue,SUM(issueCategory='3rd party Application issue') AS ThirdpartyApplicationissue,SUM(issueCategory='OS issue') AS OSissue,SUM(issueCategory='Network issue') AS Networkissue,SUM(issueCategory='Fusion Call Flow issue') AS FusionCallFlowissue,SUM(issueCategory='Maintenance Issue') AS MaintenanceIssue,SUM(issueCategory='Hardware issue') AS Hardwareissue,SUM(issueCategory='Customer issue') AS Customerissue,SUM(issueCategory='Other issue') AS Otherissue,SUM((issueCategory='System Bug')+(issueCategory='Not an Issue')+(issueCategory='System design problem')+(issueCategory='Database issue')+(issueCategory='Implementation issue')+(issueCategory='Report content issue')+(issueCategory='3rd party Application issue')+(issueCategory='OS issue')+(issueCategory='Network issue')+(issueCategory='Fusion Call Flow issue')+(issueCategory='Maintenance Issue')+(issueCategory='Hardware issue')+(issueCategory='Customer issue')+(issueCategory='Other issue')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month ;";
            if ($dregion == 'Latam')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(issueCategory='System Bug') AS SystemBug,SUM(issueCategory='Not an Issue') AS NoIssue,SUM(issueCategory='System design problem') AS SystemDesignProblem,SUM(issueCategory='Database issue') AS Databaseissue,SUM(issueCategory='Implementation issue') AS Implementationissue,SUM(issueCategory='Report content issue') AS Reportcontentissue,SUM(issueCategory='3rd party Application issue') AS ThirdpartyApplicationissue,SUM(issueCategory='OS issue') AS OSissue,SUM(issueCategory='Network issue') AS Networkissue,SUM(issueCategory='Fusion Call Flow issue') AS FusionCallFlowissue,SUM(issueCategory='Maintenance Issue') AS MaintenanceIssue,SUM(issueCategory='Hardware issue') AS Hardwareissue,SUM(issueCategory='Customer issue') AS Customerissue,SUM(issueCategory='Other issue') AS Otherissue,SUM((issueCategory='System Bug')+(issueCategory='Not an Issue')+(issueCategory='System design problem')+(issueCategory='Database issue')+(issueCategory='Implementation issue')+(issueCategory='Report content issue')+(issueCategory='3rd party Application issue')+(issueCategory='OS issue')+(issueCategory='Network issue')+(issueCategory='Fusion Call Flow issue')+(issueCategory='Maintenance Issue')+(issueCategory='Hardware issue')+(issueCategory='Customer issue')+(issueCategory='Other issue')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth'AND groupName like 'Latam%' GROUP BY Month ;";
            if ($dregion == 'Asia')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(issueCategory='System Bug') AS SystemBug,SUM(issueCategory='Not an Issue') AS NoIssue,SUM(issueCategory='System design problem') AS SystemDesignProblem,SUM(issueCategory='Database issue') AS Databaseissue,SUM(issueCategory='Implementation issue') AS Implementationissue,SUM(issueCategory='Report content issue') AS Reportcontentissue,SUM(issueCategory='3rd party Application issue') AS ThirdpartyApplicationissue,SUM(issueCategory='OS issue') AS OSissue,SUM(issueCategory='Network issue') AS Networkissue,SUM(issueCategory='Fusion Call Flow issue') AS FusionCallFlowissue,SUM(issueCategory='Maintenance Issue') AS MaintenanceIssue,SUM(issueCategory='Hardware issue') AS Hardwareissue,SUM(issueCategory='Customer issue') AS Customerissue,SUM(issueCategory='Other issue') AS Otherissue,SUM((issueCategory='System Bug')+(issueCategory='Not an Issue')+(issueCategory='System design problem')+(issueCategory='Database issue')+(issueCategory='Implementation issue')+(issueCategory='Report content issue')+(issueCategory='3rd party Application issue')+(issueCategory='OS issue')+(issueCategory='Network issue')+(issueCategory='Fusion Call Flow issue')+(issueCategory='Maintenance Issue')+(issueCategory='Hardware issue')+(issueCategory='Customer issue')+(issueCategory='Other issue')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth'AND groupName like 'Asia%' GROUP BY Month ;";
            if ($dregion == 'SEA')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(issueCategory='System Bug') AS SystemBug,SUM(issueCategory='Not an Issue') AS NoIssue,SUM(issueCategory='System design problem') AS SystemDesignProblem,SUM(issueCategory='Database issue') AS Databaseissue,SUM(issueCategory='Implementation issue') AS Implementationissue,SUM(issueCategory='Report content issue') AS Reportcontentissue,SUM(issueCategory='3rd party Application issue') AS ThirdpartyApplicationissue,SUM(issueCategory='OS issue') AS OSissue,SUM(issueCategory='Network issue') AS Networkissue,SUM(issueCategory='Fusion Call Flow issue') AS FusionCallFlowissue,SUM(issueCategory='Maintenance Issue') AS MaintenanceIssue,SUM(issueCategory='Hardware issue') AS Hardwareissue,SUM(issueCategory='Customer issue') AS Customerissue,SUM(issueCategory='Other issue') AS Otherissue,SUM((issueCategory='System Bug')+(issueCategory='Not an Issue')+(issueCategory='System design problem')+(issueCategory='Database issue')+(issueCategory='Implementation issue')+(issueCategory='Report content issue')+(issueCategory='3rd party Application issue')+(issueCategory='OS issue')+(issueCategory='Network issue')+(issueCategory='Fusion Call Flow issue')+(issueCategory='Maintenance Issue')+(issueCategory='Hardware issue')+(issueCategory='Customer issue')+(issueCategory='Other issue')) AS Total FROM ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth'AND groupName like 'East Asia%' GROUP BY Month ;";
            
            //echo $sql;

            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

	public function actionLevelseveritytrend()
    {

        $model = new Ticketinfo();

        return $this->render('Levelseveritytrend', ['model'=>$model]);

    }

    public function actionShowlevelseveritytrend()
    {

        $model = new Ticketinfo();
        
	$model->scenario = Ticketinfo::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            //$sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L1_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END) AS 'L1_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L1_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L2_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END) AS 'L2_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L2_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L3_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END) AS 'L3_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L3_Critical',SUM((CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END)) AS Total FROM `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month";
            if ($dregion == '')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L1_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END) AS 'L1_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L1_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L2_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END) AS 'L2_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L2_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L3_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END) AS 'L3_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L3_Critical',SUM((CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END)) AS Total FROM `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month ;";
            if ($dregion == 'Latam')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L1_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END) AS 'L1_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L1_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L2_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END) AS 'L2_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L2_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L3_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END) AS 'L3_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L3_Critical',SUM((CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END)) AS Total FROM `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth'AND groupName like 'Latam%' GROUP BY Month;";
            if ($dregion == 'Asia')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L1_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END) AS 'L1_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L1_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L2_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END) AS 'L2_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L2_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L3_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END) AS 'L3_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L3_Critical',SUM((CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END)) AS Total FROM `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth'AND groupName like 'Asia%' GROUP BY Month;";
            if ($dregion == 'SEA')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month,SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L1_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END) AS 'L1_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L1_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L2_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END) AS 'L2_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L2_Critical',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END) AS 'L3_Minor',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END) AS 'L3_Major',SUM(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END) AS 'L3_Critical',SUM((CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L1' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L2' AND severity='Critical' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Minor' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Major' THEN 1 ELSE 0 END)+(CASE WHEN SUBSTRING(groupName, -2, 2) = 'L3' AND severity='Critical' THEN 1 ELSE 0 END)) AS Total FROM `ticketinfo` WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth'AND groupName like 'East Asia%' GROUP BY Month;";
            
            //echo $sql;
            //AND groupName like 'Latam%'
            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }
	
	public function actionTicketvolume()
    {

        $model = new Ticketinfo();

        return $this->render('Ticketvolume', ['model'=>$model]);

    }

    public function actionShowticketvolume()
    {

        $model = new Ticketinfo();
        
	$model->scenario = Ticketinfo::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            //$sql="SELECT DATE_FORMAT(month,'%Y-%M') AS Month, SUM(received) AS Received, SUM(resolved) AS Resolved, SUM(unresolved) AS Unresolved, SUM(received+resolved+unresolved) AS Total from ticketvolume WHERE DATE_FORMAT(month,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Latam%' GROUP BY Month";
            //AND groupName like 'Latam%'
            //echo $sql;
            if ($dregion == '')
                $sql="SELECT DATE_FORMAT(month,'%Y-%M') AS Month, SUM(received) AS Received, SUM(resolved) AS Resolved, SUM(unresolved) AS Unresolved, SUM(received+resolved+unresolved) AS Total from ticketvolume WHERE DATE_FORMAT(month,'%Y-%m') BETWEEN '$smonth' AND '$emonth'  GROUP BY Month";
            if ($dregion == 'Latam')
                $sql="SELECT DATE_FORMAT(month,'%Y-%M') AS Month, SUM(received) AS Received, SUM(resolved) AS Resolved, SUM(unresolved) AS Unresolved, SUM(received+resolved+unresolved) AS Total from ticketvolume WHERE DATE_FORMAT(month,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Latam%' GROUP BY Month";
            if ($dregion == 'Asia')
                $sql="SELECT DATE_FORMAT(month,'%Y-%M') AS Month, SUM(received) AS Received, SUM(resolved) AS Resolved, SUM(unresolved) AS Unresolved, SUM(received+resolved+unresolved) AS Total from ticketvolume WHERE DATE_FORMAT(month,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Asia%' GROUP BY Month";
            if ($dregion == 'SEA')
                $sql="SELECT DATE_FORMAT(month,'%Y-%M') AS Month, SUM(received) AS Received, SUM(resolved) AS Resolved, SUM(unresolved) AS Unresolved, SUM(received+resolved+unresolved) AS Total from ticketvolume WHERE DATE_FORMAT(month,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'East Asia%' GROUP BY Month";
           
            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }
 
}



