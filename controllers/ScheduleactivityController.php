<?php

namespace app\controllers;

use Yii;
use app\models\Scheduleactivity;
use app\models\Createactivity;
use app\models\ScheduleactivitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Mop;

/**
 * ScheduleactivityController implements the CRUD actions for Scheduleactivity model.
 */
class ScheduleactivityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actionSchedule($id){

        $this->layout = 'popup';

        $model = new Scheduleactivity();

        //$activity = new Createactivity();

        $mop = new Mop();

        $model->id =$id;

        $activity = Createactivity::find()->where(['activityId' => $id])->one();
        
        if ($model->load(Yii::$app->request->post())) {

            if($model->activity->mopName){

                $result = Mop::find()->select('techApproval')->asArray()->where(['MOPName' => $model->activity->mop->MOPName])->one();

                $techApproval = $result['techApproval'];
             
                $model->techApproval = $techApproval;

                $model->save();

                    if($model->save()){

                        $model->getSendScheduleAlert();

                        $model->getSendManagerApproveAlert();

            //$activity->updateActivityStatus($id);
                        $connection = Yii::$app->getDb();

                        $command = $connection->createCommand("UPDATE createactivity SET activityStatus = 'Scheduled' WHERE activityId = $id");

                        $command->execute();

			Yii::$app->readWriteLog->readWriteLog("app\models\Scheduleactivity Schedule created successfully",['TicketId'=>$activity->ticketId,'MOP Name'=>$model->activity->mop->MOPName,'Activity'=>$activity->activity],2);

                        return $this->render('scheduleform',['model' => $model]);

                    }else {

                        return $this->render('scheduleform',['model' => $model]);
                    }

            }
            else {

            $model->save();

                if($model->save()){

                    $model->getSendScheduleAlert();

                    $model->getSendManagerApproveAlert();

                    //$activity->updateActivityStatus($id);
                    $connection = Yii::$app->getDb();

                    $command = $connection->createCommand("UPDATE createactivity SET activityStatus = 'Scheduled' WHERE activityId = $id");

                    $command->execute();

		    Yii::$app->readWriteLog->readWriteLog("app\models\Scheduleactivity Schedule created successfully",['TicketId'=>$activity->ticketId,'Activity'=>$activity->activity],2);

                    return $this->render('scheduleform',['model' => $model]);

                }else{

                    return $this->render('scheduleform',['model' => $model]);

                }

            }

        } else{

            return $this->render('scheduleform',['model' => $model]);
        }

    }

    
}
