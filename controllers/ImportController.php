<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\UploadForm;
use yii\web\UploadedFile;
use app\models\Ticketinfo;
use app\models\History;
use app\models\HistorySearch;
use yii\web\NotFoundHttpException;

class ImportController extends Controller
{
     /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
    
        ];
    }

    public function actionImport()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            
            $model->file = UploadedFile::getInstance($model, 'file');
            $filePath = $model->file->tempName;

            if ($model->file) {           
                $model->file->saveAs('uploads/' . $model->file->baseName . '.' . $model->file->extension);
            }
            
            $savedPath = Yii::getAlias('@webroot') . '/uploads/' . $model->file->baseName . '.' . $model->file->extension;

            $name = $model->file->baseName.'.'. $model->file->extension;

            $sql="LOAD DATA LOCAL INFILE '". $savedPath ."' 
                INTO TABLE `ticketinfo` 
                FIELDS TERMINATED BY ',' 
                OPTIONALLY ENCLOSED BY '\"' 
                LINES TERMINATED BY '\\n' 
                IGNORE 1 LINES
                (`ticketId`, `subject`, `status`, `priority`, `source`, `type`, `agent`, `groupName`, `createTime`, `dueTime`, `resolveTime`, `closeTime`, `lastUpdateTime`, `initialResponseTime`, `timeTracked`, `firstResponseTime`, `resolutionTime`, `agentInt`, `cusInt`, `resolutionStatus`, `responseStatus`, `tags`, `surveyResult`,`internalAgent`, `product`,`internalGroup`,`effectiveFirstResponse`,`mgmtEscalated`, `acknowledger`, `productName`, `severity`, `operator`, `issueCategory`, `workingTimeOfDay`, `neutralize`, `resolution`, `fullName`, `title`, `email`, `workPhone`, `mobilePhone`, `twitterId`, `timezone`, `language`, `uniqueExternal`, `facebook`, `contactId`, `companyName`, `companyDomain`, `healthScore`, `accountTier`, `AMCPeriodStart`, `AMCPeriodEnd`, `renewalDate`, `SLAStatus`, `SLACusContact`, `industry`, `resolutionSummary`, `escalationSummary`)
                ";


            $connection = Yii::$app->getDb();
            $transaction = $connection->beginTransaction();

            try {
                $connection->createCommand($sql)->execute();
                $transaction->commit();

                try { 

                    $history = new History();
                    $history->fileName = $name;
                    $history->save();

                } catch (Exception $e) {
                    $transaction->rollBack();
                }

            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }

        //$name = $model->file->baseName.'.'. $model->file->extension;
        $searchModel = new HistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('import', ['model'=>$model, 'searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

}

