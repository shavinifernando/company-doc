<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\NotFoundHttpException;
use app\models\Mop;
use app\models\MopSearch;
use app\controllers\AppController;
use app\models\createForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;



class MopController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
   public function behaviors()
    {
        return [
            
    
        ];
    }

    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        
        return parent::beforeAction($action);
    }

    public function actionMopoperation()
    {
        $searchModel = new MopSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('mopoperation', ['searchModel'=>$searchModel,'dataProvider' => $dataProvider]);
    }

    public function actionCreate(){

        $this->layout = 'popup';

        $model = new Mop();

        if ($model->load(Yii::$app->request->post())) {
           $session = Yii::$app->session;
           $userid = $session['user_id'];
           $model->MOPCreator=(string)$userid;
            if($model->save()){
                Yii::$app->session->setFlash('success', '<B>MOP Record created successfully</B> ');
                Yii::$app->readWriteLog->readWriteLog("app\models\Mop MOP Record created successfully",['TicketId'=>$model->ticketId,'MOP Name'=>$model->MOPName,'MOP Creator'=>$model->creatorsName->firstname],2);
                $model->getSendApprovalAlert();
                $model->ticketId='';
                $model->openDate='';
                $model->MOPCusSharedDate='';
                $model->MOPName='';
                $model->operator='';
                $model->issueDescription='';
                $model->linkToMOP='';
                $model->solution='';
                $model->techApproval='';
                $model->techApprovedBy='';
                $model->approvalStatus='';
                $model->approveComment='';
                return $this->render('createform',['model' => $model]);
            }
            else{

                return $this->render('createform',['model' => $model]);
            }
	        

        } else{

            return $this->render('createform',['model' => $model]);
        }
    }

    public function actionView($id)
    {
        $this->layout = 'popup';

        $model = Mop::findOne($id);

        return $this->render('viewform',['model'=> $model]);
    
    }

    public function actionActivityview($id)
    {
        $this->layout = 'popup';

        $model = Mop::findOne($id);

        return $this->render('activityviewform',['model'=> $model]);
    
    }

    public function actionUpdate($id){

        $this->layout = 'popup';

        $model = Mop::findOne($id);

    if($model->load(Yii::$app->request->post()) && $model->save()){
    Yii::$app->session->setFlash('success', '<B>MOP Record Updated successfully</B> ');
	Yii::$app->readWriteLog->readWriteLog("app\models\Mop MOP Record updated successfully",['TicketId'=>$model->ticketId,'MOP Name'=>$model->MOPName,'MOP Creator'=>$model->creatorsName->firstname],2);

        return $this->render('updateform',['model'=>$model]);

    }
    else{
        return $this->render('updateform',['model'=>$model]);
    }

    }

    public function actionDelete($id){

        $model = Mop::findOne($id);

        if ($model->delete()){

	    Yii::$app->readWriteLog->readWriteLog("app\models\Mop MOP Record deleted",['Ticket ID'=>$model->ticketId,'MOP Name'=>$model->MOPName],2);

            return $this->redirect(['mopoperation']);

        }
    }

    public function actionMopdistribution()
    {
  
        $model = new mop();

        return $this->render('mopdistribution', ['model'=>$model]);
    }
       
    public function actionShowmopdistribution()
    {
        
        $model = new mop();
        
        $model->scenario = mop::SCENARIO_DIST_MOP;
      
         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'startDate');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'endDate');
            $filter = \yii\helpers\ArrayHelper::getValue($_POST, 'filter');

            $sql="SELECT ";

            if ($filter=='')
                    $sql .= "DATE_FORMAT(openDate,'%Y-%M') AS Category,COUNT(mopId) AS Count from mop WHERE DATE_FORMAT(openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Category";

            if ($filter=='Month')
                    $sql .= "DATE_FORMAT(openDate,'%Y-%M') AS Category,COUNT(mopId) AS Count from mop WHERE DATE_FORMAT(openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Category";

            if ($filter=='operator')
                    /*$sql .= "operator AS Category,COUNT(mopId) AS Count from mop WHERE DATE_FORMAT(openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Category";*/
                    $sql .= "o.operatorName AS Category,COUNT(m.mopId) AS Count from mop m, operator o WHERE DATE_FORMAT(m.openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and m.operator = o.operatorId GROUP BY m.operator";

            if ($filter=='MOPCreator')
                    //Pxx $sql .= "c.creatorName AS Category,COUNT(m.mopId) AS Count from mop m, creator c WHERE DATE_FORMAT(m.openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and m.MOPCreator = c.createrId GROUP BY m.MOPCreator";
                     $sql .= "c.firstname AS Category,COUNT(m.mopId) AS Count from mop m, user c WHERE DATE_FORMAT(m.openDate,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and m.MOPCreator = c.id GROUP BY m.MOPCreator";


           $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();
                    foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

		//$data=sort($data);

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

    /*public function actionApprove($id){

        $model = Mop::findOne($id);

        $connection = Yii::$app->getDb();

        $command = $connection->createCommand("UPDATE mop SET techApproval='Approved' WHERE mopId=$id");

        $command->execute();

        return $this->redirect(['mopoperation']);
    }*/

    public function actionApprove($id)
    {

        $this->layout = 'popup';

        $model = Mop::findOne($id);

        if($model->load(Yii::$app->request->post())){

            if($model->techApproval =="1"){

                $model->techApproval = "Approved";

                $model->save();
                Yii::$app->session->setFlash('success', '<B>MOP Record approved successfully</B> ');
		Yii::$app->readWriteLog->readWriteLog("app\models\Mop MOP Approved",['Ticket ID'=>$model->ticketId,'MOP Name'=>$model->MOPName,'Approved By'=>$model->techApprovedBy,'Approval Status'=>$model->techApproval],2);

		$model->getSendApprovalStatusAlert();

                return $this->render('approveform',['model'=>$model]);

            }elseif($model->techApproval =="2"){

                $model->techApproval = "Rejected";

                $model->save();
                Yii::$app->session->setFlash('success', '<B>MOP Record rejected successfully</B> ');
		Yii::$app->readWriteLog->readWriteLog("app\models\Mop MOP Rejected",['Ticket ID'=>$model->ticketId,'MOP Name'=>$model->MOPName,'Approved By'=>$model->techApprovedBy,'Approval Status'=>$model->techApproval],2);

		$model->getSendApprovalStatusAlert();

                return $this->render('approveform',['model'=>$model]);

            }

        }
        else{
            
            return $this->render('approveform',['model'=>$model]);
        }
    
    }

}


