<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\Ticketflow;
use app\controllers\AppController;
use yii\helpers\Url;

class TickettimingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        
        return parent::beforeAction($action);
    }

     public function actionTicketdistribution()
    {
  
        $model = new Ticketflow();

        return $this->render('Ticketdistribution', ['model'=>$model]);
    }

    public function actionShowticketdistribution()
    {
        $model = new Ticketflow();
        
	$model->scenario = Ticketflow::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {
            
            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');
            $dregion = \yii\helpers\ArrayHelper::getValue($_POST, 'region');
            $dfilter = \yii\helpers\ArrayHelper::getValue($_POST, 'filter');
            //$sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month";
            if($dfilter=='Ticket Updated'){
                if($dregion==''){
                    $sql="select ticketid as TicketId,status as Status, Sum(TIME_TO_SEC(duration)) as Duration
                    from ticketflow
                    WHERE DATE_FORMAT(startTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth'
                    GROUP BY TicketId,Status order by ticketID";
                }
                else{
                    $sql="SELECT f.ticketId as TicketId, f.status as Status, Sum(TIME_TO_SEC(f.duration)) as Duration 
                    FROM ticketflow f left join ticketinfo i on i.ticketId=f.ticketID 
                    where DATE_FORMAT(f.startTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and i.operator='$dregion' group by f.ticketID, f.status order by f.ticketID";
                }
            }
            else{
                if($dregion==''){
                    $sql="select f.ticketid as TicketId,f.status as Status, Sum(TIME_TO_SEC(f.duration)) as Duration
                    from ticketflow f left join ticketinfo i on f.ticketid=i.ticketid
                    WHERE DATE_FORMAT(i.CreateTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth'
                    GROUP BY f.TicketId,f.Status order by f.ticketID";
                }
                else{
                    $sql="select f.ticketid as TicketId,f.status as Status, Sum(TIME_TO_SEC(f.duration)) as Duration
                    from ticketflow f left join ticketinfo i on f.ticketid=i.ticketid
                    WHERE DATE_FORMAT(i.CreateTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and i.operator='$dregion' group by f.ticketID, f.status order by f.ticketID";
                }
            }
           
            /*if ($dregion == '')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month ORDER BY FIELD(MONTH,'January','February','March','April','May','June','July','August','September','October','November','December');";
             if ($dregion == 'Latam')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Latam%' GROUP BY Month ORDER BY FIELD(MONTH,'January','February','March','April','May','June','July','August','September','October','November','December');";
             if ($dregion == 'Asia')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Asia%' GROUP BY Month ORDER BY FIELD(MONTH,'January','February','March','April','May','June','July','August','September','October','November','December');";
            if ($dregion == 'SEA')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'East Asia%' GROUP BY Month ORDER BY FIELD(MONTH,'January','February','March','April','May','June','July','August','September','October','November','December');";
            //echo $sql;
*/
            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
     }

    }
    public function actionTicketflow()
    {
  
        $model = new Ticketflow();

        return $this->render('Ticketflow', ['model'=>$model]);
    }
    public function actionShowticketflow()
    {
        $model = new Ticketflow();
        
	$model->scenario = Ticketflow::SCENARIO_DIST;

         if (Yii::$app->request->isPost) 
        {
            
            $model->load(Yii::$app->request->post());

            
            $dticketId = \yii\helpers\ArrayHelper::getValue($_POST, 'id');
            //$sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month";
            $sql="select ticketid as TicketId,status as Status, agent as Agent, TIME_TO_SEC(duration) as Duration
            from ticketflow
            WHERE ticketID='$dticketId'";
            /*if ($dregion == '')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month ORDER BY FIELD(MONTH,'January','February','March','April','May','June','July','August','September','October','November','December');";
             if ($dregion == 'Latam')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Latam%' GROUP BY Month ORDER BY FIELD(MONTH,'January','February','March','April','May','June','July','August','September','October','November','December');";
             if ($dregion == 'Asia')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'Asia%' GROUP BY Month ORDER BY FIELD(MONTH,'January','February','March','April','May','June','July','August','September','October','November','December');";
            if ($dregion == 'SEA')
                $sql="SELECT DATE_FORMAT(createTime,'%Y-%M') AS Month, SUM(groupName='Latam_L1') AS Latam_L1, SUM(groupName='Latam_L2') AS Latam_L2, SUM(groupName='Latam_L3') AS Latam_L3, SUM(groupName='Asia_L1') AS Asia_L1, SUM(groupName='Asia_L2') AS Asia_L2, SUM(groupName='Asia_L3') AS Asia_L3, SUM(groupName='East Asia_L1') AS EastAsia_L1 , SUM(groupName='East Asia_L2') AS EastAsia_L2, SUM(groupName='East Asia_L3') AS EastAsia_L3,SUM((groupName='Latam_L1')+(groupName='Latam_L2')+(groupName='Latam_L3')+(groupName='Asia_L1')+(groupName='Asia_L2')+(groupName='Asia_L3')+(groupName='East Asia_L1')+(groupName='East Asia_L2')+(groupName='East Asia_L3')) AS Total from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m') BETWEEN '$smonth' AND '$emonth' AND groupName like 'East Asia%' GROUP BY Month ORDER BY FIELD(MONTH,'January','February','March','April','May','June','July','August','September','October','November','December');";
           
*/
            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

                print json_encode($data);
                //print_r(Hello);
                exit;

                } catch (Exception $e) {
            }
        }

    }
    
}


