<?php

namespace app\controllers;

use Yii;
use app\models\Createactivity;
use app\models\CreateactivitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use app\models\LoginForm;
use app\models\ContactForm;
use app\controllers\AppController;
use yii\helpers\Url;
use app\models\Scheduleactivity;
use app\models\ArrayHelper;
use yii\helpers\Json;
use app\models\Mop;

/**
 * CreateactivityController implements the CRUD actions for Createactivity model.
 */
class CreateactivityController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function beforeAction($action) 
    {
        $this->enableCsrfValidation = false;
        
        return parent::beforeAction($action);
    }

    public function actionActivityoperation()
    {
        
        $searchModel = new CreateactivitySearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('activityoperation', ['searchModel'=>$searchModel,'dataProvider' => $dataProvider]);


    }

    public function actionCreate(){

        $this->layout = 'popup';

        $model = new Createactivity();

        $mop = new Mop();
         
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

	    Yii::$app->readWriteLog->readWriteLog("app\models\Createactivity Activity Record created successfully",['TicketId'=>$model->ticketId,'Activity'=>$model->activity],2);

            if($model->ticketId && $model->mopName){

                //$result = Mop::find()->select('mopId')->asArray()->where(['ticketId' => $model->ticketId,'MOPName' => $model->mopsName->mopName])->one();

                //$id = $result['mopId'];
             
                //$model->id_mop = $id;
              
                //$model->save();
		$model->id_mop = $model->mopName;
              
                $model->save();
            }

        return $this->render('createform',['model' => $model]);

        } else{

            return $this->render('createform',['model' => $model]);
        }


    }

    public function actionView($id)
    {
        $this->layout = 'popup';

        $model = Createactivity::findOne($id);

        return $this->render('viewform',['model'=> $model]);
    
    }

    public function actionMopview($id)
    {
        $this->layout = 'popup';

        $model = Createactivity::findOne($id);

        return $this->render('mopviewform',['model'=> $model]);
    
    }

    public function actionUpdate($id){

        $this->layout = 'popup';

        // $model = Createactivity::find()->where(['activityId' => $id])->with('scheduleactivity')->one();

        $model = Createactivity::findOne($id);

        $schedule = Scheduleactivity::find()->where(['id' => $model->activityId])->with('activity')->one();
      //  print_r($schedule); exit();
        //print_r($model->activityStatus);

        $prvStatus = $model->activityStatus;
        //print_r($prvStatus);
    if($model->load(Yii::$app->request->post()) && $model->save()){

        if($schedule){
            
            if($schedule->load(Yii::$app->request->post()) && $schedule->save()){
            }
        }

        if($schedule && $prvStatus=="Scheduled" && $model->activityStatus=="To Be Scheduled"){

            $model->sendActivityCancelAlert();

            $connection = Yii::$app->getDb();

            $connection->createCommand()->delete('scheduleactivity', ['id' => $model->activityId])->execute();
        }

        if($schedule && $prvStatus=="Scheduled" && $model->activityStatus=="Scheduled"){

            $model->sendActivityRescheduleAlert();

            $connection = Yii::$app->getDb();

            $connection->createCommand()->update('scheduleactivity', ['preparationReminder' => 0,'closingReminder' =>0,'outcomeReminder' =>0,'statusAlert' =>0], ['id' => $model->activityId])->execute();

        }

        if($schedule && ($prvStatus=="Successfully Completed" or $prvStatus=="Partially Completed" or $prvStatus=="Failed") && ($model->activityStatus=="Successfully Completed" or $model->activityStatus=="Partially Completed" or $model->activityStatus=="Failed")){

            $model->prvStatus = $prvStatus;

            $model->save();

	    $connection = Yii::$app->getDb();

            $connection->createCommand()->update('scheduleactivity', ['statusAlert' =>0], ['id' => $model->activityId])->execute();
            
            $model->sendCompletionStatusUpdate();

        }

        if($schedule && $prvStatus=="Scheduled" && ($model->activityStatus=="Successfully Completed" or $model->activityStatus=="Partially Completed" or $model->activityStatus=="Failed")){

            $model->sendStatusUpdateAlert();

        }

	Yii::$app->readWriteLog->readWriteLog("app\models\Createactivity Activity Record updated successfully",['TicketId'=>$model->ticketId,'Activity'=>$model->activity],2);

        return $this->render('updateform',['model'=>$model, 'schedule' => $schedule]);
    }
    else{

        return $this->render('updateform',['model'=>$model,'schedule' => $schedule]);
        
        }

    }

	public function actionDeleting($id)
    {
        
        $model = Createactivity::findOne($id);
        $this->layout = 'popup';
        if (Yii::$app->request->isPost)
        {
                $connection = Yii::$app->getDb();
            
                $command = $connection->createCommand("UPDATE createactivity SET activityStatus = 'Canceled' WHERE activityId = $id");
                $command->execute();
                Yii::$app->readWriteLog->readWriteLog("app\models\Createactivity Activity canceled successfully",['TicketId'=>$model->ticketId,'Activity'=>$model->activity],2);

                return $this->render('successform');      
        }
        else{
            return $this->render('cancelform',['model'=>$model,'id' => $id]);
        }
    }
	
    public function actionGetmopname($ticket_id) {
        /*$res = [];

        if (($field = Yii::$app->request->post('depdrop_parents')) !== null) {
            $ticketId = $field[0];
            //print_r($ticketId);
            $mops        = Mop::getName($ticketId);

            foreach ($mops as $mop) {
                $res [] = [
                    'id'   => $mop->mopId,
                    'name' => $mop->MOPName,
                ];
            }
        }

         return Json::encode(['output' => $res,]);*/

        $res       = "";
        $res       .= "<option value=\"\">MOP Name</option>";
         $mops        = Mop::getName($ticket_id);

            foreach ($mops as $mop) {
            $res .= sprintf('<option value="%s">%s</option>',$mop->mopId, $mop->MOPName);
        }

        return $res;


       
    }

    /*public function actionDelete($id)
    {
        
        $model = Createactivity::findOne($id);

        if ($model->delete()){

        return $this->redirect(['activityoperation']);

        }

        
    }
*/

   public function actionActivitycreation()
    {
  
        $model = new Createactivity();

        return $this->render('activitycreation', ['model'=>$model]);
    }
       
    public function actionShowactivitycreation()
    {
        

        $model = new Createactivity();
        
        $model->scenario = Createactivity::SCENARIO_DIST_ACT;
      
         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'startDate');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'endDate');
            $filter = \yii\helpers\ArrayHelper::getValue($_POST, 'filter');

            $sql="SELECT ";

            if ($filter=='')
                    $sql .= "DATE_FORMAT(s.scheduleTimeSL,'%Y-%M') AS Category,COUNT(a.activityId) AS Count from createactivity a, scheduleactivity s WHERE DATE_FORMAT(s.scheduleTimeSL,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and a.activityId = s.id GROUP BY Category";

            if ($filter=='Month')
                    $sql .= "DATE_FORMAT(s.scheduleTimeSL,'%Y-%M') AS Category,COUNT(a.activityId) AS Count from createactivity a, scheduleactivity s WHERE DATE_FORMAT(s.scheduleTimeSL,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and a.activityId = s.id GROUP BY Category";

            if ($filter=='Customer')
                    $sql .= "o.operatorName AS Category,COUNT(a.activityId) AS Count from createactivity a, scheduleactivity s,operator o WHERE DATE_FORMAT(s.scheduleTimeSL,'%Y-%m') BETWEEN '$smonth' AND '$emonth' and a.activityId = s.id and a.customer = o.operatorId GROUP BY Category";

           $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();
                    foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

    public function actionCompletiondistribution()
    {
  
        $model = new Createactivity();

        return $this->render('completiondistribution', ['model'=>$model]);
    }

    public function actionShowcompletiondistribution()
    {
        

        $model = new Createactivity();
        
        $model->scenario = Createactivity::SCENARIO_DIST_ACT;
      
         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $date = \yii\helpers\ArrayHelper::getValue($_POST, 'date');

            $sql="SELECT activityStatus AS activityStatus,COUNT(activityId) AS Count from createactivity left Join scheduleactivity s on s.id=createactivity.activityId WHERE DATE_FORMAT(s.scheduleTimeSL,'%Y-%m')='$date' AND (activityStatus='Successfully Completed' OR activityStatus='Failed' OR activityStatus='Partially Completed') GROUP BY activityStatus";

            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                //print_r($result);
                //exit;

                $data = array();
                    foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

    public function actionCompletiontrend()
    {
  
        $model = new Createactivity();

        return $this->render('completiontrend', ['model'=>$model]);
    }

    public function actionShowcompletiontrend()
    {

        $model = new Createactivity();
        
        $model->scenario = Createactivity::SCENARIO_DIST_ACT;

         if (Yii::$app->request->isPost) 
        {

            $model->load(Yii::$app->request->post());

            $smonth = \yii\helpers\ArrayHelper::getValue($_POST, 'start');
            $emonth = \yii\helpers\ArrayHelper::getValue($_POST, 'end');

            $sql="SELECT DATE_FORMAT(s.scheduleTimeSL,'%Y-%M') AS Month, SUM(activityStatus='Successfully Completed') AS Success, SUM(activityStatus='Failed') AS Failed, SUM(activityStatus='Partially Completed') AS Partial,SUM((activityStatus='Successfully Completed')+(activityStatus='Failed')+(activityStatus='Partially Completed')) AS Total from createactivity left Join scheduleactivity s on s.id=createactivity.activityId WHERE DATE_FORMAT(s.scheduleTimeSL,'%Y-%m') BETWEEN '$smonth' AND '$emonth' GROUP BY Month";

            //echo $sql;

            $connection = Yii::$app->getDb();
         
            try {
                
                $result = $connection->createCommand($sql)->queryAll();

                $data = array();

                foreach ($result as $row) {
                    $data[] = $row;
                }

                //$data=array_reverse($data);

                print json_encode($data);
                
                exit;

                } catch (Exception $e) {
            }
        }

    }

    public function actionApprove($id)
    {

        $this->layout = 'popup';

        $model = Createactivity::findOne($id);

        $schedule = new Scheduleactivity();

        if($model->load(Yii::$app->request->post())){

            if($model->managerApproval =="1"){

                $model->managerApproval = "Approved";

                $model->save();

		Yii::$app->readWriteLog->readWriteLog("app\models\Mop Set Manager Approval",['Ticket ID'=>$model->ticketId,'Activity'=>$model->activity,'Manager Approval Status'=>$model->managerApproval],2);

                $connection = Yii::$app->getDb();

                $connection->createCommand()->update('scheduleactivity', ['managerApproval' => "Approved"], ['id' => $id])->execute();

                return $this->render('approveform',['model'=>$model]);

            }elseif ($model->managerApproval =="2") {

		Yii::$app->readWriteLog->readWriteLog("app\models\Mop Set Manager Approval",['Ticket ID'=>$model->ticketId,'Activity'=>$model->activity,'Manager Approval Status'=>"Rejected"],2);
                
                $connection = Yii::$app->getDb();

                $connection->createCommand()->delete('scheduleactivity', ['id' => $id])->execute();

                $connection->createCommand()->update('createactivity', ['activityStatus' => "To Be Scheduled"], ['activityId' => $id])->execute();

                return $this->render('approveform',['model'=>$model]);
            }

        }
        else{
            
            return $this->render('approveform',['model'=>$model]);
        }
    
    }
    
}
