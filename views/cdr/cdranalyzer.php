<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
use kartik\widgets\DateTimePicker;
use kartik\select2\Select2;
use app\models\CdrDivision;
use app\models\Cdranalyzer;
use yii\web\Session;
$session = Yii::$app->session;
?>

<section class="content-header">
    <h1>
        Analyser
        <small>CDR Analyzer</small>
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Dashboard</li>
        <li><i class="fa fa-laptop"></i> CDR Analyser</li>
        <li class="active">Analyser</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-body">
                    <?php $form = ActiveForm::begin(['method' => 'post'], ['options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]) ?>
                    <div class="row">
                        <div class="box-body boxpad">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">CDR</label>
                                    <div class="col-sm-12 col-md-11">
                                        <?= $form->field($model, 'cdra', ['options' => ['class' => 'col-sm-12']])->textarea(['rows' => '5', 'maxlength' => true, 'placeholder' => 'Paste the CDR ', 'autocomplete' => 'off'])->label(false); ?>
                                        <!--textarea class="form-control" rows="5" placeholder="Paste or Type CDR"></textarea-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="box-body boxpad">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="file" class="col-sm-2 col-md-2 control-label">Upload CDR</label>
                                    <div class="col-sm-10 col-md-10">
                                        <div class="input-group " name="bulkUpload">
                                            <?= $form->field($model, 'file', ['options' => ['class' => 'form-control']])->fileInput()->label(false) ?>
                                            <span class="input-group-btn">
                                                <button class="btn btn-warning btn-reset" type="button">Reset</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label">Deployment</label>
                                        <?= $form->field($model, 'deployment', ['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => CdrDivision::getDeployment(), 'options' => ['placeholder' => 'Deployment','disabled'=>($session['deployment']!='')? true : false], 'pluginOptions' => ['allowClear' => true]]); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3  control-label">CDR Type</label>
                                    <?= $form->field($model, 'cdrtype', ['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => CdrDivision::getCdrtype(), 'options' => ['placeholder' => 'CDR type','disabled'=>($session['cdrtype']!='')? true : false], 'pluginOptions' => ['allowClear' => true]]); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box-footer">
							
                                <?= Html::submitButton('Insert', ['class' => 'btn btn-primary pull-right btnspace', 'name' => 'Insert', 'value' => 'Insert']) ?>
                                <?= Html::submitButton('Reset', ['class' => 'btn btn-danger pull-right btnspace', 'name' => 'Reset', 'value' => 'Reset']) ?>
								
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
    <?php $array = array(); ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box">
            <?php
                $session = Yii::$app->session;
                $array = explode('\r\n', $session['cdr_val']) ;                                  
             ?>
                <div class="box-header with-border">
                    <h3 class="box-title">CDR Configurations</h3>
                    <?= Html::a('<span class="glyphicon pull-right glyphicon-transfer"></span>', ['#'], ['data-url' => Url::toRoute(['cdr/compare']), 'id' => 'btn-compare', 'title' => 'Compare']); ?>
                </div>
				
                <div class="box-body">
                    <!--form class="form-horizontal" name="form_blacklist_table" id=""-->
                    <div class="row">
                        <div class="box-body boxpad contSeperator">
                            <div class="col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive row">
                                <div class="col-md-12">
                                <table id="analyse-table" class="table table-bordered table-striped table-hover" >
			
                                    <thead>
                                        <tr>
                                            <th class="actions-fix-wd-1 text-center">Select</th>
                                            <th>CDR</th>
                                            <th class="actions-fix-wd-2 text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($array[0]!=''){ ?>
                                        <?php $j = 0 ?>
                                        <?php do{ ?>
                                        <tr>
                                            <td class="text-center">
                                           <input name="checkbox1[]" id="checkbox1" type="checkbox" value="<?= $j ?>">
                                        </td>
                                            <td>
                                                <div>
                                                    <?php
                                                        echo($array["$j"]);
                                                        ?>
                                                </div>
                                            </td>

                                            <td class="text-center actions">
                                                <?= Html::a('<span class="glyphicon glyphicon-check"></span>', ['#'], ['data-url' => Url::toRoute(['cdr/analyze','id'=>$array["$j"]]), 'id' => 'btn-view', 'title' => 'analyze']); ?>
                                            </td>
                                            <?php $j++ ?>
                                        </tr>
                                        <?php }while($j<sizeof($array)-1); ?>
                                        <?php } ?> 

                                    </tbody>
                                </table>
				</div>
			 	</div>
                            </div>
                        </div>
                    </div>
                    <!--/form-->
                </div>
                
            </div>
        </div>
    </div>

</section>
<!-- /.content -->


<!-- View modal -->
<div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">CDR Analyze</h4>
            </div>
            <iframe id="view-modal" name="view" src="" width="100%" height="490" frameborder="0"></iframe>
        </div>
    </div>
</div>

<div class="modal fade" id="ViewComp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel" bgcolor="#ccccff">CDR Compare</h4>
            </div>
            <iframe id="view-comp" name="view" src="" width="100%" height="490" frameborder="0"></iframe>
        </div>
    </div>
</div>


<?php
$urlView = Url::to('cdr/analyze');
$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-view', function(e) {
        $('#view-modal').attr("src", $(this).attr('data-url'));
        $('#ViewModal').modal({show:true})

        return false;
    });
});
JS;
$this->registerJs($script);
?>
<?php
$url = Url::to(["cdr/compare"]);
$script1 = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-compare', function(e) {
       
        var items=document.getElementsByName('checkbox1[]');
        var selectedItems="";
        for(var i=0;i<items.length;i++){
            if(items[i].type=='checkbox' && items[i].checked==true){
                selectedItems+=items[i].value+",";
            }
        }
        
        $('#view-comp').attr("src", "http://10.210.12.140/ops_asst/web/index.php?r=cdr/compare&items="+selectedItems);
        $('#ViewComp').modal({show:true})
        return false;            
});
});
JS;
$this->registerJs($script1);
?>



<!--?php 

$url = Url::to(["cdr/compare"]);
$script1 = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-compare', function(e) {
        $('#view-comp').attr("src", "http://localhost:8080/index.php?r=cdr/compare&items=123,123");
        $('#ViewComp').modal({show:true})
        return false;            
    });
});
JS;
$this->registerJs($script1);
?-->
