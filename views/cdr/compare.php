<?php
use app\models\CdrAllinone;
use app\models\CdrDivision;
use app\models\Cdranalyzer;
use app\models\Cdr_nfe;
use yii\web\Session;
$session = Yii::$app->session;
?>

<div class="modal-body">

<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>
    <form class="form-horizontal">
        <div class="row">
       <?php if($valuex2!=null){ ?> 
        <?php $y=sizeof($valuex1);?>    
        <table class="table">
	
       		 <tr>
		<?php if ($printlabel!=null){ ?>
                    	<th bgcolor="#f5f5f0"><?php print_r($session['deployment']);?></th><th bgcolor="#f5f5f0"><?php print_r($session['cdrtype']) ?></th><th bgcolor="#f5f5f0"></th><th bgcolor="#f5f5f0"></th>
                <?php } else{ ?>
			
		<?php } ?>
		 </tr>
        
        <?php for ($r=0;$r<$y;$r++){ ?> 
                
        <tr>
                <td bgcolor="#dfbf9f"><?php echo $printlabel["$r"]; ?></td>
		<?php if($valuex1["$r"]==$valuex["$r"]){ ?>
			<td bgcolor="#f9f2ec"><?php echo $valuex1["$r"]; ?></td>
			<td bgcolor="#f9f2ec"><?php echo $valuex["$r"]; ?> </td>
			<td bgcolor="#f9f2ec"><?php echo $valuex2["$r"]; ?> </td>
		<?php }else{ ?>               
			<td bgcolor="#e6ccb3"><?php echo $valuex1["$r"]; ?></td>
                	<td bgcolor="#ecd9c6"><?php echo $valuex["$r"]; ?></td>
			<td bgcolor="#f2e6d9"><?php echo $valuex2["$r"]; ?> </td>
		<?php } ?>
                </tr>
    
                <?php }}else{ ?>
			  <?php $y=sizeof($valuex1);?>    
        <table class="table">

                 <tr>
                <?php if ($printlabel!=null){ ?>
                        <th bgcolor="#f5f5f0"><?php print_r($session['deployment']);?></th><th bgcolor="#f5f5f0"><?php print_r($session['cdrtype']) ?></th><th bgcolor="#f5f5f0"></th>
                <?php } else{ ?>

                <?php } ?>
                 </tr>

        <?php for ($r=0;$r<$y;$r++){ ?>

        <tr>
                <td bgcolor="#dfbf9f"><?php echo $printlabel["$r"]; ?></td>
                <?php if($valuex1["$r"]==$valuex["$r"]){ ?>
                        <td bgcolor="#f9f2ec"><?php echo $valuex1["$r"]; ?></td>
                        <td bgcolor="#f9f2ec"><?php echo $valuex["$r"]; ?> </td>
                <?php }else{ ?>
                        <td bgcolor="#ecd9c6"><?php echo $valuex1["$r"]; ?></td>
                        <td bgcolor="#f2e6d9"><?php echo $valuex["$r"]; ?></td>
                <?php } ?>
                </tr>

		<?php }} ?>
            </table>
        </div>
    </form>
</div>
