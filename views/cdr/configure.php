<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use app\models\CdrDivision;
use app\models\Cdrconfiguration;
$session = Yii::$app->session;

?>

<div class="modal-body">
    <form class="form-horizontal">
        <div class="row">
            <div class="box-body boxpad">
                <table class="table">
                    <h4><b><?php echo ($cdx['0']);
                            echo ('-');
                            echo ($cdx['1']); ?></b></h4>
                    <tr>
                        <th>Description</th>
                        <th style="width:150px;" class="text-center">Actions</th>
                    </tr>
                    <?php for ($i = 0; $i < sizeof($lavels2); $i++) { ?>
                        <tr>
                        <td>
                        <?php print_r($lavels2["$i"]);
                        $id=$lavels2["$i"].'|'.$cdx['0'].'|'.$cdx['1']; ?>
                        </td>
                        <td class="text-center">
                        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['#'], ['data-url' => Url::toRoute(['cdr/cdrcon','id'=>$id]), 'id' => 'btn-view', 'title' => 'con']); ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>

            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Meaning of Field</h4>
			</div>
			<iframe id="view-modal" name="view" src="" width="100%" height="490" frameborder="0"></iframe>
		</div>
	</div>
</div>




<?php

$urlView = Url::to('cdr/cdrcon');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-view', function(e) {
        $('#view-modal').attr("src", $(this).attr('data-url'));
        $('#ViewModal').modal({show:true})

        return false;
    });
});
JS;

$this->registerJs($script);
?>