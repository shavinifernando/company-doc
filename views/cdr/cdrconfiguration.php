<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
use kartik\widgets\DateTimePicker;
use kartik\select2\Select2;
use app\models\CdrDivision;
use app\models\Cdrconfiguration;
$session = Yii::$app->session;

?>


<section class="content-header">
	<h1>
		Configuration
		<small>CDR Configuration</small>
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-dashboard"></i> Dashboard</li>
		<li><i class="fa fa-laptop"></i> CDR Configuration</li>
		<li class="active">Configuration</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="box">
				<div class="box-body">
					<?php $form = ActiveForm::begin(['method' => 'post'], ['options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]) ?>
					<br>
					<div class="row">

						<div class="col-md-6 ">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Deployment</label>
								<?= $form->field($model, 'deployment', ['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => CdrDivision::getDeployment(), 'options' => ['placeholder' => 'Deployment'], 'pluginOptions' => ['allowClear' => true]]); ?>

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-3  control-label">CDR Type</label>
								<?= $form->field($model, 'cdrtype', ['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => CdrDivision::getCdrtype(), 'options' => ['placeholder' => 'CDR type'], 'pluginOptions' => ['allowClear' => true]]); ?>

							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="box-footer">
							<?= Html::submitButton('Search', ['class' => 'btn btn-danger pull-right btnspace', 'name' => 'Insert', 'id' => 'insert']) ?>

						</div>
					</div>
				</div>
				<?php ActiveForm::end() ?>

			</div>
		</div>
	</div>
	


	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Search Results</h3>
				</div>
				<div class="box-body">
						<div class="row">
							<div class="box-body boxpad contSeperator">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="table-responsive row">
										<div class="col-md-12">
											<table id="conf-table" class="table table-bordered table-striped table-hover">
												<thead>
													<tr>
														<th>Deployment</th>
														<th>CDR</th>
														<th class="text-center actions-fix-wd-2">Action</th>

													</tr>
												</thead>
												<tbody>
												
													<?php for ($i=0;$i<sizeof($depz1);$i++){ ?>
													<tr>
														
														<td><?php echo $depz1["$i"]; ?> </td>
														<td><?php echo $cdrz1["$i"];?> </td>
														
														<td class="text-center actions">
														<?php $r=$depz1["$i"].'|'.$cdrz1["$i"]; ?>
														<?= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['#'], ['data-url' => Url::toRoute(['cdr/configure','id'=>$r]), 'id' => 'btn-view', 'title' => 'configure']); ?>
														</td>
													</tr>
												<?php } ?>
												</tbody>
											</table>											
										</div>
									</div>
								</div>
							</div>
						</div>
					
				</div>
			</div>
		</div>
	</div>
</section>


<!-- View modal -->
<div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">CDR Configuration</h4>
			</div>
			<iframe id="view-modal" name="view" src="" width="100%" height="490" frameborder="0"></iframe>
		</div>
	</div>
</div>


<!-- /.content-wrapper -->

<!--?php include("_includes/scripts.php"); ?-->

<!--?php include("_includes/_footer.php");?-->


<!-- ./wrapper -->

<!--script src="js/cdr_analyser.js"></script-->


<?php

$urlView = Url::to('cdr/configure');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-view', function(e) {
        $('#view-modal').attr("src", $(this).attr('data-url'));
        $('#ViewModal').modal({show:true})

        return false;
    });
});
JS;

$this->registerJs($script);
?>