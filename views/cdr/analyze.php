<?php
use app\models\CdrAllinone;
use app\models\CdrDivision;
use app\models\Cdranalyzer;
use app\models\Cdr_nfe;
use yii\web\Session;
$session = Yii::$app->session;
?>

<div class="modal-body">


<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>
    <form class="form-horizontal">
        <div class="row">
        
        <?php $y=sizeof($printlabel);?>    
        <table class="table">
        <tr>
                    <th bgcolor="#f5f5f0"><?php print_r($session['deployment']) ?></th>
                    <th bgcolor="#f5f5f0"><?php print_r($session['cdrtype']) ?></th>
                </tr>
        
        <?php for ($r=0;$r<$y;$r++){ ?> 
                
                <tr>
                    <td bgcolor="#d6d6c2"><?php echo $printlabel["$r"]; ?></td>
                    <td bgcolor="#ebebe0"><?php echo $valuex["$r"]; ?></td>
                </tr>
    
                <?php } ?>
            </table>
        </div>
    </form>
</div>
