<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
$this->title = 'Operations Assistance Portal | Wavenet';

?>
<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Working Time Based
					<small>Monthly Reports (Tickets)</small>
				</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> Dashboard</li>
					<li><i class="fa fa-laptop"></i> Performance</li>
                    <li><i class=""></i> Monthly Reports (Tickets)</li>
					<li class="active">Working Time Based</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
                            <div class="box-header with-border">
								<h3 class="box-title">Filters - Month</h3>
							</div>

							<div class="box-body">
								<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'form-horizontal']]) ?>
									<div class="row">
										<div class="box-body boxpad">
                                            <div id="start_datetime-tr" class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Month <span
                                                            class="req-fld">*</span>
													</label>
																<?=$form->field($model, 'date',['options' => ['class' => 'col-sm-8','id' => 'date']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM','data-toggle' => 'tooltip','data-placement'=>'bottom','title'=>'Ticket Creation Date'],
                                                                    'pluginOptions' => [
                                                                        'autoclose' => true,
                                                                        'startView'=>'year',
                                                                        'minViewMode'=>'months',
                                                                        'readonly'=>'true',
                                                                        'format' => 'yyyy-mm'
                                                                        ]
                                                                    ]);?>
                                                </div>
                                            </div>
											<div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Region <span
                                                            class="req-fld">*</span>
                                                    </label>
                                                         <?= $form->field($model, 'region',['options' => ['class' => 'col-sm-9','id'=>'region']])->label(false)->widget(Select2::classname(), ['data' => ['Latam' => 'Latam', 'Asia' => 'Asia','SEA' => 'SEA'],'options' => ['placeholder' => 'Region'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                    </div>
                                            </div>
										</div>
									</div>
									<div class="row">
										<div class="box-body boxpad">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="box-footer">
														<?= Html::button('Generate Graph', ['class' => 'btn btn-primary pull-right btnspace','name' => 'graph', 'id' => 'graph']) ?>
												</div>
											</div>
										</div>						
									</div>
								<?php ActiveForm::end() ?>
							</div>
						</div>
					</div>
				</div>
			<div id="data_area">
                <div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">Distribution Graph</h3>
							</div>
							<div class="box-body">
								<canvas id="mycanvas"></canvas>
							</div>
						</div>
					</div>
                </div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">Report Data</h3>
							</div>
							<div class="box-body">
								<form class="form-horizontal" name="form_blacklist_table" id="">
									<div class="row">
										<div class="box-body boxpad contSeperator">
											<div id='table-parent' class="col-md-12 col-sm-12 col-xs-12">
												<table id="table" class="table table-bordered table-striped table-hover"></table>
											</div>
										</div>						
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			</section>
			<!-- /.content -->
<?php
$url = Url::to(['ticketinfo/showworkingtimedistribution']);
//echo $url;
$script = <<< JS
$("#graph").click(function() {
    $('html, body').animate({
        scrollTop: $("#data_area").offset().top
    }, 1000);
    });
	$(document).ready(function(){

	$('#graph').bind("click", function () {

  		$.ajax({
    			url : "{$url}",
    			type : "POST",
				data: { date: $('#ticketinfo-date').val(), region: $('#ticketinfo-region').val()},
    			success : function(data){
      			console.log(data);
      			var data = JSON.parse(data);

      			var severity = [];
      			var Office = [];
      			var Out = [];
                var Total = [];

      			for(var i in data) {
        			severity.push(data[i].severity);
        			Office.push(parseInt(data[i].Office));
        			Out.push(parseInt(data[i].Out));
                    Total.push(parseInt(data[i].Total));
      			}

      			var chartdata = {
        			labels: severity,
        			datasets: [
          			{
            				label: "Office Time",
            				fill: false,
            				lineTension: 0.1,
            				backgroundColor: "rgba(54, 162, 235, 0.8)",
            				borderColor: "rgba(54, 162, 235, 1)",
            				pointHoverBackgroundColor: "rgba(54, 162, 235, 1)",
            				pointHoverBorderColor: "rgba(54, 162, 235, 1)",
            				borderWidth: 1,
            				data: Office
          			},
          			{
            				label: "Out of Office",
            				fill: false,
            				lineTension: 0.1,
            				backgroundColor: "rgba(255, 99, 132, 0.8)",
            				borderColor: "rgba(255,99,132,1)",
            				pointHoverBackgroundColor: "rgba(255,99,132,1)",
            				pointHoverBorderColor: "rgba(255,99,132,1)",
            				borderWidth: 1,
            				data: Out
 	  			}
        				]
      				};

      				var ctx = $("#mycanvas");

                    if(window.bar != undefined)
                
                        window.bar.destroy();

      				window.bar = new Chart(ctx, {
        				type: 'bar',
        				data: chartdata,
        				options: {
                            responsive: true,
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Severity'
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Ticket Count'
                                    }
                                }]
                            }
                        }
      				});

            document.getElementById("table").innerHTML = "";

            if(data.length != 0){

      		var col = [];
        	for (var i = 0; i < data.length; i++) {
            	for (var key in data[i]) {
                	if (col.indexOf(key) === -1) {
                    	col.push(key);
                	}
            	}
        	}

      		var table = document.getElementById("table");

        	var tr = table.insertRow(-1); 

        	for (var i = 0; i < col.length; i++) {
            	var th = document.createElement("th"); 
            	th.innerHTML = col[i];
            	tr.appendChild(th);
        	}

    
        	for (var i = 0; i < data.length; i++) {

            	tr = table.insertRow(-1);

            	for (var j = 0; j < col.length; j++) {
                	var tabCell = tr.insertCell(-1);
                	tabCell.innerHTML = data[i][col[j]];
            	}
        	}

                var tr = table.insertRow(-1); 
                var td = document.createElement("td");
                td.innerHTML = "Total";
                tr.appendChild(td);

                for (var j = 1; j < col.length; j++) {

                    sum = 0;    

                    for (var i = 0; i < data.length; i++) {

                        var td = document.createElement("td");
                        var sum = parseInt(sum) + parseInt(data[i][col[j]]);
                    }
                        td.innerHTML = sum;
                        tr.appendChild(td);
                }

        	   $("#table-parent").append(table);
                
            }else
            {
                
                alert("Data not Found");
                document.getElementById("table").innerHTML = "";
                document.getElementById("mycanvas").innerHTML = "";
                
            }

    			},
    				error : function(data) {
    			}
  			});
		});
	});

JS;

$this->registerJs($script);
?>
