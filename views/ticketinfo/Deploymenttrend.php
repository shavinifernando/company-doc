<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use dosamigos\datepicker\DateRangePicker;

$this->title = 'Operations Assistance Portal | Wavenet';

?>
<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Deployment Wise Ticket
					<small>Trend Graphs (Tickets)</small>
				</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> Dashboard</li>
					<li><i class="fa fa-laptop"></i> Performance</li>
                    <li><i class=""></i> Trend Graphs (Tickets)</li>
					<li class="active">Deployment Wise Ticket</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
                            <div class="box-header with-border">
								<h3 class="box-title">Filters</h3>
							</div>

							<div class="box-body">
								<!-- <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data"  name="form_blacklist" id="form_blacklist"> -->
								<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'form-horizontal']]) ?>
									<div class="row">
										<div class="box-body boxpad">
                                            <div id="start_datetime-tr" class="col-md-5">
                                                <div class="form-group">
												<label class="col-sm-3 control-label">Date Range <span
                                                            class="req-fld">*</span>
													</label>
                                                    <!-- <div class="col-sm-9"> -->
														<!-- <div class="form-group"> -->
														<?= $form->field($model, 'start',['options' => ['class' => 'col-sm-9','id' => 'end']])->label(false)->widget(DateRangePicker::className(), ['options' => ['placeholder' => 'From'],
																'attributeTo' => 'end', 
																'form' => $form, // best for correct client validation
																'clientOptions' => [
																	'autoclose' => true,
																	'format' => 'yyyy-mm',
																	'startView'=>'year',
																	'minViewMode'=>'months',
																	'readonly'=>'true',
																	'pickButtonIcon' => 'glyphicon glyphicon-time',
																]
															]);?>
														<!-- </div> -->

                                                    <!-- </div> -->
                                                </div>
                                            </div>
											<div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Region <span
                                                            class="req-fld">*</span>
                                                    </label>
                                                         <?= $form->field($model, 'region',['options' => ['class' => 'col-sm-9','id'=>'region']])->label(false)->widget(Select2::classname(), ['data' => ['Latam' => 'Latam', 'Asia' => 'Asia','SEA' => 'SEA','Others'=> 'Others'],'options' => ['placeholder' => 'Region'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                    </div>
                                            </div>

										</div>
									</div>
									<div class="row">
										<div class="box-body boxpad">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="box-footer">
														<?= Html::button('Generate Graph', ['class' => 'btn btn-primary pull-right btnspace','name' => 'graph', 'id' => 'graph']) ?>
											        	<!-- <input type="submit" class="btn btn-primary pull-right btnspace" name="submit" id="gen_graph" value="Generate Graph" > -->
												</div>
											</div>
										</div>						
									</div>
								<?php ActiveForm::end() ?>
								<!-- </form> -->
							</div>
						</div>
					</div>
				</div>

				<div id="data_area">
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Deployment Trend Graph</h3>
								</div>
								<div class="box-body">
									<canvas id="mycanvas"></canvas>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Report Data</h3>
								</div>
								<div class="box-body">
									<form class="form-horizontal" name="form_blacklist_table" id="">
										<div class="row">
											<div class="box-body boxpad contSeperator">
												<div id='table-parent' class="col-md-12 col-sm-12 col-xs-12" style="overflow:scroll;width:100%;overflow:auto">
													<table id="table" class="table table-bordered table-striped table-hover">
													</table>
												</div>
											</div>						
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

				</div>
                
			
			</section>
			<!-- /.content -->
<?php
$url = Url::to(['ticketinfo/showdeploymenttrend']);
//echo $url;
$script = <<< JS
    $("#graph").click(function() {
    $('html, body').animate({
        scrollTop: $("#data_area").offset().top
    }, 1000);
    });
    
	$(document).ready(function(){

	$('#graph').bind("click", function () {

  		$.ajax({
    			url : "{$url}",
    			type : "POST",
				data: { start: $('#ticketinfo-start').val(), end: $('#ticketinfo-end').val(), region: $('#ticketinfo-region').val()},
    			success : function(data){
      			console.log(data);
      			var data = JSON.parse(data);

      			var Month = [];
                if($('#ticketinfo-region').val()=='' || $('#ticketinfo-region').val()=='Asia'){
                    var SriLankaHutchison = [];
                    var SriLankaTelecom = [];
                    var MaldivesOoredoo = [];
                    var IndiaIAF = [];
                    var IndiaTATA = [];
                    var KenyaSafaricom = [];
                    var TahitiVini = [];
                }
                if($('#ticketinfo-region').val()=='' || $('#ticketinfo-region').val()=='SEA'){
                    var ThailandDTAC = [];
                    var ThailandTrueMove = [];
                    var CellcardCambodia = [];
		    var SmartCambodia = [];
                }
                if($('#ticketinfo-region').val()=='' || $('#ticketinfo-region').val()=='Latam'){
                    var BoliviaTigo = [];
                    var HondurasTigo = [];
                    var ColombiaTigo = [];
                    var SalvadorTigo = [];
                    var ParaguayTigo = [];
                    var GuatemalaTigo = [];
                }
                if($('#ticketinfo-region').val()=='' || $('#ticketinfo-region').val()=='Others'){
                    var SriLankaEtisalat = [];           
                    var SriLankaAirtel = [];
                    var GhanaMillicom = [];
                    var ChadMillicom = [];
                    var SenagalMillicom = [];
                    var TanzaniaMillicom = [];
                    var NewZealandVodafone = [];
                    var IntelliAustralia = [];
                }
                var Total = [];


      			for(var i in data) {
                        Month.push(data[i].Month);
                        /*if($('#ticketinfo-region').val()=='Asia'){
                            Put if condition to change the region...
                        }*/
                        if($('#ticketinfo-region').val()=='' || $('#ticketinfo-region').val()=='Asia'){
                            SriLankaHutchison.push(data[i].SriLankaHutchison);
                            SriLankaTelecom.push(data[i].SriLankaTelecom);
                            MaldivesOoredoo.push(data[i].MaldivesOoredoo);
                            IndiaIAF.push(data[i].IndiaIAF);
                            IndiaTATA.push(data[i].IndiaTATA);
                            KenyaSafaricom.push(data[i].KenyaSafaricom);
                            TahitiVini.push(data[i].TahitiVini);
                        }
                        if($('#ticketinfo-region').val()=='' || $('#ticketinfo-region').val()=='SEA'){
                            ThailandDTAC.push(data[i].ThailandDTAC);
                            ThailandTrueMove.push(data[i].ThailandTrueMove);
                            CellcardCambodia.push(data[i].CellcardCambodia);
			    SmartCambodia.push(data[i].SmartCambodia);
                        }
                        if($('#ticketinfo-region').val()=='' || $('#ticketinfo-region').val()=='Latam'){
                            BoliviaTigo.push(data[i].BoliviaTigo);
                            //TahitiVini.push(data[i].TahitiVini);
                            HondurasTigo.push(data[i].HondurasTigo);
                            ColombiaTigo.push(data[i].ColombiaTigo);
                            SalvadorTigo.push(data[i].SalvadorTigo);
                            ParaguayTigo.push(data[i].ParaguayTigo);
                            GuatemalaTigo.push(data[i].GuatemalaTigo);
                        }
                        if($('#ticketinfo-region').val()=='' || $('#ticketinfo-region').val()=='Others'){
                            TanzaniaMillicom.push(data[i].TanzaniaMillicom);
                            GhanaMillicom.push(data[i].GhanaMillicom);
                            ChadMillicom.push(data[i].ChadMillicom);
                            SenagalMillicom.push(data[i].SenagalMillicom);
                            IntelliAustralia.push(data[i].IntelliAustralia);
                            NewZealandVodafone.push(data[i].NewZealandVodafone);
                            SriLankaEtisalat.push(data[i].SriLankaEtisalat);
                            SriLankaAirtel.push(data[i].SriLankaAirtel);
                        }
                        Total.push(data[i].Total);
                }
                if($('#ticketinfo-region').val()==''){
      		   var chartdata = {
                    labels: Month,
                    datasets: [
                    {
                            label: "Bolivia-Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.2)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            borderWidth: 1,
                            data: BoliviaTigo
                    },
                    {
                            label: "Tanzania-Millicom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(29, 202, 255, 0.2)",
                            borderColor: "rgba(29, 202, 255, 1)",
                            pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
                            pointHoverBorderColor: "rgba(29, 202, 255, 1)",
                            borderWidth: 1,
                            data: TanzaniaMillicom
                    },
                    {
                            label: "Sri Lanka Hutchison",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(100, 80, 54, 0.2)",
                            borderColor: "rgba(100, 80, 54, 1)",
                            pointHoverBackgroundColor: "rgba(100, 80, 54, 1)",
                            pointHoverBorderColor: "rgba(100, 80, 54, 1)",
                            borderWidth: 1,
                            data: SriLankaHutchison
                    },
                    {
                            label: "Sri Lanka Etisalat",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 255, 51, 0.2)",
                            borderColor: "rgba(255, 255, 51, 1)",
                            pointHoverBackgroundColor: "rgba(255, 255, 51, 1)",
                            pointHoverBorderColor: "rgba(255, 255, 51, 1)",
                            borderWidth: 1,
                            data: SriLankaEtisalat
                    },
                    {
                            label: "Sri Lanka Telecom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 51, 0, 0.2)",
                            borderColor: "rgba(0, 51, 0, 1)",
                            pointHoverBackgroundColor: "rgba(0, 51, 0, 1)",
                            pointHoverBorderColor: "rgba(0, 51, 0, 1)",
                            borderWidth: 1,
                            data: SriLankaTelecom
                    },
                    {
                            label: "Sri Lanka Airtel",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 25, 51, 0.2)",
                            borderColor: "rgba(0, 25, 51, 1)",
                            pointHoverBackgroundColor: "rgba(0, 25, 51,, 1)",
                            pointHoverBorderColor: "rgba(0, 25, 51, 1)",
                            borderWidth: 1,
                            data: SriLankaAirtel
                    },
                    {
                            label: "Maldives-Ooredoo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(51, 25, 0, 0.2)",
                            borderColor: "rgba(51, 25, 0, 1)",
                            pointHoverBackgroundColor: "rgba(51, 25, 0, 1)",
                            pointHoverBorderColor: "rgba(51, 25, 0, 1)",
                            borderWidth: 1,
                            data: MaldivesOoredoo
                    },
                    {
                            label: "India-IAF",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 0, 127, 0.2)",
                            borderColor: "rgba(255, 0, 127, 1)",
                            pointHoverBackgroundColor: "rgba(255, 0, 127, 1)",
                            pointHoverBorderColor: "rgba(255, 0, 127, 1)",
                            borderWidth: 1,
                            data: IndiaIAF
                    },
                    {
                            label: "India-TATA",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(51, 0, 0, 0.2)",
                            borderColor: "rgba(51, 0, 0, 1)",
                            pointHoverBackgroundColor: "rgba(51, 0, 0, 1)",
                            pointHoverBorderColor: "rgba(51, 0, 0, 1)",
                            borderWidth: 1,
                            data: IndiaTATA
                    },
                    {
                            label: "Thailand DTAC",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 102, 102, 0.2)",
                            borderColor: "rgba(255, 102, 102, 1)",
                            pointHoverBackgroundColor: "rgba(255, 102, 102, 1)",
                            pointHoverBorderColor: "rgba(255, 102, 102, 1)",
                            borderWidth: 1,
                            data: ThailandDTAC
                    },
                    {
                            label: "Thailand TrueMove",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(204, 0, 0, 0.2)",
                            borderColor: "rgba(204, 0, 0, 1)",
                            pointHoverBackgroundColor: "rgba(204, 0, 0, 1)",
                            pointHoverBorderColor: "rgba(204, 0, 0, 1)",
                            borderWidth: 1,
                            data: ThailandTrueMove
                    },
                    {
                            label: "Ghana-Millicom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(102, 51, 0, 0.2)",
                            borderColor: "rgba(102, 51, 0, 1)",
                            pointHoverBackgroundColor: "rgba(102, 51, 0, 1)",
                            pointHoverBorderColor: "rgba(102, 51, 0, 1)",
                            borderWidth: 1,
                            data: GhanaMillicom
                    },
                    {
                            label: "Tahiti Vini",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(153, 76, 0, 0.2)",
                            borderColor: "rgba(153, 76, 0, 1)",
                            pointHoverBackgroundColor: "rgba(153, 76, 0, 1)",
                            pointHoverBorderColor: "rgba(153, 76, 0, 1)",
                            borderWidth: 1,
                            data: TahitiVini
                    },
                    {
                            label: "Chad Millicom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 153, 51, 0.2)",
                            borderColor: "rgba(255, 153, 51, 1)",
                            pointHoverBackgroundColor: "rgba(255, 153, 51, 1)",
                            pointHoverBorderColor: "rgba(255, 153, 51, 1)",
                            borderWidth: 1,
                            data: ChadMillicom
                    },
                    {
                            label: "Senagal Millicom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(51, 255, 255, 0.2)",
                            borderColor: "rgba(51, 255, 255, 1)",
                            pointHoverBackgroundColor: "rgba(51, 255, 255, 1)",
                            pointHoverBorderColor: "rgba(51, 255, 255, 1)",
                            borderWidth: 1,
                            data: SenagalMillicom
                    },
                    {
                            label: "Kenya Safaricom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(128, 128, 128, 0.2)",
                            borderColor: "rgba(128, 128, 128, 1)",
                            pointHoverBackgroundColor: "rgba(128, 128, 128, 1)",
                            pointHoverBorderColor: "rgba(128, 128, 128, 1)",
                            borderWidth: 1,
                            data: KenyaSafaricom
                    },
                    {
                            label: "New Zealand Vodafone",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 102, 178, 0.2)",
                            borderColor: "rgba(255, 102, 178, 1)",
                            pointHoverBackgroundColor: "rgba(255, 102, 178, 1)",
                            pointHoverBorderColor: "rgba(255, 102, 178, 1)",
                            borderWidth: 1,
                            data: NewZealandVodafone
                    },
                    {
                            label: "Honduras Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(102, 102, 255, 0.2)",
                            borderColor: "rgba(102, 102, 255, 1)",
                            pointHoverBackgroundColor: "rgba(102, 102, 255, 1)",
                            pointHoverBorderColor: "rgba(102, 102, 255, 1)",
                            borderWidth: 1,
                            data: HondurasTigo
                    },
                    {
                            label: "IntelliAustralia",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 51, 102, 0.2)",
                            borderColor: "rgba(0, 51, 102, 1)",
                            pointHoverBackgroundColor: "rgba(0, 51, 102, 1)",
                            pointHoverBorderColor: "rgba(0, 51, 102, 1)",
                            borderWidth: 1,
                            data: IntelliAustralia
                    },
                    {
                            label: "Colombia Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 102, 102, 0.2)",
                            borderColor: "rgba(0, 102, 102, 1)",
                            pointHoverBackgroundColor: "rgba(0, 102, 102, 1)",
                            pointHoverBorderColor: "rgba(0, 102, 102, 1)",
                            borderWidth: 1,
                            data: ColombiaTigo
                    },
                    {
                            label: "Cellcard Cambodia",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(102, 0, 0, 0.2)",
                            borderColor: "rgba(102, 0, 0, 1)",
                            pointHoverBackgroundColor: "rgba(102, 0, 0, 1)",
                            pointHoverBorderColor: "rgba(102, 0, 0, 1)",
                            borderWidth: 1,
                            data: CellcardCambodia
                    },
		    {
                            label: "Smart Cambodia",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(111, 0, 0, 0.2)",
                            borderColor: "rgba(102, 0, 0, 1)",
                            pointHoverBackgroundColor: "rgba(111, 0, 0, 1)",
                            pointHoverBorderColor: "rgba(111, 0, 0, 1)",
                            borderWidth: 1,
                            data: SmartCambodia
                    },
                    {
                            label: "Salvador Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 153, 0, 0.2)",
                            borderColor: "rgba(0, 153, 0, 1)",
                            pointHoverBackgroundColor: "rgba(0, 153, 0, 1)",
                            pointHoverBorderColor: "rgba(0, 153, 0, 1)",
                            borderWidth: 1,
                            data: SalvadorTigo
                    },
		    {
                            label: "Paraguay Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 153, 0, 0.2)",
                            borderColor: "rgba(0, 153, 0, 1)",
                            pointHoverBackgroundColor: "rgba(0, 153, 0, 1)",
                            pointHoverBorderColor: "rgba(0, 153, 0, 1)",
                            borderWidth: 1,
                            data: ParaguayTigo
                    },
		    {
                            label: "Guatemala Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 153, 0, 0.2)",
                            borderColor: "rgba(0, 153, 0, 1)",
                            pointHoverBackgroundColor: "rgba(0, 153, 0, 1)",
                            pointHoverBorderColor: "rgba(0, 153, 0, 1)",
                            borderWidth: 1,
                            data: GuatemalaTigo
                    }
                ]
            };
        }

/////////////////////////////////////////////////////////////

if($('#ticketinfo-region').val()=='SEA'){
        var chartdata = {
                    labels: Month,
                    datasets: [
                        {
                            label: "Thailand DTAC",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 102, 102, 0.2)",
                            borderColor: "rgba(255, 102, 102, 1)",
                            pointHoverBackgroundColor: "rgba(255, 102, 102, 1)",
                            pointHoverBorderColor: "rgba(255, 102, 102, 1)",
                            borderWidth: 1,
                            data: ThailandDTAC
                    },
                    {
                            label: "Thailand TrueMove",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(204, 0, 0, 0.2)",
                            borderColor: "rgba(204, 0, 0, 1)",
                            pointHoverBackgroundColor: "rgba(204, 0, 0, 1)",
                            pointHoverBorderColor: "rgba(204, 0, 0, 1)",
                            borderWidth: 1,
                            data: ThailandTrueMove
                    },
                    {
                            label: "Cellcard Cambodia",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(102, 0, 0, 0.2)",
                            borderColor: "rgba(102, 0, 0, 1)",
                            pointHoverBackgroundColor: "rgba(102, 0, 0, 1)",
                            pointHoverBorderColor: "rgba(102, 0, 0, 1)",
                            borderWidth: 1,
                            data: CellcardCambodia
                    },
		    {
                            label: "Smart Cambodia",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(111, 0, 0, 0.2)",
                            borderColor: "rgba(102, 0, 0, 1)",
                            pointHoverBackgroundColor: "rgba(111, 0, 0, 1)",
                            pointHoverBorderColor: "rgba(111, 0, 0, 1)",
                            borderWidth: 1,
                            data: SmartCambodia
                    },


                ]
            };
                            }
/////////////////////////////////////////
if($('#ticketinfo-region').val()=='Latam'){
var chartdata = {
                    labels: Month,
                    datasets: [
                        {
                            label: "Honduras Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(102, 102, 255, 0.2)",
                            borderColor: "rgba(102, 102, 255, 1)",
                            pointHoverBackgroundColor: "rgba(102, 102, 255, 1)",
                            pointHoverBorderColor: "rgba(102, 102, 255, 1)",
                            borderWidth: 1,
                            data: HondurasTigo
                    },
                    {
                            label: "Bolivia-Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(59, 89, 152, 0.2)",
                            borderColor: "rgba(59, 89, 152, 1)",
                            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                            borderWidth: 1,
                            data: BoliviaTigo
                    },
                    {
                            label: "Colombia Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 102, 102, 0.2)",
                            borderColor: "rgba(0, 102, 102, 1)",
                            pointHoverBackgroundColor: "rgba(0, 102, 102, 1)",
                            pointHoverBorderColor: "rgba(0, 102, 102, 1)",
                            borderWidth: 1,
                            data: ColombiaTigo
                    },
                   
                    {
                            label: "Salvador Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 153, 0, 0.2)",
                            borderColor: "rgba(0, 153, 0, 1)",
                            pointHoverBackgroundColor: "rgba(0, 153, 0, 1)",
                            pointHoverBorderColor: "rgba(0, 153, 0, 1)",
                            borderWidth: 1,
                            data: SalvadorTigo
                    },
		            {
                            label: "Paraguay Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 153, 0, 0.2)",
                            borderColor: "rgba(0, 153, 0, 1)",
                            pointHoverBackgroundColor: "rgba(0, 153, 0, 1)",
                            pointHoverBorderColor: "rgba(0, 153, 0, 1)",
                            borderWidth: 1,
                            data: ParaguayTigo
                    },
		            {
                            label: "Guatemala Tigo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 153, 0, 0.2)",
                            borderColor: "rgba(0, 153, 0, 1)",
                            pointHoverBackgroundColor: "rgba(0, 153, 0, 1)",
                            pointHoverBorderColor: "rgba(0, 153, 0, 1)",
                            borderWidth: 1,
                            data: GuatemalaTigo
                    }
                ]
            };
                            }
///////////////////////////////////////////////

if($('#ticketinfo-region').val()=='Asia'){
                var chartdata = {
                    labels: Month,
                    datasets: [
                        {
                            label: "Sri Lanka Hutchison",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(100, 80, 54, 0.2)",
                            borderColor: "rgba(100, 80, 54, 1)",
                            pointHoverBackgroundColor: "rgba(100, 80, 54, 1)",
                            pointHoverBorderColor: "rgba(100, 80, 54, 1)",
                            borderWidth: 1,
                            data: SriLankaHutchison
                    },
                    {
                            label: "Sri Lanka Telecom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 51, 0, 0.2)",
                            borderColor: "rgba(0, 51, 0, 1)",
                            pointHoverBackgroundColor: "rgba(0, 51, 0, 1)",
                            pointHoverBorderColor: "rgba(0, 51, 0, 1)",
                            borderWidth: 1,
                            data: SriLankaTelecom
                    },
                   
                    {
                            label: "Maldives-Ooredoo",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(51, 25, 0, 0.2)",
                            borderColor: "rgba(51, 25, 0, 1)",
                            pointHoverBackgroundColor: "rgba(51, 25, 0, 1)",
                            pointHoverBorderColor: "rgba(51, 25, 0, 1)",
                            borderWidth: 1,
                            data: MaldivesOoredoo
                    },
                    {
                            label: "India-IAF",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 0, 127, 0.2)",
                            borderColor: "rgba(255, 0, 127, 1)",
                            pointHoverBackgroundColor: "rgba(255, 0, 127, 1)",
                            pointHoverBorderColor: "rgba(255, 0, 127, 1)",
                            borderWidth: 1,
                            data: IndiaIAF
                    },
                    {
                            label: "India-TATA",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(51, 0, 0, 0.2)",
                            borderColor: "rgba(51, 0, 0, 1)",
                            pointHoverBackgroundColor: "rgba(51, 0, 0, 1)",
                            pointHoverBorderColor: "rgba(51, 0, 0, 1)",
                            borderWidth: 1,
                            data: IndiaTATA
                    },
                    {
                            label: "Tahiti Vini",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(153, 76, 0, 0.2)",
                            borderColor: "rgba(153, 76, 0, 1)",
                            pointHoverBackgroundColor: "rgba(153, 76, 0, 1)",
                            pointHoverBorderColor: "rgba(153, 76, 0, 1)",
                            borderWidth: 1,
                            data: TahitiVini
                    },
                    {
                            label: "Kenya Safaricom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(128, 128, 128, 0.2)",
                            borderColor: "rgba(128, 128, 128, 1)",
                            pointHoverBackgroundColor: "rgba(128, 128, 128, 1)",
                            pointHoverBorderColor: "rgba(128, 128, 128, 1)",
                            borderWidth: 1,
                            data: KenyaSafaricom
                    }



                ]
            };
                            }

/////////////////////////////////////////////////////////////

if($('#ticketinfo-region').val()=='Others'){
        var chartdata = {
                    labels: Month,
                    datasets: [
                        {
                            label: "Tanzania-Millicom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(29, 202, 255, 0.2)",
                            borderColor: "rgba(29, 202, 255, 1)",
                            pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
                            pointHoverBorderColor: "rgba(29, 202, 255, 1)",
                            borderWidth: 1,
                            data: TanzaniaMillicom
                    },
                    {
                            label: "Sri Lanka Etisalat",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 255, 51, 0.2)",
                            borderColor: "rgba(255, 255, 51, 1)",
                            pointHoverBackgroundColor: "rgba(255, 255, 51, 1)",
                            pointHoverBorderColor: "rgba(255, 255, 51, 1)",
                            borderWidth: 1,
                            data: SriLankaEtisalat
                    },
                    {
                            label: "Sri Lanka Airtel",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 25, 51, 0.2)",
                            borderColor: "rgba(0, 25, 51, 1)",
                            pointHoverBackgroundColor: "rgba(0, 25, 51,, 1)",
                            pointHoverBorderColor: "rgba(0, 25, 51, 1)",
                            borderWidth: 1,
                            data: SriLankaAirtel
                    },
                    {
                            label: "Ghana-Millicom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(102, 51, 0, 0.2)",
                            borderColor: "rgba(102, 51, 0, 1)",
                            pointHoverBackgroundColor: "rgba(102, 51, 0, 1)",
                            pointHoverBorderColor: "rgba(102, 51, 0, 1)",
                            borderWidth: 1,
                            data: GhanaMillicom
                    },
                    {
                            label: "Chad Millicom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 153, 51, 0.2)",
                            borderColor: "rgba(255, 153, 51, 1)",
                            pointHoverBackgroundColor: "rgba(255, 153, 51, 1)",
                            pointHoverBorderColor: "rgba(255, 153, 51, 1)",
                            borderWidth: 1,
                            data: ChadMillicom
                    },
                    {
                            label: "New Zealand Vodafone",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(255, 102, 178, 0.2)",
                            borderColor: "rgba(255, 102, 178, 1)",
                            pointHoverBackgroundColor: "rgba(255, 102, 178, 1)",
                            pointHoverBorderColor: "rgba(255, 102, 178, 1)",
                            borderWidth: 1,
                            data: NewZealandVodafone
                    },
                    {
                            label: "IntelliAustralia",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(0, 51, 102, 0.2)",
                            borderColor: "rgba(0, 51, 102, 1)",
                            pointHoverBackgroundColor: "rgba(0, 51, 102, 1)",
                            pointHoverBorderColor: "rgba(0, 51, 102, 1)",
                            borderWidth: 1,
                            data: IntelliAustralia
                    },
                    {
                            label: "Senagal Millicom",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(51, 255, 255, 0.2)",
                            borderColor: "rgba(51, 255, 255, 1)",
                            pointHoverBackgroundColor: "rgba(51, 255, 255, 1)",
                            pointHoverBorderColor: "rgba(51, 255, 255, 1)",
                            borderWidth: 1,
                            data: SenagalMillicom
                    }
                ]
            };
                            }
////////////////////////////////////



      				var ctx = $("#mycanvas");

                    if(window.LineGraph != undefined)
                
                        window.LineGraph.destroy();

      				window.LineGraph = new Chart(ctx, {
        				type: 'line',
        				data: chartdata,
        				options: {
        					scales: {
            					xAxes: [{
                					display: true,
                					scaleLabel: {
                    					display: true,
                    					labelString: 'Month'
                					}
            					}],
            					yAxes: [{
                					display: true,
                					scaleLabel: {
                    					display: true,
                    					labelString: 'Ticket Count'
                					},
                				ticks: {
                    				beginAtZero:true
                				}
            				}]
        				}
    				}
      				});

      		document.getElementById("table").innerHTML = "";

      		var col = [];
        	for (var i = 0; i < data.length; i++) {
            	for (var key in data[i]) {
                	if (col.indexOf(key) === -1) {
                    	col.push(key);
                	}
            	}
        	}
        	var table = document.getElementById("table");

        	var tr = table.insertRow(-1); 

        	for (var i = 0; i < col.length; i++) {
            	var th = document.createElement("th"); 
            	th.innerHTML = col[i];
            	tr.appendChild(th);
        	}

    
        	for (var i = 0; i < data.length; i++) {

            	tr = table.insertRow(-1);

            	for (var j = 0; j < col.length; j++) {
                	var tabCell = tr.insertCell(-1);
                	tabCell.innerHTML = data[i][col[j]];
            	}
        	}

            var tr = table.insertRow(-1); 
            var td = document.createElement("td");
            td.innerHTML = "Total";
            tr.appendChild(td);

            for (var j = 1; j < col.length; j++) {

                sum = 0;    

                for (var i = 0; i < data.length; i++) {

                    var td = document.createElement("td");
                    var sum = parseInt(sum) + parseInt(data[i][col[j]]);
                }
                    td.innerHTML = sum;
                    tr.appendChild(td);
            } 

        	$("#table-parent").append(table);
        
			},
    				error : function(data) {

    			}
  			});
		});
	});

JS;

$this->registerJs($script);
?>
