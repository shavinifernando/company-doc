<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use dosamigos\datepicker\DateRangePicker;
$this->title = 'Operations Assistance Portal | Wavenet';

?>
<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Level and Severity Based Ticket
					<small>Trend Graphs (Tickets)</small>
				</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> Dashboard</li>
					<li><i class="fa fa-laptop"></i> Performance</li>
                    <li><i class=""></i> Trend Graphs (Tickets)</li>
					<li class="active">Level Wise Ticket</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
                            <div class="box-header with-border">
								<h3 class="box-title">Filters</h3>
							</div>

							<div class="box-body">
								<!-- <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data"  name="form_blacklist" id="form_blacklist"> -->
								<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'form-horizontal']]) ?>
									<div class="row">
										<div class="box-body boxpad">
                                            <div id="start_datetime-tr" class="col-md-5">
                                                <div class="form-group">
												<label class="col-sm-3 control-label">Date Range <span
                                                            class="req-fld">*</span>
													</label>
                                                    <!-- <div class="col-sm-9"> -->
														<!-- <div class="form-group"> -->
														<?= $form->field($model, 'start',['options' => ['class' => 'col-sm-9','id' => 'end']])->label(false)->widget(DateRangePicker::className(), ['options' => ['placeholder' => 'From'],
																'attributeTo' => 'end', 
																'form' => $form, // best for correct client validation
																'clientOptions' => [
																	'autoclose' => true,
																	'format' => 'yyyy-mm',
																	'startView'=>'year',
																	'minViewMode'=>'months',
																	'readonly'=>'true',
																	'pickButtonIcon' => 'glyphicon glyphicon-time',
																]
															]);?>
														<!-- </div> -->

                                                    <!-- </div> -->
                                                </div>
                                            </div>
											<div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Region <span
                                                            class="req-fld">*</span>
                                                    </label>
                                                         <?= $form->field($model, 'region',['options' => ['class' => 'col-sm-9','id'=>'region']])->label(false)->widget(Select2::classname(), ['data' => ['Latam' => 'Latam', 'Asia' => 'Asia','SEA' => 'SEA'],'options' => ['placeholder' => 'Region'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                    </div>
                                            </div>

										</div>
									</div>
									<div class="row">
										<div class="box-body boxpad">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="box-footer">
														<?= Html::button('Generate Graph', ['class' => 'btn btn-primary pull-right btnspace','name' => 'graph', 'id' => 'graph']) ?>
											        	<!-- <input type="submit" class="btn btn-primary pull-right btnspace" name="submit" id="gen_graph" value="Generate Graph" > -->
												</div>
											</div>
										</div>						
									</div>
								<?php ActiveForm::end() ?>
								<!-- </form> -->
							</div>
						</div>
					</div>
				</div>

				<div id="data_area">
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Level and Severity Based Trend Graph</h3>
								</div>
								<div class="box-body">
									<canvas id="mycanvas"></canvas>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Report Data</h3>
								</div>
								<div class="box-body">
									<form class="form-horizontal" name="form_blacklist_table" id="">
										<div class="row">
											<div class="box-body boxpad contSeperator">
												<div id='table-parent' class="col-md-12 col-sm-12 col-xs-12" style="overflow:scroll;width:100%;overflow:auto">
													<table id="table" class="table table-bordered table-striped table-hover">
													</table>
												</div>
											</div>						
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

				</div>
                
			
			</section>
			<!-- /.content -->
<?php
$url = Url::to(['ticketinfo/showlevelseveritytrend']);
//echo $url;
$script = <<< JS
	$("#graph").click(function() {
    $('html, body').animate({
        scrollTop: $("#data_area").offset().top
    }, 2000);
    });
	$(document).ready(function(){

	$('#graph').bind("click", function () {

  		$.ajax({
    			url : "{$url}",
    			type : "POST",
				data: { start: $('#ticketinfo-start').val(), end: $('#ticketinfo-end').val(), region: $('#ticketinfo-region').val()},
    			success : function(data){
      			console.log(data);
      			var data = JSON.parse(data);

      			var Month = [];
      			var L1_Minor = [];
      			var L1_Major = [];
      			var L1_Critical = [];
      			var L2_Minor = [];
      			var L2_Major = [];
      			var L2_Critical = [];
      			var L3_Minor = [];
      			var L3_Major = [];
      			var L3_Critical = [];
      			var Total = [];

      			for(var i in data) {
        			Month.push(data[i].Month);
        			L1_Minor.push(data[i].L1_Minor);
        			L1_Major.push(data[i].L1_Major);
        			L1_Critical.push(data[i].L1_Critical);
					    L2_Minor.push(data[i].L2_Minor);
       				L2_Major.push(data[i].L2_Major);
        			L2_Critical.push(data[i].L2_Critical);
        			L3_Minor.push(data[i].L3_Minor);
        			L3_Major.push(data[i].L3_Major);
        			L3_Critical.push(data[i].L3_Critical);
        			Total.push(data[i].Total);
      			}

      			var chartdata = {
        			labels: Month,
        			datasets: [
          			{
            				label: "L1 Minor",
            				fill: false,
            				backgroundColor: "rgba(59, 89, 152, 0.2)",
            				borderColor: "rgba(59, 89, 152, 1)",
            				pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
            				pointHoverBorderColor: "rgba(59, 89, 152, 1)",
            				borderWidth: 1,
            				data: L1_Minor
          			},
	  				{
            				label: "L1 Major",
            				fill: false,
            				backgroundColor: "rgba(102, 0, 204, 0.2)",
            				borderColor: "rgba(102, 0, 204, 1)",
            				pointHoverBackgroundColor: "rgba(102, 0, 204, 1)",
            				pointHoverBorderColor: "rgba(102, 0, 204, 1)",
            				borderWidth: 1,
            				data: L1_Major
          			},
          			{ 
            				label: "L1 Critical",
            				fill: false,
            				backgroundColor: "rgba(255, 255, 0, 0.2)",
            				borderColor: "rgba(255, 255, 0, 1)",
            				pointHoverBackgroundColor: "rgba(255, 255, 0, 1)",
            				pointHoverBorderColor: "rgba(255, 255, 0, 1)",
            				borderWidth: 1,
            				data: L1_Critical
          			},
          			{
            				label: "L2 Minor",
            				fill: false,
            				backgroundColor: "rgba(0, 153, 0, 0.2)",
            				borderColor: "rgba(0, 153, 0, 1)",
           	 			pointHoverBackgroundColor: "rgba(0, 153, 0, 1)",
            				pointHoverBorderColor: "rgba(0, 153, 0, 1)",
            				borderWidth: 1,
            				data: L2_Minor
          			},
          			{
            				label: "L2 Major",
            				fill: false,
            				backgroundColor: "rgba(153, 0, 153, 0.2)",
            				borderColor: "rgba(153, 0, 153, 1)",
            				pointHoverBackgroundColor: "rgba(153, 0, 153, 1)",
            				pointHoverBorderColor: "rgba(153, 0, 153, 1)",
            				borderWidth: 1,
            				data: L2_Major
          			},
          			{
            				label: "L2 Critical",
            				fill: false,
            				backgroundColor: "rgba(29, 202, 255, 0.2)",
            				borderColor: "rgba(29, 202, 255, 1)",
            				pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
            				pointHoverBorderColor: "rgba(29, 202, 255, 1)",
            				borderWidth: 1,
            				data: L2_Critical
          			},
          			{
            				label: "L3 Minor",
            				fill: false,
            				backgroundColor: "rgba(51, 0, 25, 0.2)",
            				borderColor: "rgba(51, 0, 25, 1)",
            				pointHoverBackgroundColor: "rgba(51, 0, 25, 1)",
            				pointHoverBorderColor: "rgba(51, 0, 25, 1)",
            				borderWidth: 1,
            				data: L3_Minor
          			},
          			{
            				label: "L3 Major",
            				fill: false,
            				backgroundColor: "rgba(255, 51, 51, 0.2)",
            				borderColor: "rgba(255, 51, 51, 1)",
            				pointHoverBackgroundColor: "rgba(255, 51, 51, 1)",
            				pointHoverBorderColor: "rgba(255, 51, 51, 1)",
            				borderWidth: 1,
            				data: L3_Major
          			},
 	  				{
            				label: "L3 Critical",
            				fill: false,
            				backgroundColor: "rgba(100, 80, 54, 0.2)",
            				borderColor: "rgba(100, 80, 54, 1)",
            				pointHoverBackgroundColor: "rgba(100, 80, 54, 1)",
            				pointHoverBorderColor: "rgba(100, 80, 54, 1)",
            				borderWidth: 1,
            				data: L3_Critical
          			}
        		]
      		};

      				var ctx = $("#mycanvas");

              if(window.LineGraph != undefined)
                
                window.LineGraph.destroy();

      				window.LineGraph = new Chart(ctx, {
        				type: 'line',
        				data: chartdata,
        				options: {
        					scales: {
            					xAxes: [{
                					display: true,
                					scaleLabel: {
                    					display: true,
                    					labelString: 'Month'
                					}
            					}],
            					yAxes: [{
                					display: true,
                					scaleLabel: {
                    					display: true,
                    					labelString: 'Ticket Count'
                					},
                				ticks: {
                    				beginAtZero:true
                				}
            				}]
        				}
    				}
      				});

      		document.getElementById("table").innerHTML = "";

      		var col = [];
        	for (var i = 0; i < data.length; i++) {
            	for (var key in data[i]) {
                	if (col.indexOf(key) === -1) {
                    	col.push(key);
                	}
            	}
        	}
        	var table = document.getElementById("table");

        	var tr = table.insertRow(-1); 

        	for (var i = 0; i < col.length; i++) {
            	var th = document.createElement("th"); 
            	th.innerHTML = col[i];
            	tr.appendChild(th);
        	}

    
        	for (var i = 0; i < data.length; i++) {

            	tr = table.insertRow(-1);

            	for (var j = 0; j < col.length; j++) {
                	var tabCell = tr.insertCell(-1);
                	tabCell.innerHTML = data[i][col[j]];
            	}
        	} 

        	  var tr = table.insertRow(-1); 
            var td = document.createElement("td");
            td.innerHTML = "Total";
            tr.appendChild(td);

            for (var j = 1; j < col.length; j++) {

                sum = 0;    

                for (var i = 0; i < data.length; i++) {

                    var td = document.createElement("td");
                    var sum = parseInt(sum) + parseInt(data[i][col[j]]);
                }
                    td.innerHTML = sum;
                    tr.appendChild(td);
            }
        
        $("#table-parent").append(table);
        
			},
    				error : function(data) {

    			}
  			});
		});
	});

JS;

$this->registerJs($script);
?>
  
