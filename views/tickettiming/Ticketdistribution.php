<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use dosamigos\datepicker\DateRangePicker;
use app\models\Operator;

$this->title = 'Operations Assistance Portal | Wavenet';

?>
<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Ticket Status Flow
					<small>Ticket Lifecycle</small>
				</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> Dashboard</li>
					<li><i class="fa fa-laptop"></i> Performance</li>
                    <li><i class=""></i> Trend Graphs (Tickets)</li>
					<li class="active">Ticket Distribution</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
                            <div class="box-header with-border">
								<h3 class="box-title">Filters</h3>
							</div>
							<div class="box-body">
								<!-- <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data"  name="form_blacklist" id="form_blacklist"> -->
								<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'form-horizontal']]) ?>
									<div class="row">
										<div class="box-body boxpad">
                                            <div id="start_datetime-tr" class="col-md-5">
                                                <div class="form-group">
												<label class="col-sm-3 control-label">Date Range <span
                                                            class="req-fld">*</span>
													</label>
                                                    <!-- <div class="col-sm-9"> -->
														<!-- <div class="form-group"> -->
														<?= $form->field($model, 'start',['options' => ['class' => 'col-sm-9','id' => 'end']])->label(false)->widget(DateRangePicker::className(), ['options' => ['placeholder' => 'From'],
																'attributeTo' => 'end', 
																'form' => $form, // best for correct client validation
																'clientOptions' => [
																	'autoclose' => true,
																	'format' => 'yyyy-mm',
																	'startView'=>'year',
																	'minViewMode'=>'months',
																	'readonly'=>'true',
																	'pickButtonIcon' => 'glyphicon glyphicon-time',
																]
															]);?>
														<!-- </div> -->

                                                    <!-- </div> -->
                                                </div>
											</div>
											<div id="start_datetime-tr" class="col-md-3">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Customer</label>
                                                        <!--?= $form->field($searchModel, 'operatorName',['options' => ['class' => 'col-sm-9']])->textInput(['maxlength' => true,'placeholder' => 'Operator'])->label(false);?-->
                                                    	<?= $form->field($model, 'region',['options' => ['class' => 'col-sm-9','id'=>'region']])->label(false)->widget(Select2::classname(), ['data' => Operator::getOperatorByName(),'options' => ['placeholder' => 'Operator'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>
											<!-- <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Region <span
                                                            class="req-fld">*</span>
                                                    </label>
                                                          <?//= $form->field($model, 'region',['options' => ['class' => 'col-sm-9','id'=>'region']])->label(false)->widget(Select2::classname(), ['data' => ['Latam' => 'Latam', 'Asia' => 'Asia','SEA' => 'SEA'],'options' => ['placeholder' => 'Region'],'pluginOptions' => ['allowClear' => true]]); ?> 
                                                    </div>
                                            </div> -->
											<div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Filter By</label>
                                                        <!--?= $form->field($searchModel, 'operatorName',['options' => ['class' => 'col-sm-9']])->textInput(['maxlength' => true,'placeholder' => 'Operator'])->label(false);?-->
														<?php
															$model->filter='Ticket Updated';
														?>
														<?= $form->field($model, 'filter',['options' => ['class' => 'col-sm-6','id'=>'region']])->label(false)->widget(Select2::classname(), ['data' => ['Ticket Created' => 'Ticket Created', 'Ticket Updated' => 'Ticket Updated'],'options' => ['placeholder' => 'Filter'],'pluginOptions' => []]); ?>   </div>
                                            </div>
										</div>
										
									</div>
									<div class="row">
										<div class="box-body boxpad">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="box-footer">
														<?= Html::button('Generate Graph', ['class' => 'btn btn-primary pull-right btnspace','name' => 'graph', 'id' => 'graph']) ?>
											        	<!-- <input type="submit" class="btn btn-primary pull-right btnspace" name="submit" id="gen_graph" value="Generate Graph" > -->
												</div>
											</div>
										</div>						
									</div>
								<?php ActiveForm::end() ?>
								<!-- </form> -->
							</div>
						</div>
					</div>
				</div>


				<div id="data_area">
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Ticket Status Movement Graph</h3>
								</div>
								<div class="box-body">
									<canvas id="mycanvas"></canvas>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Report Data</h3>
								</div>
								<div class="box-body">
									<form class="form-horizontal" name="form_blacklist_table" id="">
										<div class="row">
											<div class="box-body boxpad contSeperator">
												<div id='table-parent' class="col-md-12 col-sm-12 col-xs-12" style="overflow:scroll;width:100%;overflow:auto">
													<table id="table" class="table table-bordered table-striped table-hover">
													</table>
												</div>
											</div>						
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

				</div>
                
			
			</section>
			<!-- /.content -->
<?php

$url = Url::to(['tickettiming/showticketdistribution']);
//echo $url;
$script = <<< JS
	$("#graph").click(function() {
    $('html, body').animate({
        scrollTop: $("#data_area").offset().top
    }, 2000);
	});
	$(document).ready(function(){
	
	$('#graph').bind("click", function () {
		

  		$.ajax({
    			url : "{$url}",
    			type : "POST",
				data: { start: $('#ticketflow-start').val(), end: $('#ticketflow-end').val(), region: $('#ticketflow-region').val(), filter: $('#ticketflow-filter').val()},
    			success : function(data){
      			console.log(data);
      			var data = JSON.parse(data);
				//echo data;
				//sprint_r(data);
				var Status=['Open','InProgress','NeutralizationPending','WorkaroundProvided','MOPPending','RCAPending','ActivityPending','PermanentfixPending','WaitingonThirdParty','WaitingonCustomer','Pending','Resolved','Closed'];
      			var TicketId = [];
				var ticket=0;
				//var my_array = array_fill(0,13, 0);
				var my_array=[0,0,0,0,0,0,0,0,0,0,0,0,0];
				var datas=[];
				var da;
				var a=59;
				var b=89;
				var c=152;
				var d=1;
				var e=0.2;
				var isUpdated=false;
      			for(var i in data) {
					  a=((Math.random() * 250) + 1);
					  b=((Math.random() * 250) + 1);
					  c=((Math.random() * 250) + 1);
					  var color1="rgba("+a+", "+b+", "+c+", "+d+")";
					  var color2="rgba("+a+", "+b+", "+c+", "+e+")";
					
					 if(ticket!=data[i].TicketId){
						if(ticket!=''){
							TicketId.push(ticket);
							

							da={
								label: ticket,
								fill: false,
								backgroundColor: color2,
								borderColor: color1,
								pointHoverBackgroundColor: color1,
								pointHoverBorderColor: color1,
								borderWidth: 1,
								data: my_array
							};
							
							my_array=[0,0,0,0,0,0,0,0,0,0,0,0,0];
							datas.push(da);
					 	}
						ticket=data[i].TicketId;
					  } 
					  for(j=0;j<13;j++){
						if(Status[j]==data[i].Status){
							my_array[j]=data[i].Duration/60;
						}
					  }
      			}
				  da={
						label: ticket,
						fill: false,
						backgroundColor: color2,
						borderColor: color1,
						pointHoverBackgroundColor: color1,
						pointHoverBorderColor: color1,
						borderWidth: 1,
						data: my_array
					};
					datas.push(da);
			  var chartdata = {
        			labels: Status,
        			datasets: datas
      		};
			  
      		var ctx = $("#mycanvas");

              if(window.LineGraph != undefined)
                
                window.LineGraph.destroy();

      				window.LineGraph = new Chart(ctx, {
        				type: 'line',
        				data: chartdata,
        				options: {
							/*legend: {
								display: false,
							},*/
        					scales: {
            					xAxes: [{
                					display: true,
                					scaleLabel: {
                    					display: true,
                    					labelString: 'Status'
                					},
									ticks: {
										autoSkip: false
                					}
            					}],
            					yAxes: [{
                					display: true,
                					scaleLabel: {
                    					display: true,
                    					labelString: 'Duration(Minutes)'
                					},
                				ticks: {
                    				beginAtZero:true,
									
                				}
            				}]
        				}
    				}
      				});

      		document.getElementById("table").innerHTML = "";

      		var col = [];
        	for (var i = 0; i < data.length; i++) {
            	for (var key in data[i]) {
                	if (col.indexOf(key) === -1) {
                    	col.push(key);
                	}
            	}
        	}
        	var table = document.getElementById("table");

        	var tr = table.insertRow(-1); 

        	for (var i = 0; i < col.length-1; i++) {
            	var th = document.createElement("th"); 
            	th.innerHTML = col[i];
            	tr.appendChild(th);
        	}
		var th = document.createElement("th");
                th.innerHTML = "Duration(Minutes)";
                tr.appendChild(th);


    
        	for (var i = 0; i < data.length; i++) {

            	tr = table.insertRow(-1);

            	for (var j = 0; j < col.length; j++) {
                	var tabCell = tr.insertCell(-1);
                	tabCell.innerHTML = data[i][col[j]];
            	}
        	} 

        	  var tr = table.insertRow(-1); 
            var td = document.createElement("td");
            td.innerHTML = "Total";
            tr.appendChild(td);

            for (var j = 1; j < col.length; j++) {
                sum = 0;    
                for (var i = 0; i < data.length; i++) {
                    var td = document.createElement("td");
                    var sum = parseInt(sum) + parseInt(data[i][col[j]]);
                }
                    td.innerHTML = sum;
                    tr.appendChild(td);
            }
        
        $("#table-parent").append(table);
        
			},
    				error : function(data) {

    			}
  			});
		});
	});

JS;

$this->registerJs($script);
?>
  


