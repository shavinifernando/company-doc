<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScheduleactivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Scheduleactivities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scheduleactivity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Scheduleactivity'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'scheduleId',
            'scheduleTimeSL',
            'scheduleTimeOther',
            'L1Resource',
            'L2Resource',
            //'L3Resource',
            //'cusApproval',
            //'managerApproval',
            //'techApproval',
            //'engResponsible',
            //'id',
            //'activityStatus',
            //'cusContactPoint',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
