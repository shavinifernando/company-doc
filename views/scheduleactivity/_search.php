<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScheduleactivitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scheduleactivity-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'scheduleId') ?>

    <?= $form->field($model, 'scheduleTimeSL') ?>

    <?= $form->field($model, 'scheduleTimeOther') ?>

    <?= $form->field($model, 'L1Resource') ?>

    <?= $form->field($model, 'L2Resource') ?>

    <?php // echo $form->field($model, 'L3Resource') ?>

    <?php // echo $form->field($model, 'cusApproval') ?>

    <?php // echo $form->field($model, 'managerApproval') ?>

    <?php // echo $form->field($model, 'techApproval') ?>

    <?php // echo $form->field($model, 'engResponsible') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'activityStatus') ?>

    <?php // echo $form->field($model, 'cusContactPoint') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
