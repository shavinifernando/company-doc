<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\models\Creator;
use app\models\Operator;
use app\models\User;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;
//use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-body">
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'Schedule-form','class'=>'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">
                                        <div class="row">
											<div id="start_datetime-tr" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Schedule Time <span
                                                            class="req-fld">*</span>
                                                            <div class="small-help-text">(Sri Lanka)</div>
													</label>
                                                    <?=$form->field($model, 'scheduleTimeSL',['options' => ['class' => 'col-sm-8','id' => 'scheduleDateSL']])->label(false)->widget(DateTimePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD HH:MM'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'minuteStep' => 1,
                                                                'format' => 'yyyy-mm-dd hh:ii'
                                                            ]
                                                        ]);?>
                                                </div>
                                            </div>

											<div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Schedule Time <span
                                                            class="req-fld">*</span>
                                                            <div class="small-help-text">(Other Country)</div>
													</label>
                                                    <?=$form->field($model, 'scheduleTimeOther',['options' => ['class' => 'col-sm-8','id' => 'scheduleTimeOther']])->label(false)->widget(DateTimePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD HH:MM'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'minuteStep' => 1,
                                                                'format' => 'yyyy-mm-dd hh:ii'
                                                            ]
                                                        ]);?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Activity Status<span class="req-fld">*</span></label> -->
                                                    <!--?= $form->field($model, 'activityStatus',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => ['Successfully Completed' => 'Successfully Completed', 'Partially Completed' => 'Partially Completed','Failed' => 'Failed', 'Scheduled' => 'Scheduled','To be Scheduled' => 'To be Scheduled'],'options' => ['placeholder' => 'Activity Status'],'pluginOptions' => ['allowClear' => true],]); ?-->
                                                <!-- </div>
                                            </div> -->

											<div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">L1 Resource<span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'L1Resource',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => User::getLevelInfo(1),'options' => ['placeholder' => 'L1 Resource'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>
                                        <!-- </div> -->
                                        <!-- <div class="row"> -->
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">L2 Resource</label>
                                                    <?= $form->field($model, 'L2Resource',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => User::getLevelInfo(2),'options' => ['placeholder' => 'L2 Resource'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">L3 Resource</label>
                                                    <?= $form->field($model, 'L3Resource',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => User::getLevelInfo(3),'options' => ['placeholder' => 'L3 Resource'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>
										<!-- </div> -->
                                        <!-- <div class="row"> -->	
											<div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Customer Contact Point<span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'cusContactPoint',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => 'Customer Contact Point'])->label(false);?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!--div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Manager Approval <span class="req-fld">*</span></label-->
                                                    <!--?= $form->field($model, 'managerApproval',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => ['Approved' => 'Approved', 'Not Approved' => 'Not Approved'],'options' => ['placeholder' => 'Customer Approval'],'pluginOptions' => ['allowClear' => true],]); ?-->
                                                <!--/div>
                                            </div-->
                                        <!-- </div> -->
                                        <!-- <div class="row"> -->
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Customer Approval <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'cusApproval',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => ['Approved' => 'Approved', 'Not Approved' => 'Not Approved'],'options' => ['placeholder' => 'Customer Approval'],'pluginOptions' => ['allowClear' => true],]); ?>
                                                </div>
                                            </div>
                                            <?php
                                        if($model->activity->mopName== ""){    
                                            echo '<div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Technical Approval <span class="req-fld">*</span></label>';
                                                        echo $form->field($model, 'techApproval',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => ['Approved' => 'Approved', 'Not Approved' => 'Not Approved'],'options' => ['placeholder' => 'Technical Approval'],'pluginOptions' => ['allowClear' => true],]);  
                                                echo '</div>
                                            </div>';
                                        }
                                        ?>
                                        <?php
                                        if($model->activity->mopName!= ""){    
                                            echo '<div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Technical Approval <span class="req-fld">*</span></label>
                                                    <div class="col-sm-8" >
                                                        <input type="text" disabled class="form-control" value="'.@$model->activity->mop->techApproval.'">
                                                    </div> 
                                                </div>
                                            </div>';
                                        }
                                        ?>
                                        </div>
                                        <div class="row"> 
                                        <!-- </div> -->
                                        <!-- <div class="row"> -->
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Engineer Responsible after <abbr title="Maintenance Window">MW</abbr><span class="req-fld">*</span></label>
                                                     <?= $form->field($model, 'engResponsible',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => 'Engineer Responsible after MW'])->label(false);?>
                                                </div>
                                            </div>
                                        <!-- </div> -->
                                        </div>
                                    </div>
                                </div>                                                        
                        </div>

                        <div class="modal-footer">
			<!--?php	 echo ($model->activity->mop->techApproval!='Approved')?'<font color="red"><B>Please Approve the MOP before scheduling it!!!</B></font>':'';?-->
                        
                            <!--?= Html::button('Close', ['class' => 'btn btn-default','name' => 'closeSchedule', 'id' => 'closeSchedule']) ?-->
                            <?= Html::submitButton('Schedule', ['class' => 'btn btn-primary','name' => 'schedule', 'id' => 'schedule']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeSchedule').click(function() {
             window.parent.closeScheduleModal();
         });  
    });
JS;

$this->registerJs($script);

?>
