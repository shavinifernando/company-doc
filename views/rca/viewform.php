<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-body">
                            <!-- <form class="form-horizontal"> -->
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'View-form', 'class' => 'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ticket ID</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->ticketId; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Opened Date</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->openDate; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">RCA Name</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->RCAName; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">RCA Shared Date</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->sharedDate; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Operator</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->operatorsName->operatorName; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Issue Description</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->issueDescription; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">RCA Creator</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->creatorsName->firstname; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Link to RCA</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <a href="<?= $model->linkToRCA; ?>" target="_blank">
                                                        <?php echo $model->linkToRCA; ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div> 
                            <!-- </form>  -->                                                      
                        </div>
                        
                        <div class="modal-footer">
                            <?= Html::button('Close', ['class' => 'btn btn-default', 'name' => 'closeView', 'id' => 'closeView']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeView').click(function() {
            //alert('dddd');
            // window.parent.$('#ViewModal').modal('hide');
             window.parent.closeViewModal();
         });  
    });
JS;

$this->registerJs($script);
?>
