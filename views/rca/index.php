<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RcaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rcas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rca-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Rca'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'rcaId',
            'ticketId',
            'openDate',
            'sharedDate',
            'RCAName',
            //'operator',
            //'issueDescription',
            //'RCACreator',
            //'linkToRCA',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
