<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>                    
                    <div class="modal-body">
                            <!-- <form class="form-horizontal"> -->
                            <?php $form = ActiveForm::begin(['options' => ['id' => 'Update-form','class'=>'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ticket ID</label>
                                                <div class="col-sm-8" >
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->ticketId;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Opened Date</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->openDate;?>'>      
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">RCA Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->RCAName;?>'>
                                                     </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">RCA Shared Date</label>
                                                <!-- <div class="col-sm-8"> -->
                                                    <?=$form->field($model, 'sharedDate',['options' => ['class' => 'col-sm-8','id' => 'sharedDate']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'format' => 'yyyy-mm-dd'
                                                            ]
                                                        ]);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Operator</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->operatorsName->operatorName;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">RCA Creator</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->creatorsName->firstname;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Link to RCA</label>
                                                <!-- <div class="col-sm-8"> -->
                                                    <?= $form->field($model, 'linkToRCA',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => 'URL','title' => $model->linkToRCA])->label(false);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Issue Description</label>
                                                <!-- <div class="col-sm-8"> -->
                                                    <?= $form->field($model, 'issueDescription',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Description'])->label(false);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>

                                    </div>
                                </div> 
                            <!-- </form> -->                                                       
                        </div>

                        <div class="modal-footer">
                            <!--?= Html::button('Close', ['class' => 'btn btn-default','name' => 'closeUpdate', 'id' => 'closeUpdate']) ?-->
                            <?= Html::submitButton('Save Changes', ['class' => 'btn btn-primary','name' => 'update', 'id' => 'update']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeUpdate').click(function() {
             window.parent.closeUpdateModal();
         });  
    });
JS;

$this->registerJs($script);
?>
