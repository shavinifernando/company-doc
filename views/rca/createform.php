<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\models\Creator;
use app\models\User;
use app\models\Operator;
use yii\helpers\ArrayHelper;
//use yii\bootstrap\Modal;
$session = Yii::$app->session;
$userid = $session['user_id'];
$username = $session['user_name'];
$first_name = $session['firstname'];

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>

                        <div class="modal-body">
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'Create-form','class'=>'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">
					<div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Ticket ID <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'ticketId',['options' => ['class' => 'col-sm-8','name' => 'ticketid','id' => 'ticketid','autocomplete'=>'off']])->textInput(['maxlength' => true,'placeholder' => 'Ticket ID','data-toggle' => 'tooltip','data-placement'=>'bottom','title'=>'1234 or 12345','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>

                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"><abbr title="Root Cause Analysis">RCA</abbr> Name <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'RCAName',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => 'RCA Name','data-toggle' => 'tooltip','data-placement'=>'bottom','title'=>'RCA_Customer_TicketID_Product_DDMMYYYY_vx','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
					    </div>
					    <div class="row">
                                            <div id="start_datetime-tr" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Opened Date <span
                                                            class="req-fld">*</span>
                                                    </label>
                                                   <?=$form->field($model, 'openDate',['options' => ['class' => 'col-sm-8','id' => 'openDate']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD','autocomplete'=>'off'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'format' => 'yyyy-mm-dd'
                                                            ]
                                                        ]);?>
                                                </div>
                                            </div>

                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"><abbr title="Root Cause Analysis">RCA</abbr> Shared Date
                                                    </label>
                                                    <?=$form->field($model, 'sharedDate',['options' => ['class' => 'col-sm-8','id' => 'sharedDate']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD','autocomplete'=>'off'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'format' => 'yyyy-mm-dd'
                                                            ]
                                                        ]);?>
                                                </div>
                                            </div>
					    </div>
					    <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"><abbr title="Root Cause Analysis">RCA</abbr> Creator <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'RCACreator',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => User::getUser(),'options' => ['placeholder' => 'RCA Creator'],'pluginOptions' => ['allowClear' => true],]); ?>
                                                    <!--?= $form->field($model, 'RCACreator',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => $first_name,'autocomplete'=>'off', 'disabled' => true,'value' => $first_name])->label(false);?-->
                                                </div>
                                            </div>
                                                            
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Link to <abbr title="Root Cause Analysis">RCA</abbr> <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'linkToRCA',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => 'URL','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                            </div>
					    <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Operator Name <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'operator',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(),['data' => Operator::getOperator(),'options' => ['placeholder' => 'Operator'],'pluginOptions' => ['allowClear' => true],]); ?>
                                                </div>
                                            </div>
					    
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Issue Description <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'issueDescription',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Description','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                            
                                            
					</div>
                                    </div>
                                </div>                                                     
                        </div>

                        <div class="modal-footer">
                           <!--?= Html::button('Close', ['class' => 'btn btn-default','name' => 'closeCreate', 'id' => 'closeCreate']) ?-->
                            <?= Html::submitButton('Create', ['class' => 'btn btn-primary','name' => 'create', 'id' => 'create']) ?>
                        </div>
                        <?php ActiveForm::end();?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeCreate').click(function() {
             window.parent.closeCreateModal();
         });  
    });
JS;

$this->registerJs($script);
?>
