<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RcaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rca-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'rcaId') ?>

    <?= $form->field($model, 'ticketId') ?>

    <?= $form->field($model, 'openDate') ?>

    <?= $form->field($model, 'sharedDate') ?>

    <?= $form->field($model, 'RCAName') ?>

    <?php // echo $form->field($model, 'operator') ?>

    <?php // echo $form->field($model, 'issueDescription') ?>

    <?php // echo $form->field($model, 'RCACreator') ?>

    <?php // echo $form->field($model, 'linkToRCA') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
