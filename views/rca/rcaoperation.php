<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use app\models\Creator;
use app\models\User;
use app\models\Operator;


$this->title = 'Operations Assistance Portal | Wavenet';

?>
<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Root Cause Analysis (RCA)
					<small>Resolution and Activities</small>
				</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> Dashboard</li>
					<li><i class="fa fa-laptop"></i> Performance</li>
                    <li><i class=""></i> Resolution and Activities</li>
					<li class="active">Root Cause Analysis (RCA)</li>
				</ol>
			</section>
			
			<div class="floating-button" id="floating-button" data-toggle="modal" data-target="#CreateRCAModal" title="Create RCA"><i class="fa fa-plus"></i></div>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
                            <div class="box-header with-border">
								<h3 class="box-title">Filters</h3>
							</div>

							<div class="box-body">
								<?php $form = ActiveForm::begin(['method'=>'get'],['options' => ['enctype' => 'multipart/form-data', 'class'=>'form-horizontal']]) ?>
									<div class="row">
										<div class="box-body boxpad">

                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Start</label>
                                                         <?=$form->field($searchModel, 'start',['options' => ['class' => 'col-sm-9','id' => 'start']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD','data-toggle' => 'tooltip','data-placement'=>'bottom','title'=>'RCA Created Date'],
															'pluginOptions' => [
																'autoclose' => true,
																'format' => 'yyyy-mm-dd'
															]
														]);?> 
                                                    </div>
                                            </div>

                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">End</label>
                                                            <?=$form->field($searchModel, 'end',['options' => ['class' => 'col-sm-9','id' => 'end']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD','data-toggle' => 'tooltip','data-placement'=>'bottom','title'=>'RCA Created Date'],
															'pluginOptions' => [
																'autoclose' => true,
																'format' => 'yyyy-mm-dd'
															]
														]);?>
                                                </div>
                                            </div>

                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Ticket ID</label>
                                                    <?= $form->field($searchModel, 'id',['options' => ['class' => 'col-sm-9']])->textInput(['maxlength' => true,'placeholder' => 'Ticket ID','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>

										</div>
									</div>
                                    <div class="row">
                                        <div class="box-body boxpad">
                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Operator</label>
                                                        <!--?= $form->field($searchModel, 'operatorName',['options' => ['class' => 'col-sm-9']])->textInput(['maxlength' => true,'placeholder' => 'Operator'])->label(false);?-->
							<?= $form->field($searchModel, 'operatorName',['options' => 
		                                                    	['class' => 'col-sm-9']
		                                                    ])->label(false)->widget(Select2::classname(), [
		                                                    	'data' => Operator::getOperator(),
		                                                    	'changeOnReset' => true,
		                                                    	'options' => [
		                                                    		'placeholder' => 'Operator', 
		                                                    		//'value' => ""
									],
	                                                    		'pluginOptions' => [
	                                                    			'allowClear' => true,

	                                                    		]]); ?>
                                                </div>
                                            </div>
                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Creator</label>
                                                        <!--?= $form->field($searchModel, 'creator',['options' => ['class' => 'col-sm-9']])->textInput(['maxlength' => true,'placeholder' => 'RCA Creater'])->label(false);?-->
							<?= $form->field($searchModel, 'creator',['options' => ['class' => 'col-sm-9']])->label(false)->widget(Select2::classname(), ['data' => User::getUser(),
                                                        'options' => [
                                                        	'placeholder' => 'RCA Creator',
                                                        	//'value' => ""
                                                        ],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<div class="row">
										<div class="box-body boxpad">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="box-footer">										<?= Html::submitButton('Search', ['class' => 'btn btn-primary pull-right btnspace','name' => 'search', 'id' => 'search']) ?>
	 <?= Html::resetButton('Reset', ['class' => 'btn btn-default pull-right btnspace','name' => 'resetSearch', 'id' => 'resetSearch']) ?>
												</div>
											</div>
										</div>						
									</div>
								<?php ActiveForm::end() ?>
							</div>
						</div>
					</div>
				</div>

				<div id="data_area">

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Search Results</h3>
								</div>
								<div class="box-body">
									<form class="form-horizontal" name="form_blacklist_table" id="">
										<div class="row">
											<div class="box-body boxpad contSeperator">
												<div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive row">
                                                    <div class="col-md-12">
													    <?= GridView::widget([
			                            				'dataProvider' => $dataProvider,
			                            				//'filterModel' => $searchModel,
			                                   			'showOnEmpty'=>true,
			                            				'columns' => [
			                              					//['class' => 'yii\grid\SerialColumn'],
													        /*[
													            'header' => 'Ticket ID',
													            'attribute' => 'ticketId',
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],*/
														[
            													'header' => 'Ticket ID',
            													'format'=>'raw',
            													'value' => function($model){
                												$url = "http://wavenet.freshdesk.com//helpdesk/tickets/$model->ticketId";
                												return Html::a($model->ticketId, $url, ['title' => 'Freshdesk Ticket Link','target' => '_blank']); 
            													},
														'headerOptions' => ['style'=>'vertical-align: middle;'],
        													],
													        [
													            'header' => 'Created Date',
													            'attribute' => 'openDate',
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'Shared Date',
													            'attribute' => 'sharedDate',
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'RCA Name',
													            'attribute' => 'RCAName',
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'Operator',
													            'attribute' => 'operator',
														    'value' =>function($model){ return	$model->operatorsName->operatorName;},
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'Issue Description',
													            'attribute' => 'issueDescription',
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'RCA Creator',
													            'attribute' => 'RCACreator',
														    'value' => function($model){return $model->creatorsName=='' ? '':$model->creatorsName->firstname;},
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        ['class' => 'yii\grid\ActionColumn','header' => 'Actions','template'=>'{view} {update} {link} {delete}',
													        'buttons' => [
																'view' => function ($url,$model) {
																return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',['#'], ['data-url' => Url::toRoute(['rca/view', 'id' => $model->rcaId]), 'id' => 'btn-view','title'=>'View RCA']);
															},
															'update' => function ($url,$model) {
																return Html::a('<span class="glyphicon glyphicon-pencil"></span>',['#'], ['data-url' => Url::toRoute(['rca/update', 'id' => $model->rcaId]), 'id' => 'btn-update','title'=>'Update RCA']);
															},
															'link' => function ($url,$model) {
																return Html::a('<span class="glyphicon glyphicon-paperclip"></span>', $model->linkToRCA, ['target' => '_blank','title'=>'Link to RCA']);
															},
															'delete' => function ($url,$model) {
																return Html::a('<span class="glyphicon glyphicon-trash"></span>',['rca/delete', 'id' => $model->rcaId], ['data' => ['confirm' => Yii::t('app', 'Are you sure you want to delete this record?')],'title'=>'Delete RCA']);
															}
															],
													        'headerOptions' => ['style'=>'vertical-align: middle; width:120px;']
													    	],
			                            					],
			                          					]); ?>
                                                        </div>                                                    
                                                    </div>
													
												</div>
											</div>						
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

				</div>
                

                <!-- Create modal -->               
               <div class="modal fade" id="CreateRCAModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                    	<div class="modal-content">
                    	<div class="modal-header">
                            <button type="button" id="closeCreateButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Root Cause Analysis (RCA)</h4>
                        </div>
                    	<iframe id="create-view" name="create" src="<?= Url::to(['rca/create']) ?>"  width="100%" height="375" frameborder="0"></iframe>
                        </div>
					</div>
               </div>

                <!-- View modal -->
                <div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">RCA Details</h4>
                        </div>
                        <iframe id="view-modal" name="view" src=""  width="100%" height="350" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
                
                <!-- Edit modal -->
                    <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="closeUpdateButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit RCA</h4>
                        </div>
                        <iframe id="update-view" name="update" src=""  width="100%" height="375" frameborder="0"></iframe>
                        <?php echo yii::$app->session->getFlash('messsage'); ?>
                        </div>
                    </div>
                </div>

                
			
			</section>
			<!-- /.content -->
<?php

$urlView = Url::to('rca/view');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-view', function(e) {
        $('#view-modal').attr("src", $(this).attr('data-url'));
        $('#ViewModal').modal({show:true})

        return false;
    });
});
JS;

$this->registerJs($script);

$urlUpdate = Url::to('rca/update');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-update', function(e) {
        $('#update-view').attr("src", $(this).attr('data-url'));
        $('#EditModal').modal({show:true})
        return false;
    });
});
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeCreateModal = function(){ 
    		$('#CreateRCAModal').modal('hide');
    		location.reload(); 
    	};
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeViewModal = function(){ $('#ViewModal').modal('hide'); };
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeUpdateModal = function() { 
    		$('#EditModal').modal('hide');
    		location.reload();
    	};
 		
    });
JS;

$this->registerJs($script);

$script = <<< JS
$( document ).ready(function() {
       
        $('#closeCreateButton').click(function() {
             window.parent.closeCreateModal();
         });  
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeCreateModal = function(){ 
    		$('#CreateMOPModal').modal('hide'); 
    		location.reload();
    	};
    });
JS;

$this->registerJs($script);

$script = <<< JS
$( document ).ready(function() {
       
        $('#closeUpdateButton').click(function() {
             window.parent.closeUpdateModal();
         });  
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeUpdateModal = function(){ 
    		$('#EditModal').modal('hide'); 
    		location.reload();
    	};
    });
JS;

$this->registerJs($script);

$script = <<< JS
$( document ).ready(function() {
    $('#resetSearch').click(function() {
        $('#rcasearch-operatorname').val(null).trigger("change");
       	$('#rcasearch-creator').val(null).trigger("change");
        });  
    });
JS;

$this->registerJs($script);

?>

