<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">CDR Details</h4>
    </div>

    <div class="modal-body">
        <form class="form-horizontal">
            <div class="row">
                <div class="box-body boxpad">

                    <table class="table">
                        <tr>
                            <th>Label</th>
                            <th>Value</th>
                            <th>Description</th>
                        </tr>
                        <tr>
                            <td style="width:150px;">Label 1</td>
                            <td>2342</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
                        </tr>
                        <tr>
                            <td>Label 2</td>
                            <td>3345</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
                        </tr>
                    </table>

                </div>
            </div> 
        </form>                                                       
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
    </div>
</div>
</div>
