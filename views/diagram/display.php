<?php
use app\models\CdrAllinone;
use app\models\CdrDivision;
use app\models\Cdranalyzer;
use yii\widgets\Pjax;
use app\models\Cdr_nfe;
use yii\web\Session;
$session = Yii::$app->session;
?>

<div class="modal-body">
<script src="/ops_asst/vendor/bower-asset/vis/dist/vis.min.js"></script> 
<link href="/ops_asst/vendor/bower-asset/vis/dist/vis.min.css" rel="stylesheet" type="text/css" />
<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>
    <form class="form-horizontal">
        <div class="row">
		<table class="table">
			<tr>
			<?php $nxt=array();
				$final4= explode('\r\n', $session['ussd']) ;
				foreach ($final4 as $key) {
					$dew=explode(',',$key);
					$final5[] = $dew;
				}
				
				$sesid5=array(); $prior=array();
				for($i=0;$i<sizeof($final5);$i++){
					if($final5["$i"]['19']==$id){
						$sesid5[]=$final4["$i"];
						$sesid4[]=explode(',',$final4["$i"]);
						$prior[]=$final5["$i"]['18'];
					}
				}
				
				$reduced=array_unique($prior);
				
				for($k=0;$k<sizeof($session['send']);$k++){
					if(isset($session['send']["$k"]['18'])){
						if(in_array($session['send']["$k"]['18'],$reduced)){
							array_unshift($sesid4,$session['send']["$k"]);
						}
					}
				}
				
				
			?>
			<th>Session ID :<?php print_r($id); ?></th>
			</tr>	
			<?php $type=array(); $code=array();$request=array(); ?>
			<?php	for($j=0;$j<sizeof($sesid4);$j++){ 
					$type[]=$sesid4["$j"]['0'];
					$code[]=$sesid4["$j"]['28'];
					$request[]=$sesid4["$j"]['33'];
					$smcid3[]=$sesid4["$j"]['32'];
					$esme3[]=$sesid4["$j"]["29"];
					
				}
				$smcid2=array_reverse($smcid3); $esme2=array_reverse($esme3);
				$smcid=$smcid2["0"];$esme=$esme2["0"];
				$no60=0; $no61=0; $no62=0; $no63=0;  
				for ($j=0;$j<sizeof($type);$j++){
					if($type["$j"]=="60"){
						$no60++ ;
					}
					else if($type["$j"]=="61"){
						$no61++ ;
					}
					else if($type["$j"]=="62"){
						$no62++ ;
					}
					else{
						$no63++ ;
					}
				}
			?>
			<?php $cube=max($no60,$no61,$no62,$no63);$lenn=sizeof($type); ?>   
			<tr> 
			<td>
				<div class=".mycontainer"><div id="mynetwork"></div></div><!--?php echo($sesid5["$j"]);?><p>&darr;</p--></td>
			<script>
			var maximun = <?php echo json_encode($cube); ?>; var esme = <?php echo json_encode($esme); ?>;
			var lenn = <?php echo json_encode($lenn); ?>;	var smcid = <?php echo json_encode($smcid); ?>;
			var myarray= <?php echo json_encode($type); ?>;	var request = <?php echo json_encode($request); ?>;
			var code= <?php echo json_encode($code); ?>;
			var nodes = [
				{ id: 1, label: "  Mobile Device  "+'\n'+'\n', x: 0, y: 0, fixed: true ,color: "#FB7E81" },
				{ id: 2, label: "  USSD gateway  "+'\n'+'\n'+'SMSCID:'+smcid, x: 350, y: 0, fixed: true ,color: "#FB7E81" },
				{ id: 3, label: "  Application  "+'\n'+'\n'+'ESME:'+esme, x: 700, y: 0, fixed: true ,color: "#FB7E81" },
				{ id: 4, label: "  Mobile Device  ", x: 0, y: 300*maximun, fixed: true ,color: "#FB7E81" },
				{ id: 5, label: "  USSD Gateway  ", x: 350, y: 300*maximun, fixed: true ,color: "#FB7E81"},
				{ id: 6, label: "  Application  ", x: 700, y: 300*maximun, fixed: true ,color: "#FB7E81" },
				];
			var edges =[
				{ from: 3, to: 6, arrows: "" ,label:"" },
				{ from: 2, to: 5, arrows: "" ,label:"" },
				{ from: 1, to: 4, arrows: "" ,label:"" },
				];
			
			var up1=0 ; var previous=0; var basic=50; var index=0
			for (var element of myarray){
				
				if (previous>=element){
					up1=up1+8;
				}	
				if (element=='60'){
					nodes.push({id: 7+up1, label:"", x:0,y: basic,fixed: true},
								{id: 8+up1, label:"", x:350,y: basic,fixed: true});
					edges.push({ from: 7+up1, to: 8+up1, arrows: "to" ,label:"Event: 60 "+'\n'+'OP Code:'+code[index]+'\n'+'USSD:'+request[index] });
					basic=basic+70;
				}
				else if (element=='61'){
					nodes.push({id: 9+up1, label:"" ,x:350,y: basic,fixed: true},
				{id: 10+up1, label:"", x:700,y: basic,fixed: true});
					edges.push({ from: 9+up1, to: 10+up1, arrows: "to" ,label:"Event: 61 "+'\n'+'USSD:'+request[index]});
					basic=basic+70;
				}
				else if (element=='62'){
					nodes.push({id: 11+up1, label:"", x:700,y: basic,fixed: true},
				{id: 12+up1, label:"", x:350,y: basic,fixed: true});
					edges.push({ from: 11+up1, to: 12+up1, arrows: "to" ,label:"Event: 62 "+'\n'+'USSD:'+request[index]});
					basic=basic+70;					
				}
				else if (element=='63'){
					nodes.push({id: 13+up1, label:"", x:350,y: basic,fixed: true },
				{id: 14+up1, label:"", x:0,y: basic,fixed: true});
					edges.push({ from: 13+up1, to: 14+up1, arrows: "to" ,label:"Event: 63 "+'\n'+'OP Code:'+code[index]+'\n'+'USSD:'+request[index]});
					basic=basic+70;
				}
				previous=element;
				index++;
			};
			
			var container = document.getElementById("mynetwork");
			var data = {
			  nodes: nodes,
			  edges: edges
			};
			var options = {
				nodes:{
					shape: "box",
					size: 2 ,
				},
				physics: false,
				clickToUse: false,
				width: '850px',
				height:300*maximun
			};
			var network = new vis.Network(container, data, options);
			</script>
			</tr>
			
			<tr><td>
		</table>
		</div>
	</form>
</div>
