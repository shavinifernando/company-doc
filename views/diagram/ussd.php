<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
use kartik\widgets\DateTimePicker;
use kartik\select2\Select2;
use app\models\Ussd;
use yii\widgets\Pjax;
use yii\web\Session;
use yii\vis\vis;
$session = Yii::$app->session;
?>
<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>
<section class="content-header">
    <h1>
        USSD
        <small>USSD</small>
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Dashboard</li>
        <li><i class="fa fa-laptop"></i> USSD</li>
        <li class="active">USSD</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="box">
				<div class="box-body">
					<?php $form = ActiveForm::begin(['method' => 'post'], ['options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]) ?>
					<br>
					<div class="row">
					<?= $form->field($model, 'Ussd', ['options' => ['class' => 'col-sm-12']])->textarea(['rows' => '10', 'maxlength' => true, 'placeholder' => 'Paste the CDR ', 'autocomplete' => 'off'])->label(false); ?>
						
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="box-footer">
							<?= Html::submitButton('Insert', ['class' => 'btn btn-primary pull-right btnspace', 'name' => 'Insert', 'value' => 'Insert']) ?>

						</div>
					</div>
				</div>
				<?php ActiveForm::end() ?>
				 
				<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="box">
				<?php
                $session = Yii::$app->session;                             
				?>
					<div class="box-header with-border">
						<h3 class="box-title">USSD logs </h3>
					</div>
				
					<div class="box-body">
						<!--form class="form-horizontal" name="form_blacklist_table" id=""-->
						<div class="row">
							<div class="box-body boxpad contSeperator">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<table id="analyse-table" class="table table-bordered table-striped table-hover">
										<thead>
											<tr>
												<th>Session ID</th>
												<th>CDR</th>
												<th class="actions-fix-wd-2 text-center">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php Pjax::begin(); ?>
											<?php
											
											for($i=0; $i<sizeof($sesid3);$i++){ ?>
											<tr>
												<td><div><?php print_r($sesid3["$i"]); ?></div></td>
												<?php $wee=array(); 
													for ($j=0;$j<sizeof($final1);$j++){
														if($final2["$j"]['19']==$sesid3["$i"]){
															$wee[]=$final1["$j"];
															
															
														}															
													}
		
													$session['send']=$seszero;
													
													
													?>
												<td>
													<div>
                                                    <?php
														for($r=0;$r<sizeof($wee);$r++){
														print_r($wee["$r"]);
														?><br><?php 
											}
											
                                                    ?>
                                                </div>
                                            </td>
											<td><?= Html::a('<span class="glyphicon glyphicon-object-align-right"></span>', ['#'], ['data-url' => Url::toRoute(['diagram/display','id'=>$sesid3["$i"]]), 'id' => 'btn-view', 'title' => 'display']); ?></td>
                                        </tr>
                                        <?php } ?>
										<?php Pjax::end(); ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <!--/form-->
                </div>
                
            </div>
        </div>
    </div>
	</div>
	</div>
	</div>
</section>

<div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">USSD Path-flow</h4>
            </div>
            <iframe id="view-modal" name="view" src="" width="100%" height="490" frameborder="0"></iframe>
        </div>
    </div>
</div>







<?php
$urlView = Url::to('diagram/display');
$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-view', function(e) {
        $('#view-modal').attr("src", $(this).attr('data-url'));
        $('#ViewModal').modal({show:true})

        return false;
    });
});
JS;
$this->registerJs($script);
?>
