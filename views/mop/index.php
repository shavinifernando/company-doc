<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MopSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mops');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mop-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Mop'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'mopId',
            'ticketId',
            'openDate',
            'MOPCusSharedDate',
            'MOPCreator',
            //'MOPName',
            //'operator',
            //'issueDescription',
            //'solution',
            //'activityScheduleDate',
            //'linkToMOP',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
