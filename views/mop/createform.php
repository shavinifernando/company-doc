<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use app\models\Creator;
use app\models\Operator;
use app\models\User;
use yii\helpers\ArrayHelper;
//use yii\bootstrap\Modal;
$session = Yii::$app->session;
$userid = $session['user_id'];
$username = $session['user_name'];
$first_name = $session['firstname'];

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>

                        <div class="modal-body">
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'Create-form','class'=>'form-horizontal']]) ?>
                            <?php if (Yii::$app->session->hasFlash('success')): ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <?= Yii::$app->session->getFlash('success') ?>
                                </div>
                            <?php endif; ?>

                            <?php if (Yii::$app->session->hasFlash('error')): ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            <?php endif; ?>    
                            <div class="row">
                                    <div class="box-body boxpad">
                                        <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Ticket ID <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'ticketId',['options' => ['class' => 'col-sm-8','name' => 'ticketid','id' => 'ticketid']])->textInput(['maxlength' => true,'placeholder' => 'Ticket ID','data-toggle' => 'tooltip','data-placement'=>'bottom','title'=>'1234 or 12345','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>

                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"><abbr title="Method of Procedure">MOP</abbr> Name <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'MOPName',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => 'MOP Name','data-toggle' => 'tooltip','data-placement'=>'bottom','title'=>'MOP_Customer_TicketID_Product_DDMMYYYY_vx','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="start_datetime-tr" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Opened Date <span
                                                            class="req-fld">*</span>
                                                    </label>
                                                   <?=$form->field($model, 'openDate',['options' => ['class' => 'col-sm-8','id' => 'openDate']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'format' => 'yyyy-mm-dd'
                                                            ]
                                                        ]);?>
                                                </div>
                                            </div>

                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Customer Shared Date
                                                    </label>
                                                    <?=$form->field($model, 'MOPCusSharedDate',['options' => ['class' => 'col-sm-8','id' => 'MOPCusSharedDate']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'format' => 'yyyy-mm-dd'
                                                            ]
                                                        ]);?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"><abbr title="Root Cause Analysis">MOP</abbr> Creator <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'MOPCreator',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => $first_name,'autocomplete'=>'off', 'disabled' => true,'value' => $first_name])->label(false);?>
                                                    <!--?= $form->field($model, 'MOPCreator',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => User::getUser(),'options' => ['placeholder' => 'MOP Creator'],'pluginOptions' => ['allowClear' => true],]); ?-->
                                               
                                                </div>
                                            </div>
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Link to <abbr title="Root Cause Analysis">MOP</abbr> <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'linkToMOP',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => 'URL','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Issue Description <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'issueDescription',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Description','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Solution <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'solution',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Solution','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"> Activity Schedule Date
                                                    </label> -->
                                                    <!--?=$form->field($model, 'activityScheduleDate',['options' => ['class' => 'col-sm-8','id' => 'activityScheduleDate']])->label(false)->widget(DateTimePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD HH:MM'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'format' => 'yyyy-mm-dd hh:ii'
                                                            ]
                                                        ]);?-->
                                                <!-- </div>
                                            </div> -->
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Operator Name <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'operator',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => Operator::getOperator(),'options' => ['placeholder' => 'Operator'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>
                                            <!-- <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Technical Approval <span class="req-fld">*</span></label> -->
                                                    <!--?= $form->field($model, 'techApproval',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => ['Approved' => 'Approved', 'Not Approved' => 'Not Approved'],'options' => ['placeholder' => 'Technical Approval'],'pluginOptions' => ['allowClear' => true],]); ?-->
                                                <!-- </div>
                                            </div> -->
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Approve By<span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'techApprovedBy',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => User::getUser(),'options' => ['placeholder' => 'Approve By'],'pluginOptions' => ['allowClear' => true],]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                                     
                        </div>

                        <div class="modal-footer">
                            <!-- ?= Html::button('Close', ['class' => 'btn btn-default','name' => 'closeCreate', 'id' => 'closeCreate']) ?-->
                            <?= Html::submitButton('Create', ['class' => 'btn btn-primary','name' => 'create', 'id' => 'create']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeCreate').click(function() {
             window.parent.closeCreateModal();
         });  
    });
JS;

$this->registerJs($script);
?>
