<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>                    
                    <div class="modal-body">
                            <!-- <form class="form-horizontal"> -->
                            <?php $form = ActiveForm::begin(['options' => ['id' => 'Update-form','class'=>'form-horizontal']]) ?>
                            <?php if (Yii::$app->session->hasFlash('success')): ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <?= Yii::$app->session->getFlash('success') ?>
                                </div>
                            <?php endif; ?>

                            <?php if (Yii::$app->session->hasFlash('error')): ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            <?php endif; ?>     
                            <div class="row">
                                    <div class="box-body boxpad">

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ticket ID</label>
                                                <div class="col-sm-8" >
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->ticketId;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->MOPName;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Operator</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->operatorsName->operatorName;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Creator</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->creatorsName->firstname;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Technical Approval</label>
                                                <!-- <div class="col-sm-10"> -->
                                                    <?= $form->field($model, 'techApproval',['options' => ['class' => 'col-sm-8 detail-view-padding']])->radioList(array('1'=>'Approve',2=>'Reject'))->label(false);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Approve Comments</label>
                                                <!-- <div class="col-sm-10"> -->
                                                    <?= $form->field($model, 'approveComment',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Approve Comments','autocomplete'=>'off'])->label(false);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>

                                    </div>
                                </div> 
                            <!-- </form> -->                                                       
                        </div>

                        <div class="modal-footer">
                            <!-- ?= Html::button('Close', ['class' => 'btn btn-default','name' => 'closeUpdate', 'id' => 'closeUpdate']) ?-->
                            <?= Html::submitButton('Save', ['class' => 'btn btn-primary','name' => 'update', 'id' => 'update']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeApprove').click(function() {
             window.parent.closeApproveModal();
         });  
    });
JS;

$this->registerJs($script);
?>
