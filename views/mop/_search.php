<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MopSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mop-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'mopId') ?>

    <?= $form->field($model, 'ticketId') ?>

    <?= $form->field($model, 'openDate') ?>

    <?= $form->field($model, 'MOPCusSharedDate') ?>

    <?= $form->field($model, 'MOPCreator') ?>

    <?php // echo $form->field($model, 'MOPName') ?>

    <?php // echo $form->field($model, 'operator') ?>

    <?php // echo $form->field($model, 'issueDescription') ?>

    <?php // echo $form->field($model, 'solution') ?>

    <?php // echo $form->field($model, 'activityScheduleDate') ?>

    <?php // echo $form->field($model, 'linkToMOP') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
