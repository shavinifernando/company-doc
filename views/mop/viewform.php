<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-body">
                            <!-- <form class="form-horizontal"> -->
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'View-form', 'class' => 'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ticket ID</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->ticketId;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Opened Date</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->openDate;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Name</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->MOPName;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Customer Shared Date</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->MOPCusSharedDate;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Operator</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->operatorsName->operatorName;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Issue Description</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->issueDescription;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Solution</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->solution;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Creator</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->creatorsName->firstname;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Technical Approval</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->techApproval;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Approve By</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->approverName->firstname;?>
                                                </div>
                                            </div>
                                        </div>
					<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Activity Schedule Date</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->createactivity->scheduleactivity->scheduleTimeSL;?>
                                                </div>
                                            </div>
                                        </div>
					<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Approve Comments</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->approveComment;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Link to MOP</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <a href="<?= $model->linkToMOP;?>" target="_blank">
                                                        <?php echo $model->linkToMOP;?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div> 
                            <!-- </form>  -->                                                      
                        </div>
                        
                        <div class="modal-footer">
                            <?= Html::button('Close', ['class' => 'btn btn-default', 'name' => 'closeView', 'id' => 'closeView']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeView').click(function() {
             window.parent.closeViewModal();
         });  
    });
JS;

$this->registerJs($script);
?>
