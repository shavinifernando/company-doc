<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>                    
                    <div class="modal-body">
                    <?php if (Yii::$app->session->hasFlash('success')): ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <?= Yii::$app->session->getFlash('success') ?>
                                </div>
                            <?php endif; ?>

                            <?php if (Yii::$app->session->hasFlash('error')): ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            <?php endif; ?>   
                            <!-- <form class="form-horizontal"> -->
                            <?php $form = ActiveForm::begin(['options' => ['id' => 'Update-form','class'=>'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ticket ID</label>
                                                <div class="col-sm-8" >
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->ticketId;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Opened Date</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->openDate;?>'>      
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->MOPName;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Customer Shared Date</label>
                                                <!-- <div class="col-sm-10"> -->
                                                    <?=$form->field($model, 'MOPCusSharedDate',['options' => ['class' => 'col-sm-8','id' => 'MOPCusSharedDate']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'format' => 'yyyy-mm-dd'
                                                            ]
                                                        ]);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Technical Approval</label>
                                                    <!--?= $form->field($model, 'techApproval',['options' => ['class' => 'col-sm-10']])->label(false)->widget(Select2::classname(), ['data' => ['Approved' => 'Approved', 'Not Approved' => 'Not Approved'],'options' => ['placeholder' => 'Technical Approval'],'pluginOptions' => ['allowClear' => true],]); ?-->
                                                    <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->techApproval;?>'>
                                                    </div>
                                                </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Approve By</label>
                                                    <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo @$model->approverName->firstname;?>'>
                                                    </div>
                                                </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Operator</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->operatorsName->operatorName;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Issue Description</label>
                                                <!-- <div class="col-sm-10"> -->
                                                    <?= $form->field($model, 'issueDescription',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Description'])->label(false);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Solution</label>
                                                <!-- <div class="col-sm-10"> -->
                                                    <?= $form->field($model, 'solution',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Solution'])->label(false);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Activity Schedule Date</label>
                                                <!-- <div class="col-sm-10"> -->
                                                    <!--?=$form->field($model, 'activityScheduleDate',['options' => ['class' => 'col-sm-8','id' => 'activityScheduleDate']])->label(false)->widget(DateTimePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD HH:MM'],
                                                            'pluginOptions' => [
                                                                'autoclose' => true,
                                                                'format' => 'yyyy-mm-dd hh:ii'
                                                            ]
                                                        ]);?-->
                                                <!-- </div> -->
                                                <div class="col-sm-8" >
                                                    <input type="text" disabled class="form-control" value='<?php echo @$model->createactivity->scheduleactivity->scheduleTimeSL;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Creator</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->creatorsName->firstname;?>'>
                                                </div>
                                            </div>
                                        </div>

					<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Approve Comments</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo @$model->approveComment;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Link to MOP</label>
                                                <!-- <div class="col-sm-10"> -->
                                                    <?= $form->field($model, 'linkToMOP',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => 'URL'])->label(false);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>

                                    </div>
                                </div> 
                            <!-- </form> -->                                                       
                        </div>

                        <div class="modal-footer">
                            <!-- ?= Html::button('Close', ['class' => 'btn btn-default','name' => 'closeUpdate', 'id' => 'closeUpdate']) ?-->
                            <?= Html::submitButton('Save Changes', ['class' => 'btn btn-primary','name' => 'update', 'id' => 'update']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeUpdate').click(function() {
             window.parent.closeUpdateModal();
         });  
    });
JS;

$this->registerJs($script);
?>
