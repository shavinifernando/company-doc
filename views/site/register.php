<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegisterForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use app\models\Level;
use app\models\User;
$this->title = 'Operations Assistance Portal |  Wavenet';
?>
   <p class="login-box-msg">Create a new User</p>
   <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>
    <?php $form = ActiveForm::begin(); 
    $fieldOptions1 = [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
    ];
    $fieldOptions2 = [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
    ];
    $fieldOptions3 = [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
    ];
    $fieldOptions4 = [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => "{input}<span class='form-control-feedback'></span>"
    ];
    ?>
        <?= $form->field($model, 'username',$fieldOptions1)->textInput(['autofocus' => true])->input('email', ['placeholder' => "Email"])->label(false) ?>
        <!--?= $form->field($model, 'password',$fieldOptions2)->passwordInput()->input('password', ['placeholder' => "Password"])->label(false) ?-->
        <?= $form->field($model, 'firstname',$fieldOptions3)->textInput()->input('firstname', ['placeholder' => "First Name"])->label(false) ?>
        <?= $form->field($model, 'lastname',$fieldOptions3)->textInput()->input('lastname', ['placeholder' => "Last Name"])->label(false) ?>
        <?= $form->field($model, 'department',$fieldOptions4,['options' => 
		                                                    	['class' => 'col-sm-12']
		                                                    ])->label(false)->widget(Select2::classname(), [
		                                                    	'data' =>['Operations'=>'Operations','Support'=>'Support','Delivery'=>'Delivery'],
		                                                    	'changeOnReset' => true,
		                                                    	'options' => [
		                                                    		'placeholder' => 'Department', 
		                                                    		//'value' => ""
									],//
                                                            ]); ?>
        <?= $form->field($model, 'level',$fieldOptions4,['options' => 
		                                                    	['class' => 'col-sm-12']
		                                                    ])->label(false)->widget(Select2::classname(), [
		                                                    	'data' =>Level::getLevel(),
		                                                    	'changeOnReset' => true,
		                                                    	'options' => [
		                                                    		'placeholder' => 'Level', 
		                                                    		//'value' => ""
									],//
	                                                    	]); ?>
        <div class="row">
            <div class="col-xs-4">
                <?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'register-button']) ?>
            </div>
		</div>
        
           
        

    <?php ActiveForm::end(); ?>
</div>
<?php
//$this->registerJs($script);