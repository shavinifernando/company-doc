<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


$this->title = 'Operations Assistance Portal |  Wavenet';
?>
   <p class="login-box-msg">Change your password</p>
    <?php $form = ActiveForm::begin(); 
    $fieldOptions1 = [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
    ];
    
    ?>
        <?= $form->field($model, 'password',$fieldOptions1)->textInput(['autofocus' => true])->input('password', ['placeholder' => "New Password"])->label(false) ?>
        <?= $form->field($model, 'confirmpassword',$fieldOptions1)->input('password', ['placeholder' => "Confirm Password"])->label(false) ?>
        <div class="row">
			<div class="col-xs-8">
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton('Change', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
		</div>      

    <?php ActiveForm::end(); ?>
</div>
