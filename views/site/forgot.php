<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'Operations Assistance Portal | Wavenet';
?>
	<?php $form = ActiveForm::begin()?>
                          
			<div class="modal-body">
				<center>
					<p>Please enter your email address to send you the reset password link.</p>
					<?php if (Yii::$app->session->hasFlash('success')): ?>
						<div class="alert alert-success alert-dismissable">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							<?= Yii::$app->session->getFlash('success') ?>
						</div>
					<?php endif; ?>
					<?php if (Yii::$app->session->hasFlash('error')): ?>
						<div class="alert alert-danger alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								<?= Yii::$app->session->getFlash('error') ?>
						</div>
					<?php endif; ?>
						<div class="row">
							<div class="box-body boxpad">
								<div class="col-md-12">
									<div class="form-group">
										<div class="col-sm-12">
											<?= $form->field($model, 'username')->textInput(['autofocus' => true])->input('email', ['placeholder' => "Email"])->label(false) ?>
										</div>
									</div> 
								</div>
							</div>
						</div>
				</center>
			</div>
			<div class="modal-footer">
				<div class='row'>
					<div class="col-sm-9">
					</div>
					<div class="col-sm-3">
					<?= Html::submitButton('Send Reset Link', ['class' => 'btn btn-primary btn-block btn-flat pull-left', 'name' => 'reset-button']) ?>
					</div>
				</div>
				<!--button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button-->
				<!--button type="button" class="btn btn-primary">Send Reset Link</button-->
					</div>
			<?php ActiveForm::end(); ?>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>