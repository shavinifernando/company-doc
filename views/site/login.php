<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


$this->title = 'Operations Assistance Portal |  Wavenet';
?>
   <p class="login-box-msg">Sign in to start your session</p>
    <?php $form = ActiveForm::begin(); 
    $fieldOptions1 = [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
    ];
    $fieldOptions2 = [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
    ];
    $fieldOptions3 = [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => "<label>{input}</label>"
    ];
    ?>
        <?= $form->field($model, 'username',$fieldOptions1)->textInput(['autofocus' => true])->input('email', ['placeholder' => "Email"])->label(false) ?>
        <?= $form->field($model, 'password',$fieldOptions2)->passwordInput()->input('password', ['placeholder' => "Password"])->label(false) ?>
        <div class="row">
			<div class="col-xs-8">
                <?= $form->field($model, 'rememberMe', ['inputOptions' => ['id' => 'rememberMe']],$fieldOptions3)->checkbox([/*'class' => 'icheckbox_square-blue checked',*/]) ?> 
            </div> 
            <div class="col-xs-4">

                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
		</div>
        
           
        

    <?php ActiveForm::end(); ?>
    <a data-toggle="modal" data-target="#forgotPassword" style="cursor:pointer;">I forgot my password</a>
    
</div>
<div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" id="closeCreateButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Reset Password</h4>
        </div>
        <iframe id="create-view" name="create" src="<?= Url::to(['site/forgot']) ?>"  width="100%" height="200" frameborder="0"></iframe>
        </div>
    </div>
</div>


<?php
$urlView = Url::to('site/forgot');
//$urlView = Url::to('site/forgotPassword');

$script = <<< JS
    
    $( document ).ready(function() {
        window.closeCreateModal = function(){ 
            $('#forgotPassword').modal('hide');
            location.reload(); 
        };
    });
JS;
$this->registerJs($script);
?>
<?php
$script = <<< JS
  function() {
    $('rememberMe').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      //increaseArea: '20%' /* optional */
    });
  }
JS;
?>
<?php/*
$urlView = Url::to('site/forgot');
//$urlView = Url::to('site/forgotPassword');

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeCreateModal = function(){ 
    		$('#forgotPassword').modal('hide');
    		location.reload(); 
    	};
    });
JS;

$thisregisterJs($script);*/?>