<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CreateactivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Createactivities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="createactivity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Createactivity'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'activityId',
            'ticketId',
            'activityBy',
            'customer',
            'activity',
            //'comments',
            //'mopName',
            //'activityLevel',
            //'product',
            //'patch',
            //'description',
            //'outcome',
            //'output',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
