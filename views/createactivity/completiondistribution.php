<?php

//use dosamigos\datepicker\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\date\DatePicker;


$this->title = 'Operations Assistance Portal | Wavenet';

?>
<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Maintenance Activity
					<small>Distribution Graphs </small>
				</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> Dashboard</li>
					<li><i class="fa fa-laptop"></i> Resolution and Activities</li>
                    <li><i class=""></i> Maintenance Activity</li>
					<li class="active">Maintenance Activity Distribution</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
                            <div class="box-header with-border">
								<h3 class="box-title">Filters - Month</h3>
							</div>

							<div class="box-body">
								<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'form-horizontal']]) ?>
									<div class="row">
										<div class="box-body boxpad">
                                            <div id="start_datetime-tr" class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Month <span
                                                            class="req-fld">*</span>
													</label>
																<?=$form->field($model, 'date',['options' => ['class' => 'col-sm-8','id' => 'date']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM'],
																	'pluginOptions' => [
																		'autoclose' => true,
																		'startView'=>'year',
																		'minViewMode'=>'months',
																		'readonly'=>'true',
																		'format' => 'yyyy-mm'
																		]
																	]);?>
                                                </div>
                                            </div>

										</div>
									</div>
									<div class="row">
										<div class="box-body boxpad">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="box-footer">
														<?= Html::button('Generate Graph', ['class' => 'btn btn-primary pull-right btnspace','name' => 'graph', 'id' => 'graph']) ?>
												</div>
											</div>
										</div>						
									</div>
								<?php ActiveForm::end() ?>
							</div>
						</div>
					</div>
				</div>

                <div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">Maintenance Activity Distribution Graph</h3>
							</div>
							<div class="box-body">
								<canvas id="mycanvas"></canvas>
							</div>
						</div>
					</div>
                </div>
						<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">Report Data</h3>
							</div>
							<div class="box-body">
								<form class="form-horizontal" name="form_blacklist_table" id="">
									<div class="row">
										<div class="box-body boxpad contSeperator">
											<div id='table-parent' class="col-md-12 col-sm-12 col-xs-12">
												<table id="table" class="table table-bordered table-striped table-hover"></table>
											</div>
										</div>						
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
			</section>
			<!-- /.content -->
<?php
$url = Url::to(['createactivity/showcompletiondistribution']);
//echo $url;
$script = <<< JS
	$(document).ready(function(){
	
	$('#graph').bind("click", function () {
		

		  $.ajax({
			url : "{$url}",
			type : "POST",
			data: { date: $('#createactivity-date').val()},
			success : function(data){
			console.log(data);
			var data = JSON.parse(data);

			var filter = [];
			var Count = [];

            var len = data.length;

            for(var i in data) {
        		
        		filter.push(data[i].activityStatus);
                Count.push(parseInt(data[i].Count));
        
            }

            //alert(filter);

            var ctx = $("#mycanvas");
      

            var data = {
                labels : ["Successfully Completed", "Partially Completed", "Failed"],
                datasets : [
                {
                    label : "activityStatus",
                    data : Count,
                    backgroundColor : [
                        "#000066",
                        "#FF8000",
                        "#660033",
                        
                    ],
                    borderColor : [
                        "#FFFF00",
                        "#FFFF00",
                        "#FFFF00",
                        
                    ],
                    borderWidth : [2, 2, 2],


          }
        ]
      };

            var options = {
                legend : {
                    display : true,
                    position : "top"
                }
            };


            var chart = new Chart( ctx, {
                type : "pie",
                data : data,
                options : options
            });

            document.getElementById("table").innerHTML = "";

      		var col = [];
        	for (var i = 0; i < len; i++) {
            	for (var key in data[i]) {
                	if (col.indexOf(key) === -1) {
                    	col.push(key);
                	}
            	}
        	}

        	var table = document.getElementById("table");

        	var tr = table.insertRow(-1); 

        	for (var i = 0; i < col.length; i++) {
            	var th = document.createElement("th"); 
            	th.innerHTML = col[i];
            	tr.appendChild(th);
        	}

    
        	for (var i = 0; i < len; i++) {

            	tr = table.insertRow(-1);

            	for (var j = 0; j < col.length; j++) {
                	var tabCell = tr.insertCell(-1);
                	tabCell.innerHTML = data[i][col[j]];
            	}
        	} 

        	var tr = table.insertRow(-1); 
            var td = document.createElement("td");
            td.innerHTML = "Total";
            tr.appendChild(td);

            for (var j = 1; j < col.length; j++) {

                sum = 0;    

                for (var i = 0; i < len; i++) {

                    var td = document.createElement("td");
                    var sum = parseInt(sum) + parseInt(data[i][col[j]]);
                }
                    td.innerHTML = sum;
                    tr.appendChild(td);
            }
        
        	$("#table-parent").append(table);
        
        },
        error : function(data) {
            //console.log(data);
        }

        });
    });

});

JS;

$this->registerJs($script);
?>