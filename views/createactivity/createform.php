<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use app\models\Creator;
use app\models\Operator;
use app\models\Mop;
use app\models\Activityby;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
//use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>

                        <div class="modal-body">
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'Create-form','class'=>'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">

                                        <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Ticket ID <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'ticketId',['options' => ['class' => 'col-sm-8','name' => 'ticketId']])->textInput(['maxlength' => true,'placeholder' => 'Ticket ID','data-toggle' => 'tooltip','data-placement'=>'bottom','title'=>'1234 or 12345','autocomplete'=>'off','id' => 'ticketId'])->label(false);?>
                                                    <!--?= $form->field($model, 'ticketId',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => Mop::getTicket(),'options' => ['placeholder' => 'Ticket ID','id'=>'ticketId'],'pluginOptions' => ['allowClear' => true]]); ?-->
                                                </div>
                                            </div>

                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"><abbr title="Method of Procedure">MOP</abbr> Name</label>
                                                    <?= $form->field($model, 'mopName',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => (!$model->isNewRecord)?ArrayHelper::map(Mop::getName($model->ticketId),'mopId','MOPName'):null,
                                                        'options' => [
                                                            'placeholder' => 'MOP Name',
                                                            'id' => 'mopName',
                                                            'disabled'=>$model->mopName?false:true
                                                        ],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ]]); ?>
                                                    <!--?= $form->field($model, 'mopName',['options' => ['class' => 'col-sm-8']])->label(false)->widget(DepDrop::classname(), [
                                                        'data' => (!$model->isNewRecord)?ArrayHelper::map(Mop::getName($model->ticketId),'mopId','MOPName'):null,
                                                        'pluginOptions' => [
                                                            'allowClear' => true,
                                                            'placeholder' => 'MOP Name',
                                                            'depends'=>['ticketId'],
                                                            'url'=> Url::to(['get-mopname'])
                                                        ],
                                                        'type'=>DepDrop::TYPE_SELECT2
                                                    ]); ?-->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Activity By <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'activityBy',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => ActivityBy::getActivityby(),'options' => ['placeholder' => 'Activity By'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>

                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Activity Level <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'activityLevel',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => ['Minor' => 'Minor', 'Major' => 'Major','Critical' => 'Critical'],'options' => ['placeholder' => 'Activity Level'],'pluginOptions' => ['allowClear' => true],]); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Patch/Not </label>
                                                    <div class="col-sm-8">
                                                        <div >
                                                            <div class="col-xs-2">
                                                                <div class="form-group">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input id="patchCheck" type="checkbox">
                                                                        </label>
                                                                    </div>
                                                                </div>                                                                
                                                            </div>
                                                                    <?= $form->field($model, 'patch',['options' => ['class' => 'col-xs-10','id' =>'patchNo','style'=>'display: none;']])->textInput(['maxlength' => true,'placeholder' => 'Patch Number','autocomplete'=>'off'])->label(false);?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Product <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'product',['options' => ['class' => 'col-sm-8']])->textInput(['maxlength' => true,'placeholder' => 'Product','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Customer <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'customer',['options' => ['class' => 'col-sm-8']])->label(false)->widget(Select2::classname(), ['data' => Operator::getOperator(),'options' => ['placeholder' => 'Customer'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>



                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Activity <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'activity',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Activity','data-toggle' => 'tooltip','data-placement'=>'bottom','title'=>'Max 100 Characters','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Description <span class="req-fld">*</span></label>
                                                    <?= $form->field($model, 'description',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Description','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                    
                                            <div id="" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Comments </label>
                                                    <?= $form->field($model, 'comments',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Comments','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>
                                        </div>
                                            
                                    </div>
                                </div>                                                     
                        </div>

                        <div class="modal-footer">
                            <!-- ?= Html::button('Close', ['class' => 'btn btn-default','name' => 'closeCreate', 'id' => 'closeCreate']) ?-->
                            <?= Html::submitButton('Create', ['class' => 'btn btn-primary','name' => 'create', 'id' => 'create']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeCreate').click(function() {
             window.parent.closeCreateModal();
         });  
    });
JS;

$this->registerJs($script);

$script = <<< JS
$(function () {

    $("#patchCheck").change(function() {
        if( $(this).prop("checked") ){
            $("#patchNo").show();
        }else{
            $("#patchNo").hide();
        }
    });

});
JS;

$this->registerJs($script);

$url = Url::toRoute(["getmopname"]);
$this->registerJs("var mopUrl = '{$url}';");
$script = <<< JS

$('#ticketId').on('focusout', function(e) {
    e.preventDefault();
    var ticketId = $(this).val();
    if(ticketId.length >0) {
        $('#mopName').prop('disabled',true);
        $.ajax({
            async : true,
            encode: true,
            url   : mopUrl +"&ticket_id="+ ticketId,
            type  : 'post',
            error : function (xhr, status, error) {
                        alert('There was an error on getting odometer reading info.');
                    }
        }).done(function(data) {
            e.preventDefault();
            $('#mopName').html(data);
            $('#mopName').prop('disabled',false);
        });
    }
});
JS;

$this->registerJs($script);

?>
