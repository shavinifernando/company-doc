<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use app\models\Creator;
use app\models\Operator;
use app\models\User;
use app\models\Level;


$this->title = 'Operations Assistance Portal | Wavenet';

?>
<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Maintenance Activity
					<small>Resolution and Activities</small>
				</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> Dashboard</li>
					<li><i class="fa fa-laptop"></i> Performance</li>
                    <li><i class=""></i> Resolution and Activities</li>
					<li class="active">Maintenance Activity</li>
				</ol>
			</section>
			
			<div class="floating-button" id="floating-button" data-toggle="modal" data-target="#CreateActivityModal" title="Create Maintenence Activity"><i class="fa fa-plus"></i></div>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
                            <div class="box-header with-border">
								<h3 class="box-title">Filters</h3>
							</div>

							<div class="box-body">
								<?php $form = ActiveForm::begin(['method'=>'get'],['options' => ['enctype' => 'multipart/form-data', 'class'=>'form-horizontal']]) ?>
									<div class="row">
										<div class="box-body boxpad">

                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Start</label>
                                                         <?=$form->field($searchModel, 'start',['options' => ['class' => 'col-sm-9','id' => 'start']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD','data-placement'=>'bottom','title'=>'Activity Scheduled Date'],
															'pluginOptions' => [
																'autoclose' => true,
																'format' => 'yyyy-mm-dd'
															]
														]);?>  
                                                    </div>
                                            </div>

                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">End</label>
                                                            <?=$form->field($searchModel, 'end',['options' => ['class' => 'col-sm-9','id' => 'end']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM/DD','data-placement'=>'bottom','title'=>'Activity Scheduled Date'],
															'pluginOptions' => [
																'autoclose' => true,
																'format' => 'yyyy-mm-dd'
															]
														]);?>
                                                </div>
                                            </div>

                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Ticket ID</label>
                                                    <?= $form->field($searchModel, 'id',['options' => ['class' => 'col-sm-9']])->textInput(['maxlength' => true,'placeholder' => 'Ticket ID','autocomplete'=>'off'])->label(false);?>
                                                </div>
                                            </div>

										</div>
									</div>
                                    <div class="row">
                                        <div class="box-body boxpad">
                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Customer</label>
                                                        <!--?= $form->field($searchModel, 'customerName',['options' => ['class' => 'col-sm-9']])->textInput(['maxlength' => true,'placeholder' => 'Customer'])->label(false);?-->
							<?= $form->field($searchModel, 'customerName',['options' => ['class' => 'col-sm-9','id'=>'selectCustomer']])->label(false)->widget(Select2::classname(), ['data' => Operator::getOperator(),'options' => ['placeholder' => 'Customer'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>
                                            <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">L1 Resource</label>
                                                        <!--?= $form->field($searchModel, 'resourceName',['options' => ['class' => 'col-sm-9']])->textInput(['maxlength' => true,'placeholder' => 'L1 Resource'])->label(false);?-->
							<?= $form->field($searchModel, 'resourceName',['options' => ['class' => 'col-sm-9']])->label(false)->widget(Select2::classname(), ['data' => User::getLevelInfo(1),'options' => ['placeholder' => 'L1 Resource'],'pluginOptions' => ['allowClear' => true]]); ?>
                                                </div>
                                            </div>
					   <div id="start_datetime-tr" class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Manager Approval </label>
                                                    <?= $form->field($searchModel, 'approval',['options' => ['class' => 'col-sm-9']])->label(false)->widget(Select2::classname(), ['data' => ['Approved' => 'Approved', 'To Be Approved' => 'To Be Approved'],'options' => ['placeholder' => 'Manager Approval'],'pluginOptions' => ['allowClear' => true],]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
				    <div class="row">
                                    	<div class="box-body boxpad">
					    <div id="start_datetime-tr" class="col-md-4">
						<div class="form-group">
                                                	<label class="col-sm-3 control-label">Activity Status</label>					
							<?= $form->field($searchModel, 'status',['options' => ['class' => 'col-sm-9']])->label(false)->widget(Select2::classname(), ['data' => ['Successfully Completed' => 'Successfully Completed','Partially Successful' => 'Partially Successful','Failed' => 'Failed','Scheduled' => 'Scheduled','To Be Scheduled'=>'To Be Scheduled','Canceled'=>'Canceled'],'options' => ['placeholder' => 'Activity Status'],'pluginOptions' => ['allowClear' => true],]); ?>
						</div>
					   </div>
				       </div>						
				    </div>
									<div class="row">
										<div class="box-body boxpad">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="box-footer">										<?= Html::submitButton('Search', ['class' => 'btn btn-primary pull-right btnspace','name' => 'search', 'id' => 'search']) ?>
	 <?= Html::resetButton('Reset', ['class' => 'btn btn-default pull-right btnspace','name' => 'resetSearch', 'id' => 'resetSearch']) ?>
												</div>
											</div>
										</div>						
									</div>
								<?php ActiveForm::end() ?>
							</div>
						</div>
					</div>
				</div>

				<div id="data_area">

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Search Results</h3>
								</div>
								<div class="box-body">
									<form class="form-horizontal" name="form_blacklist_table" id="">
										<div class="row">
											<div class="box-body boxpad contSeperator">
												<div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive row">
                                                    <div class="col-md-12">
													    <?= GridView::widget([
			                            				'dataProvider' => $dataProvider,
			                            				//'filterModel' => $searchModel,
			                                   			'showOnEmpty'=>true,
			                            				'columns' => [
			                              					//['class' => 'yii\grid\SerialColumn'],
													        /*[
													            'header' => 'Ticket ID',
													            'attribute' => 'ticketId',
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],*/
														[
            													'header' => 'Ticket ID',
            													'format'=>'raw',
            													'value' => function($model){
                												$url = "http://wavenet.freshdesk.com//helpdesk/tickets/$model->ticketId";
                												return Html::a($model->ticketId, $url, ['title' => 'Freshdesk Ticket Link','target' => '_blank']); 
            													},
														'headerOptions' => ['style'=>'vertical-align: middle;'],
        													],
													        [
													            'header' => 'Activity Status',
													            'attribute' => 'activityStatus',
													            /*'value' =>function($model){ return @$model->scheduleactivity->activityStatus;},*/
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
														[
													            'header' => 'Manager Approval',
													            'attribute' => 'managerApproval',
													            'value' =>function($model){ return @$model->scheduleactivity->managerApproval;},
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													         [
													            'header' => 'Schedule Time SL',
													            'attribute' => 'scheduleactivity.scheduleTimeSL',
													            'value' =>function($model){ return @$model->scheduleactivity->scheduleTimeSL;},
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'Schedule Time Other',
													            'attribute' => 'scheduleactivity.scheduleTimeOther',
													            'value' =>function($model){ return @$model->scheduleactivity->scheduleTimeOther;},
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'Activity By',
													            'attribute' => 'activityBy',
													            'value' =>function($model){ return @$model->activitybyNames->activityByName;},
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'L1 Resource',
													            'attribute' => 'scheduleactivity.L1Resource',
													            'value' =>function($model){ return @$model->scheduleactivity->level1ResourceName->firstname;},
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'Customer',
													            'attribute' => 'customer',
													            'value' =>function($model){ return @$model->operatorsName->operatorName;},
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'Product',
													            'attribute' => 'product',
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        /*[
													            'header' => 'MOP Name',
													            'attribute' => 'mopsName.MOPName',
													            'value' =>function($model){ return @$model->mopsName->MOPName;},
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],*/
														[
													            'header' => 'MOP Name',
													            'attribute' => 'mopsName.MOPName',
													            'format'=>'raw',
													            'value' =>function($model){
																return Html::a(@$model->mopsName->MOPName, ['#'], ['class' => 'mopView','title' => 'Link to MOP','datalink' => Url::toRoute(['createactivity/mopview', 'id' => $model->activityId]), 'id' => 'btn-mopView'
																	]);
													            },
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        [
													            'header' => 'Activity',
													            'attribute' => 'activity',
													            'headerOptions' => ['style'=>'vertical-align: middle;'],
													        ],
													        ['class' => 'yii\grid\ActionColumn','header' => 'Actions','template'=>'{view} {update} {scheduleactivity/schedule}{deleting}{approve}',
													        'buttons' => [
															'view' => function ($url,$model) {
																return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',['#'], ['data-url' => Url::toRoute(['createactivity/view', 'id' => $model->activityId]), 'id' => 'btn-view','title'=>'View Activity']);
															},
															'update' => function ($url,$model) {
																return Html::a('<span class="glyphicon glyphicon-pencil"></span>',['#'], ['data-url' => Url::toRoute(['createactivity/update', 'id' => $model->activityId]), 'id' => 'btn-update','title'=>'Update Activity']);
															},
															'scheduleactivity/schedule' => function ($url,$model) {
																if($model->activityStatus == "To Be Scheduled") {
																return Html::a('<span class="glyphicon glyphicon-calendar"></span>',['#'], ['data-url' => Url::toRoute(['scheduleactivity/schedule', 'id' => $model->activityId]), 'id' => 'btn-schedule','title'=>'Schedule Activity']);
																}
															},
															
															'deleting' => function ($url,$model) {
																if($model->activityStatus == "To Be Scheduled") {
																return Html::a('<span class="glyphicon glyphicon-remove"></span>',['#'], ['data-url' => Url::toRoute(['createactivity/deleting', 'id' => $model->activityId]), 'id' => 'btn-deleting','title'=>'Cancel Activity']);
																}
															}, 
															'approve' => function ($url,$model) {
																if(@$model->scheduleactivity->managerApproval == "To Be Approved") {
																return Html::a('<span class="glyphicon glyphicon-ok"></span>',['#'], ['data-url' => Url::toRoute(['createactivity/approve', 'id' => $model->activityId]), 'id' => 'btn-approve','title'=>'Manager Approval']);
																}
															},
															],
													        'headerOptions' => ['style'=>'vertical-align: middle; width:120px;']
													    	],
			                            					],
			                          					]); ?>
                                                        </div>                                                    
                                                    </div>
													
												</div>
											</div>						
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

				</div>
                

                <!-- Create modal -->               
               <div class="modal fade" id="CreateActivityModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                    	<div class="modal-content">
                    	<div class="modal-header">
                            <button type="button" id="closeCreateButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Maintenance Activity </h4>
                        </div>
                    	<iframe id="create-view" name="create" src="<?= Url::to(['createactivity/create']) ?>"  width="100%" height="550" frameborder="0">
                    	</iframe>
                        </div>
					</div>
               </div>

                <!-- View modal -->
                <div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Maintenance Activity Details</h4>
                        </div>
                        <iframe id="view-modal" name="view" src=""  width="100%" height="800" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
                
                <!-- Edit modal -->
                    <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="closeUpdateButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Maintenance Activity</h4>
                        </div>
                        <iframe id="update-view" name="update" src=""  width="100%" height="600" frameborder="0"></iframe>
                        <?php echo yii::$app->session->getFlash('messsage'); ?>
                        </div>
                    </div>
                </div>

                <!-- Schedule modal -->
                    <div class="modal fade" id="ScheduleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="closeScheduleButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Schedule Maintenance Activity</h4>
                        </div>
                        <iframe id="schedule-view" name="schedule" src=""  width="100%" height="500" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
				<!-- Cancel modal -->
				<div class="modal fade" id="CancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        
                        <iframe id="cancel-view" name="cancel" src=""  width="100%" height="170" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>

	       <!-- MOP View modal -->
                    <div class="modal fade" id="MopViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="closeMopViewButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">MOP Details</h4>
                        </div>
                        <iframe id="mopview-modal" name="mopViewModal" src=""  width="100%" height="450" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>

	      <!-- Approve modal -->
                <div class="modal fade" id="ApproveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="closeApproveButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Manager Approval</h4>
                        </div>
                        <iframe id="approve-view" name="approve" src=""  width="100%" height="410" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>

                
			
			</section>
			<!-- /.content -->
<?php

$urlView = Url::to('createactivity/view');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-view', function(e) {
        $('#view-modal').attr("src", $(this).attr('data-url'));
        $('#ViewModal').modal({show:true})

        return false;
    });
});
JS;

$this->registerJs($script);
$urlView = Url::to('createactivity/deleting');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-deleting', function(e) {
        $('#cancel-view').attr("src", $(this).attr('data-url'));
        $('#CancelModal').modal({show:true})

        return false;
    });
});
JS;

$this->registerJs($script);

$urlUpdate = Url::to('createactivity/update');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-update', function(e) {
        $('#update-view').attr("src", $(this).attr('data-url'));
        $('#EditModal').modal({show:true})
        return false;
    });
});
JS;

$this->registerJs($script);

$urlUpdate = Url::to('scheduleactivity/schedule');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-schedule', function(e) {
        $('#schedule-view').attr("src", $(this).attr('data-url'));
        $('#ScheduleModal').modal({show:true})
        return false;
    });
});
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeCreateModal = function(){ 
    		$('#CreateActivityModal').modal('hide');
    		location.reload(); 
    	};
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeViewModal = function(){ $('#ViewModal').modal('hide'); };
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeUpdateModal = function() { 
    		$('#EditModal').modal('hide');
    		location.reload();
    	};
 		
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeScheduleModal = function() { 
    		$('#ScheduleModal').modal('hide');
    		location.reload();
    	};
 		
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeCancelModal = function() { 
    		$('#CancelModal').modal('hide');
    		location.reload();
    	};
 		
    });
JS;

$this->registerJs($script);
/*$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeAddOperatorModal = function() { 
    		$('#CreateActivityModal').modal('hide');
    	};
 		
    });
JS;

$this->registerJs($script);*/

$script = <<< JS
$( document ).ready(function() {
       
        $('#closeCreateButton').click(function() {
             window.parent.closeCreateModal();
         });  
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeCreateModal = function(){ 
    		$('#CreateMOPModal').modal('hide'); 
    		location.reload();
    	};
    });
JS;

$this->registerJs($script);

$script = <<< JS
$( document ).ready(function() {
       
        $('#closeUpdateButton').click(function() {
             window.parent.closeUpdateModal();
         });  
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeUpdateModal = function(){ 
    		$('#EditModal').modal('hide'); 
    		location.reload();
    	};
    });
JS;

$this->registerJs($script);

$script = <<< JS
$( document ).ready(function() {
       
        $('#closeScheduleButton').click(function() {
             window.parent.closeScheduleModal();
         });  
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeScheduleModal = function(){ 
    		$('#ScheduleModal').modal('hide'); 
    		location.reload();
    	};
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeCancelModal = function(){ 
    		$('#CancelModal').modal('hide'); 
    		location.reload();
    	};
    });
JS;

$this->registerJs($script);

$script = <<< JS
$( document ).ready(function() {
    $('#resetSearch').click(function() {
        $('#createactivitysearch-customername').val(null).trigger("change");
	$('#createactivitysearch-resourcename').val(null).trigger("change");
        });  
    });
JS;

$this->registerJs($script);

$urlMOPView = Url::to('createactivity/mopview');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-mopView', function(e) {
        $('#mopview-modal').attr("src", $(this).attr('datalink'));
        $('#MopViewModal').modal({show:true})

        return false;
    });
});
JS;

$this->registerJs($script);

$script = <<< JS
$( document ).ready(function() {
       
        $('#closeMopViewButton').click(function() {
             window.parent.closeMopViewModal();
         });  
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeMopViewModal = function(){ 
    		$('#MopViewModal').modal('hide'); 
    		location.reload();
    	};
    });
JS;

$this->registerJs($script);

$urlUpdate = Url::to('createactivity/approve');

$script = <<< JS
$( document ).ready(function() {
    $(document).on('click', '#btn-approve', function(e) {
        $('#approve-view').attr("src", $(this).attr('data-url'));
        $('#ApproveModal').modal({show:true})

        return false;
    });
});
JS;

$this->registerJs($script);

$script = <<< JS
$( document ).ready(function() {
       
        $('#closeApproveButton').click(function() {
             window.parent.closeApproveModal();
         });  
    });
JS;

$this->registerJs($script);

$script = <<< JS
	
    $( document ).ready(function() {
    	window.closeApproveModal = function(){ 
    		$('#ApproveModal').modal('hide'); 
    		location.reload();
    	};
    });
JS;

$this->registerJs($script);
?>

