<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>                    
                    <div class="modal-body">
                            <!-- <form class="form-horizontal"> -->
                            <?php $form = ActiveForm::begin(['options' => ['id' => 'Update-form','class'=>'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ticket ID</label>
                                                <div class="col-sm-8" >
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->ticketId;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Activity</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo $model->activity;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Schedule Time SL</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo @$model->scheduleactivity->scheduleTimeSL;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Schedule Time Other</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo @$model->scheduleactivity->scheduleTimeOther;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">L1 Resource</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo @$model->scheduleactivity->level1ResourceName->firstname;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">L2 Resource</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo @$model->scheduleactivity->level2ResourceName->firstname;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">L3 Resource</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo @$model->scheduleactivity->level3ResourceName->firstname;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Customer</label>
                                                <div class="col-sm-8">
                                                    <input type="text" disabled class="form-control" value='<?php echo @$model->operatorsName->operatorName;?>'>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Manager Approval</label>
                                                <!-- <div class="col-sm-10"> -->
                                                    <?= $form->field($model, 'managerApproval',['options' => ['class' => 'col-sm-8']])->radioList(array('1'=>'Approve',2=>'Reject'))->label(false);?>
                                                <!-- </div> -->
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Comments </label>
                                                    <?= $form->field($model, 'approvalComment',['options' => ['class' => 'col-sm-8']])->textarea(['rows' => '3','maxlength' => true,'placeholder' => 'Comments'])->label(false);?>
                                                </div>
                                        </div>

                                    </div>
                                </div> 
                            <!-- </form> -->                                                       
                        </div>

                        <div class="modal-footer">
                            <!-- ?= Html::button('Close', ['class' => 'btn btn-default','name' => 'closeUpdate', 'id' => 'closeUpdate']) ?-->
                            <?= Html::submitButton('Save', ['class' => 'btn btn-primary','name' => 'update', 'id' => 'update']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeApprove').click(function() {
             window.parent.closeApproveModal();
         });  
    });
JS;

$this->registerJs($script);
?>
