<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\date\DatePicker;

$this->title = 'Operations Assistance Portal | Wavenet';

?>
<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Maintenance Activity
					<small>Trend Graphs </small>
				</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> Dashboard</li>
					<li><i class="fa fa-laptop"></i> Resolution and Activities</li>
                    <li><i class=""></i> Maintenance Activity</li>
					<li class="active">Maintenance Activity Distribution</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="box">
                            <div class="box-header with-border">
								<h3 class="box-title">Filters</h3>
							</div>

							<div class="box-body">
								<!-- <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data"  name="form_blacklist" id="form_blacklist"> -->
								<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'form-horizontal']]) ?>
									<div class="row">
										<div class="box-body boxpad">

                                            <div id="start_datetime-tr" class="col-md-5">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Start <span
                                                            class="req-fld">*</span>
													</label>
                                                    <!-- <div class="col-sm-9"> -->
														<!-- <div class="form-group"> -->
															<?=$form->field($model, 'start',['options' => ['class' => 'col-sm-9','id' => 'start']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM'],
                                                                    'pluginOptions' => [
                                                                        'autoclose' => true,
                                                                        'startView'=>'year',
                                                                        'minViewMode'=>'months',
                                                                        'readonly'=>'true',
                                                                        'format' => 'yyyy-mm'
                                                                        ]
                                                                    ]);?>
														<!-- </div> -->

                                                    <!-- </div> -->
                                                </div>
                                            </div>

                                            <div id="start_datetime-tr" class="col-md-5">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">End <span
                                                            class="req-fld">*</span>
													</label>
                                                    <!-- <div class="col-sm-9"> -->
														<!-- <div class="form-group"> -->
															<?=$form->field($model, 'end',['options' => ['class' => 'col-sm-9','id' => 'end']])->label(false)->widget(DatePicker::classname(), ['options' => ['placeholder' => 'YYYY/MM'],
                                                                    'pluginOptions' => [
                                                                        'autoclose' => true,
                                                                        'startView'=>'year',
                                                                        'minViewMode'=>'months',
                                                                        'readonly'=>'true',
                                                                        'format' => 'yyyy-mm'
                                                                        ]
                                                                    ]);?>
														<!-- </div> -->

                                                    <!-- </div> -->
                                                </div>
                                            </div>

										</div>
									</div>
									<div class="row">
										<div class="box-body boxpad">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="box-footer">
														<?= Html::button('Generate Graph', ['class' => 'btn btn-primary pull-right btnspace','name' => 'graph', 'id' => 'graph']) ?>
											        	<!-- <input type="submit" class="btn btn-primary pull-right btnspace" name="submit" id="gen_graph" value="Generate Graph" > -->
												</div>
											</div>
										</div>						
									</div>
								<?php ActiveForm::end() ?>
								<!-- </form> -->
							</div>
						</div>
					</div>
				</div>

				<div id="data_area">
					<div class="row">
						<div class="col-md-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Maintenance Activity Trend Graphs</h3>
								</div>
								<div class="box-body">
									<canvas id="mycanvas"></canvas>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Report Data</h3>
								</div>
								<div class="box-body">
									<form class="form-horizontal" name="form_blacklist_table" id="">
										<div class="row">
											<div class="box-body boxpad contSeperator">
												<div id='table-parent' class="col-md-12 col-sm-12 col-xs-12" style="overflow:scroll;width:100%;overflow:auto">
													<table id="table" class="table table-bordered table-striped table-hover">
													</table>
												</div>
											</div>						
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

				</div>
                
			
			</section>
			<!-- /.content -->
<?php
$url = Url::to(['createactivity/showcompletiontrend']);
//echo $url;
$script = <<< JS
	$(document).ready(function(){

	$('#graph').bind("click", function () {

  		$.ajax({
    			url : "{$url}",
    			type : "POST",
				data: { start: $('#createactivity-start').val(), end: $('#createactivity-end').val()},
    			success : function(data){
      			console.log(data);
      			var data = JSON.parse(data);

      			var Month = [];
      			var Success = [];
      			var Failed = [];
      			var Partial = [];
      			var Total = [];

      			for(var i in data) {
        			Month.push(data[i].Month);
        			Success.push(data[i].Success);
        			Failed.push(data[i].Failed);
        			Partial.push(data[i].Partial);
        			Total.push(data[i].Total);
      			}

      			var chartdata = {
        			labels: Month,
        			datasets: [
          			{
            				label: "Successfully Completed",
            				fill: false,
            				backgroundColor: "rgba(59, 89, 152, 0.2)",
            				borderColor: "rgba(59, 89, 152, 1)",
            				pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
            				pointHoverBorderColor: "rgba(59, 89, 152, 1)",
            				borderWidth: 1,
            				data: Success
          			},
	  				{
            				label: "Failed",
            				fill: false,
            				backgroundColor: "rgba(102, 0, 204, 0.2)",
            				borderColor: "rgba(102, 0, 204, 1)",
            				pointHoverBackgroundColor: "rgba(102, 0, 204, 1)",
            				pointHoverBorderColor: "rgba(102, 0, 204, 1)",
            				borderWidth: 1,
            				data: Failed
          			},
          			{ 
            				label: "Partially Completed",
            				fill: false,
            				backgroundColor: "rgba(255, 255, 0, 0.2)",
            				borderColor: "rgba(255, 255, 0, 1)",
            				pointHoverBackgroundColor: "rgba(255, 255, 0, 1)",
            				pointHoverBorderColor: "rgba(255, 255, 0, 1)",
            				borderWidth: 1,
            				data: Partial
          			},
          			{
            				label: "Total",
            				fill: false,
            				backgroundColor: "rgba(0, 153, 0, 0.2)",
            				borderColor: "rgba(0, 153, 0, 1)",
           	 			    pointHoverBackgroundColor: "rgba(0, 153, 0, 1)",
            				pointHoverBorderColor: "rgba(0, 153, 0, 1)",
            				borderWidth: 1,
            				data: Total
          			}
        		]
      		};

      				var ctx = $("#mycanvas");

              if(window.LineGraph != undefined)
                
                window.LineGraph.destroy();

      				window.LineGraph = new Chart(ctx, {
        				type: 'line',
        				data: chartdata,
        				options: {
        					scales: {
            					xAxes: [{
                					display: true,
                					scaleLabel: {
                    					display: true,
                    					labelString: 'Month'
                					}
            					}],
            					yAxes: [{
                					display: true,
                					scaleLabel: {
                    					display: true,
                    					labelString: 'Count'
                					},
                				ticks: {
                    				beginAtZero:true
                				}
            				}]
        				}
    				}
      				});

      		document.getElementById("table").innerHTML = "";

      		var col = [];
        	for (var i = 0; i < data.length; i++) {
            	for (var key in data[i]) {
                	if (col.indexOf(key) === -1) {
                    	col.push(key);
                	}
            	}
        	}

        	//alert(data.length);

        	var table = document.getElementById("table");

        	var tr = table.insertRow(-1); 

        	for (var i = 0; i < col.length; i++) {
            	var th = document.createElement("th"); 
            	th.innerHTML = col[i];
            	tr.appendChild(th);
        	}

    
        	for (var i = 0; i < data.length; i++) {

            	tr = table.insertRow(-1);

            	for (var j = 0; j < col.length; j++) {
                	var tabCell = tr.insertCell(-1);
                	tabCell.innerHTML = data[i][col[j]];
            	}
        	} 

        	  var tr = table.insertRow(-1); 
            var td = document.createElement("td");
            td.innerHTML = "Total";
            tr.appendChild(td);

            for (var j = 1; j < col.length; j++) {

                sum = 0;    

                for (var i = 0; i < data.length; i++) {

                    var td = document.createElement("td");
                    var sum = parseInt(sum) + parseInt(data[i][col[j]]);
                }
                    td.innerHTML = sum;
                    tr.appendChild(td);
            }
        
        $("#table-parent").append(table);
        
			},
    				error : function(data) {

    			}
  			});
		});
	});

JS;

$this->registerJs($script);
?>