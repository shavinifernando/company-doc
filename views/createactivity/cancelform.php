<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>                    
                    <div class="modal-body">
                            <!-- <form class="form-horizontal"> -->
                            <?php $form = ActiveForm::begin(['options' => ['id' => 'Update-form','class'=>'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">
                                    <div id="" class="col-md-2 col-sm-2 col-xs-12"></div>
                                    <div id="" class="col-md-8 col-sm-8 col-xs-12">
                                            <div class="form-group">
                                                <h3>Are you sure you want to cancel the scheduled Activity?</h3> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- </form> -->                                                       
                        </div>
                        <div class="modal-footer">
                            <!-- ?= Html::button('Close', ['class' => 'btn btn-default','name' => 'closeUpdate', 'id' => 'closeUpdate']) ?-->
                            <?= Html::submitButton('Back', ['class' => 'btn btn-primary','name' => 'closeCancel', 'id' => 'closeCancel']) ?>
                            <?= Html::submitButton('Cancel', ['class' => 'btn btn-primary','name' => 'delete', 'id' => 'delete']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeCancel').click(function() {
             window.parent.closeCancelModal();
         });  
    });
JS;

$this->registerJs($script);
?>
