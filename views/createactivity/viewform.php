<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-body">
                            <!-- <form class="form-horizontal"> -->
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'View-form', 'class' => 'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ticket ID</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->ticketId;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Schedule Time SL</label>
                                               <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->scheduleTimeSL;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Schedule Time Other</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->scheduleTimeOther;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Activity Status</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->activityStatus;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">L1 Resource</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->level1ResourceName->firstname;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">L2 Resource</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->level2ResourceName->firstname;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">L3 Resource</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->level3ResourceName->firstname;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Customer Contact Point</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->cusContactPoint;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Activity By</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->activitybyNames->activityByName;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Customer</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->operatorsName->operatorName;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Product</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->product;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Name</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->mopsName->MOPName;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Activity</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->activity;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Customer Approval</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->cusApproval;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Manager Approval</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->managerApproval;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Technical Approval</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->techApproval;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Activity Level</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->activityLevel;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Patch Number</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->patch;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Description</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->description;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Output</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->output;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Outcome</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->outcome;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Engineer Responsible after MW</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->engResponsible;?>
                                                </div>
                                            </div>
                                        </div>
					<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Comments</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->comments;?>
                                                </div>
                                            </div>
                                        </div>
					<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Approve Comments</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->approvalComment;?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div> 
                            <!-- </form>  -->                                                      
                        </div>
                        
                        <div class="modal-footer">
                            <?= Html::button('Close', ['class' => 'btn btn-default', 'name' => 'closeView', 'id' => 'closeView']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeView').click(function() {
             window.parent.closeViewModal();
         });  
    });
JS;

$this->registerJs($script);
?>
