<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CreateactivitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="createactivity-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'activityId') ?>

    <?= $form->field($model, 'ticketId') ?>

    <?= $form->field($model, 'activityBy') ?>

    <?= $form->field($model, 'customer') ?>

    <?= $form->field($model, 'activity') ?>

    <?php // echo $form->field($model, 'comments') ?>

    <?php // echo $form->field($model, 'mopName') ?>

    <?php // echo $form->field($model, 'activityLevel') ?>

    <?php // echo $form->field($model, 'product') ?>

    <?php // echo $form->field($model, 'patch') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'outcome') ?>

    <?php // echo $form->field($model, 'output') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
