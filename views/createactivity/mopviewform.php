<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rca */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-body">
                            <!-- <form class="form-horizontal"> -->
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'View-form', 'class' => 'form-horizontal']]) ?>
                                <div class="row">
                                    <div class="box-body boxpad">
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ticket ID</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->mop->ticketId;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Opened Date</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->mop->openDate;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Name</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->mop->MOPName;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Customer Shared Date</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->mop->MOPCusSharedDate;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Operator</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->operatorsName->operatorName;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Issue Description</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->mop->issueDescription;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Solution</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->mop->solution;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">MOP Creator</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->mop->creatorsName->firstname;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Technical Approval</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo $model->mop->techApproval;?>
                                                </div>
                                            </div>
                                        </div>
										<div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Approve By</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->mop->approverName->firstname;?>
                                                </div>
                                            </div>
                                        </div>
					                   <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Activity Schedule Date</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->scheduleactivity->scheduleTimeSL;?>
                                                </div>
                                            </div>
                                        </div>
					                   <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Approve Comments</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                    <?php echo @$model->mop->approveComment;?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="" class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Link to MOP</label>
                                                <div class="col-sm-8 detail-view-padding" >
                                                 <a href="<?= $model->mop->linkToMOP;?>" target="_blank"><?php echo $model->mop->linkToMOP;?>
                                                 </a>  
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div> 
                            <!-- </form>  -->                                                      
                        </div>
                        
                        <div class="modal-footer">
                            <?= Html::button('Close', ['class' => 'btn btn-default', 'name' => 'closeView', 'id' => 'closeMopView']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
<?php
$script = <<< JS
$( document ).ready(function() {
       
        $('#closeMopView').click(function() {
             window.parent.closeMopViewModal();
         });  
    });
JS;

$this->registerJs($script);
?>
