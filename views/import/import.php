<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Operations Assistance Portal | Wavenet';

?>  
      <section class="content-header">
        <h1>
          File Import
          <small>Performance</small>
        </h1>
        <ol class="breadcrumb">
          <li><i class="fa fa-dashboard"></i> Dashboard</li>
          <li><i class="fa fa-laptop"></i> Performance</li>
          <li class="active">File Import</li>
        </ol> 
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box">
              <div class="box-body">
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class'=>'form-horizontal']]) ?>
                  <div class="row">
                    <div class="box-body boxpad">
                      <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label for="file" class="col-sm-4 control-label">Upload CSV</label>
                          <div class="col-sm-8">
                            <div class="input-group " name="bulkUpload">
                              <?= $form->field($model, 'file',['options' => ['class' => 'form-control']])->fileInput()->label(false) ?>
                              <span class="input-group-btn">
                                <input type="reset" class="btn btn-warning btn-reset" name="reset" id="reset" value="Reset">
                              </span>
                            </div> <br/>
                            <p class="help-block" style="float: left; width: 100%;">
                              .CSV files only. Multiple entries seperated by new line with format &#91;MSISDN&#93;. <br/> <i><b>eg:</b> <br/> 66891231000 <br/> 66891231001</i>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="box-body boxpad">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="box-footer">
                          <?= Html::submitButton('Upload', ['class' => 'btn btn-primary pull-right btnspace']) ?>
                          <!-- ?= Html::beginForm(Url::to('import/delete'), ['data-method' => 'POST']) ?-->
                          <button type="submit" class="btn btn-default pull-right btnspace" name="clear" id="clear">Clear</button>
                          <!-- ?= Html::endForm() ?-->
                        </div>
                      </div>
                    </div>            
                  </div>
                <?php ActiveForm::end() ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Import File History</h3>
              </div>
              <div class="box-body">
                <form class="form-horizontal" name="form_blacklist_table" id="">
                  <div class="row">
                    <div class="box-body boxpad contSeperator">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            //'filterModel' => $searchModel,
                            'showOnEmpty'=>true,
                            'columns' => [
                              'fileName',
                              'date',
                            ],
                          ]); ?>
                      </div>
                    </div>            
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      
      </section>
      <!-- /.content -->
