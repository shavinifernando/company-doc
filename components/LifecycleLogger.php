<?php

namespace app\components;

use Yii;
use yii\base\Component;

class LifecycleLogger extends Component
{
    /**
     * Log type 1 - Activity log
     */
    public $logType = 1;
    /**
     * Log data
     */
    public $logParams = array();
    /**
     * Current log level
     */
    //public $logLevel = self::DEBUG;
    /**
     * Whether log request come from console application
     */
    public $isConsole = false;
    
    /**
     * Log path
     */
    private $logPath = '/tmp/';
    /**
     * Log Server socket
     */
    private $logSocket = 'localhost:0';
    /**
     * Log file name
     */
    private $logName = '';

    function init()
    {

    }

    /**
     * Prepare log message and write accordingly
     *
     * @param string $message Log message
     * @param integer $logLevel Log level
     */
    function lifecycleLog($message)
    {
        // Initialize log params according log type
        $this->setLogParams();
        //$time = new \DateTime('now', new \DateTimeZone('Asia/Calcutta'));
        //$date= $time->format('Y-m-d H:i:s');
        //$msg = '';

        

        $logFile = $this->logPath . $this->logName;

        if (!is_file($logFile)) {
            $handle = fopen($logFile, "w+");
            fclose($handle);
            chmod($logFile, 777);
            @exec("chmod 777 {$logFile}");
        }
        //if ($logLevel <= $this->logLevel) {
        file_put_contents($logFile, $message, FILE_APPEND);
       // }
    }

    /**
     * Set log data to an array
     */
    private function setLogParams()
    {
        $logParams = $this->logParams[$this->logType];
        $this->logPath = $logParams['logPath'];
        $this->logName =date('Ymd',strtotime("-2 days")).$logParams['logName'];
        $this->logSocket = $logParams['logSocket'];
       // $this->logLevel = $logParams['logLevel'];
        $this->isConsole = $logParams['isConsole'];
    }
}

?>
