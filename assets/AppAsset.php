<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    'themes/default/js/files/bootstrap/css/bootstrap.min.css',
		'themes/default/js/files/dist/css/font-awesome_4.5.0/font-awesome.min.css',
		'themes/default/js/files/dist/css/ionicons_2.0.1/ionicons.min.css',
		'themes/default/js/files/plugins/datatables/dataTables.bootstrap.css',
		//'themes/default/js/files/plugins/datepicker/datepicker3.css',
		//'themes/default/js/files/plugins/timepicker/bootstrap-timepicker.min.css',
		//'themes/default/js/files/plugins/daterangepicker/daterangepicker.css',
		'themes/default/js/files/plugins/iCheck/all.css',
		'themes/default/js/files/plugins/iCheck/square/_all.css',
		//'themes/default/js/files/plugins/select2/select2.min.css',
		'themes/default/js/files/dist/css/AdminLTE.css',
		'themes/default/js/files/dist/css/skins/_all-skins.min.css',
		'themes/default/js/files/toaster/toastr.css',
		//'themes/default/js/files/plugins/datetimepicker/bootstrap-datetimepicker-new.css',
		'themes/default/js/files/plugins/toggle/bootstrap-toggle-2.2.2.min.css',
		'themes/default/css/newtheme.css',
    ];
    public $js = [
		//'themes/default/js/files/plugins/jQuery/jquery-2.2.3.min.js',
		'themes/default/js/files/bootstrap/js/bootstrap.min.js',
		//'themes/default/js/files/plugins/select2/select2.full.min.js',
		'themes/default/js/files/dist/js/moment-2.11.2.min.js',
		//'themes/default/js/files/plugins/daterangepicker/daterangepicker.js',
		//'themes/default/js/files/plugins/datepicker/bootstrap-datepicker.js',
		//'themes/default/js/files/plugins/timepicker/bootstrap-timepicker.min.js',
		//'themes/default/js/files/plugins/datetimepicker/bootstrap-datetimepicker-new.js',
		'themes/default/js/files/plugins/datatables/jquery.dataTables.js',
		'themes/default/js/files/plugins/datatables/dataTables.bootstrap.js',
		'themes/default/js/files/plugins/iCheck/icheck.min.js',
		'themes/default/js/files/plugins/slimScroll/jquery.slimscroll.min.js',
		'themes/default/js/files/plugins/fastclick/fastclick.js',
		'themes/default/js/files/dist/js/app.min.js',
		/*'themes/default/js/files/plugins/chartjs/2.7.0/Chart.min.js',
		'themes/default/js/files/plugins/chartjs/2.7.0/chartjs-plugin-annotation.min.js',
		'themes/default/js/files/plugins/chartjs/2.7.0/Chart.PieceLabel.js',*/
		'themes/default/js/files/plugins/chartjs/2.7.3/chart.bundle.js',
		'themes/default/js/files/plugins/chartjs/2.7.3/utils.js',
		'themes/default/js/files/toaster/toastr.js',
		'themes/default/js/files/plugins/toggle/bootstrap-toggle-2.2.2.min.js',
		'themes/default/js/files/plugins/ZingChart/zingchart.min.js',
		'themes/default/js/files/plugins/radial/radialIndicator.js',
		'themes/default/js/performance_file_import.js',
		
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
