<?php

return [
    'adminEmail' => 'lakshman@globalwavenet.com',
    'user.passwordResetTokenExpire' => 36000,
    'supportEmail' => 'lakshman@globalwavenet.com'
];
