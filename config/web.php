<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
	'@upload' => '@app/web/uploads',
    ],
    'components' => [
        'session' => [
            'class' => 'yii\web\DbSession',
            // 'db' => 'mydb',
            // 'sessionTable' => 'my_session',
            'timeout' => 60*60, // 3 hours
            'usecookies'=>true,
        ],
        /*'cookie' => [

            'name'     => '_identity', 
        
            'httpOnly' => true,
        
            'secure'   => true
        
        ],*/
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'SCRV4HGDJKLNf62DTFIbYpt3CWt0R6QP',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            //'class' => 'listfixer\remember\RememberMe',
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            //'authTimeout' => 3600*24*30,
            'enableSession'=>true,
            /*'identityCookie'=>[
                'name'=>'_identity',
                'httpOnly'=>true,
                'domain'=>'localhost:8080',
            ],*/
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
	    'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.office365.com',
                //'username' => 'prajwal@globalwavenet.com',
                //'password' => 'test.123',
                'username' => 'support@globalwavenet.com',
                'password' => 'Pr!m3%m%c',
		//'password' => 'omnibis@123',
                'port' => '587',
                'encryption' => 'tls',
                 //'streamOptions' => [ 
                 //    'ssl' => [ 
                 //        'allow_self_signed' => true,
                 //        'verify_peer' => false,
                 //       'verify_peer_name' => false,
                 //   ],
                 //],
            ],
            'useFileTransport' => false,
        ],
	/*'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.sendgrid.net',
                'username' => 'kasuni@globalwavenet.com',
                'password' => 'Kasuni@1993',
                'port' => '25',
            ],
            'useFileTransport' => false,
        ],*/
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
	'appLog' => [
            'class' => 'app\components\AppLogger',
            'logType' => 1,
            'logParams' => [
                1 => [
                    'logPath' => dirname(__DIR__) . '/runtime/logs/',
                    'logName' => '-webhook.log',
                    'logLevel' => 2,
                    'logSocket' => '',
                    'isConsole' => false
                ],
            ]
        ],
	'readWriteLog' => [
            'class' => 'app\components\ReadWriteLogger',
            'logType' => 1,
            'logParams' => [
                1 => [
                    'logPath' => dirname(__DIR__) . '/runtime/logs/',
                    'logName' => 'operation.log',
                    'logLevel' => 2,
                    'logSocket' => '',
                    'isConsole' => false
                ],
            ]
        ],
	 'lifecycleLogg' => [
            'class' => 'app\components\LifecycleLogger',
            'logType' => 1,
            'logParams' => [
                1 => [
                    'logPath' => dirname(__DIR__) . '/runtime/logs/ticket_lifecycle/',
                    'logName' => '-lifecycle.log',
                    'logLevel' => 2,
                    'logSocket' => '',
                    'isConsole' => false
                ],
            ]
        ],
        'db' => $db,
	'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@webroot/themes/default/views',
                ],
                'baseUrl' => '@web/themes/default',
                'basePath' => '@app/web/themes/default',
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => false,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'as beforeRequest'=>[
        'class'=>'yii\filters\AccessControl',
        'rules'=>[
            [
                'allow'=>true,
                'actions'=>['login','forgot','resetpassword','updatedatabase','insertactivities','getTicketActivities','lifecycleLog'],
            ],
            [
                'allow'=>true,
                'roles'=>['@'],
            ],
        ],
        'denyCallback'=>function(){
            return Yii::$app->response->redirect(['site/login']);
        },
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    //$config['bootstrap'][] = 'debug';
    //$config['modules']['debug'] = [
        //'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['*'],
    //];
        
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*.*.*.*', '::1'],
    ];
}

return $config;
