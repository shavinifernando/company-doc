<?php

namespace app\commands;

use Yii;
//use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\helpers\BaseArrayHelper;
use app\controllers\ArrayHelper;
use app\components\TicketLifeCycle;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Command;
use app\models\Lifecycle;
use app\models\Previnfo;
use app\models\Tickettiming;
use phpDocumentor\Reflection\Types\Boolean;

class LifecycleController extends Controller
{
   
    public function actionInsertactivities()
    {   
    //Read the json file
        $obj = new TicketLifeCycle();

        $result = json_decode($obj->getTicketActivities());

        $resultArray = (array)($result);

        $count = count($resultArray['activities_data']);
            
        for($i = 0; $i < $count ; $i++){
            $model = new Lifecycle();

            $activityArray = $resultArray['activities_data'][$i];

            $activity = (array)($activityArray);

            $actArray = $activity['activity'];

            $act = (array)($actArray);

            @$note = $act['note'];

            @$noteArray = (array)($note);

            $newFormat = 'Y-m-d H:i:s';
            $date = gmdate($newFormat, strtotime($activity['performed_at'])) . PHP_EOL;
            $date = new \DateTime($date, new \DateTimeZone('UTC'));
            $date->setTimezone(new \DateTimeZone('Asia/Kolkata'));

            $dateNew = $date->format('Y-m-d H:i:s');

            $trimStatus = str_replace(' ','',@$act['status']); // Check the status 

            $model->performedAt = $dateNew; 
            $model->ticketId = $activity['ticket_id']; 
            $model->performerType = $activity['performer_type'];         
            @$model->performerId = $activity['performer_id'];  //check the performer id
            @$model->agentId = $act['agent_id']; 
            @$model->status = $trimStatus;  
            @$model->source = $act['source'];
            @$model->priority = $act['priority'];
            @$model->managementEscalated = $act['Management Escalated'];
            @$model->acknowledger = $act['Acknowledger'];
            @$model->product = $act['Product']; 
            @$model->severity = $act['Severity'];
            @$model->operator = $act['Operator'];
            @$model->issueCategory = $act['Issue Category'];
            @$model->workingTimeOfDay = $act['Working Time of Day'];
            @$model->neutralize = $act['Neutralize'];
            @$model->resolution = $act['Resolution'];
            @$model->groupName = $act['group'];
            @$model->ticketType = $act['ticket_type'];
            @$model->effectiveFirstResponse = $act['Effective First Response'];
            @$model->noteId = $noteArray['id'];
            @$model->noteType = $noteArray['type'];
            $model->save();

            //Inserting to lifecycle table finished here
            $sql = "SELECT p.performerName AS Name FROM ticket_lifecycle left Join performer p on p.performerId=ticket_lifecycle.performerId WHERE performedAt = '$dateNew' && ticketId=$activity[ticket_id]";
            $connection = Yii::$app->getDb();
            $results = $connection->createCommand($sql)->queryAll();

            if($results){
                foreach ($results as $result) {
                    $name=$result['Name'];
                }
            }
            @$model->performerName = $name;

            $statusNew = @$trimStatus;

            $time = $dateNew;

            $tid = $activity['ticket_id'];

            $model->save();
            //Updating agentID in prevInfo
            $isagentchanged=false;
            $newagentname='';
            if($model->agentId!=''){
                $sqlagent1="SELECT performername as agentname from performer WHERE performerid=(Select agentid from ticket_lifecycle where performedAt = '$dateNew' && ticketId=$activity[ticket_id] && agentid!='' limit 1)";
                $connection = Yii::$app->getDb();
                $results = $connection->createCommand($sqlagent1)->queryAll();
                if($results){
                    foreach($results as $result){
                        $newagentname=$result['agentname'];
                    }
                }
                
                $sqlagent="SELECT prevAgent as agentname,prevStatus as prevstatus,prevtime as prevtime from ticket_previnfo where ticketid=$model->ticketId";
                $connection = Yii::$app->getDb();
                $results = $connection->createCommand($sqlagent)->queryAll();
               
                //New agent is created or updated here
                //Only Open tickets should be 
                if($results){
                    foreach ($results as $output) {  
                        $prevAgent = $output['agentname']; 
                        $prevStatus = $output['prevstatus']; 
                        $prevTime = $output['prevtime'];
                        $tid = $activity['ticket_id'];  
                        //$newStatus = $statusNew;
                    }
                    $strStart = $prevTime; 
                    $strEnd   = $time;

                    $dteStart = new \DateTime($strStart); 
                    $dteEnd   = new \DateTime($strEnd);

                    $dteDiff  = $dteStart->diff($dteEnd); 

                    $dteDiffNew = $dteDiff->format("%H:%I:%S");
                    if($prevAgent=='' && $prevStatus=='Open'){
                        print_r('status'.$prevStatus);
                        $connection->createCommand()->insert('ticketflow',['ticketId' => $model->ticketId,'agent' =>$newagentname,'status'=>$prevStatus,'duration'=>$dteDiffNew,'starttime'=>$prevTime])->execute();
                        $isagentchanged=true;
                    }
                    else {
                        if($prevStatus=='Reopen'){
                            print_r('Hello');
                            $connection->createCommand()->insert('ticketflow',['ticketId' => $model->ticketId,'agent' =>$newagentname,'status'=>'Reopen','duration'=>$dteDiffNew,'starttime'=>$prevTime])->execute();
                        }
                        else{
                            $connection->createCommand()->insert('ticketflow',['ticketId' => $model->ticketId,'agent' =>$prevAgent,'status'=>$prevStatus,'duration'=>$dteDiffNew,'starttime'=>$prevTime])->execute();
                        }
                        $isagentchanged=true;
                    }
                    $connection->createCommand()->update('ticket_previnfo',['prevAgent'=>$newagentname,'prevtime'=>$model->performedAt],['ticketid'=>$model->ticketId ])->execute();
                }
                else{

                    $connection->createCommand()->insert('ticket_previnfo',['ticketId' => $model->ticketId,'prevAgent'=>$newagentname,'prevtime'=>$model->performedAt])->execute();
                }

               
                
               }
            //Prajwal end
           
   
            
            $sql1 = "SELECT ticketId,prevAgent AS prevAgent,prevStatus AS prevStatus,prevTime AS prevTime FROM ticket_previnfo WHERE ticketId=$activity[ticket_id]";//
            //$sql1 = "SELECT i.prevStatus AS prevStatus,i.prevTime AS prevTime FROM lifecycle left Join previnfo i on i.ticketId=lifecycle.ticketId WHERE lifecycle.ticketId=$activity[ticket_id] AND prevStatus!='$statusNew'";
            $connection = Yii::$app->getDb(); 

            $outputs = $connection->createCommand($sql1)->queryAll();
            
            //print_r($outputs);
          //If their is a ticket which has the ticketId in the prevInfo // and status has been changed and not null //or the agent has been changed and not null .
            if($outputs){ 
                
                foreach ($outputs as $output) {  
                    $prevAgent = $output['prevAgent']; 
                    $prevStatus = $output['prevStatus']; 
                    $prevTime = $output['prevTime'];
                    $tid = $activity['ticket_id'];  
                    $newStatus = $statusNew; 

                   ///////////////This area must be commented /////////////////////////////////////////////// 
                   if($newStatus!=''  &&  $prevStatus!=$newStatus ) 
                   {
                       $sql="UPDATE ticket_previnfo SET prevStatus='";
                       $sql.=($newStatus!='Open')?$newStatus:'Reopen';
                       $sql.="', prevTime='$dateNew' WHERE ticketId=$tid";
                       $command = $connection->createCommand($sql); 
                       $success = $command->execute(); 

                        if($success && !$isagentchanged){
                            
                            $strStart = $prevTime;
                            $strEnd   = $time;

                            $dteStart = new \DateTime($strStart);
                            $dteEnd   = new \DateTime($strEnd);

                            $dteDiff  = $dteStart->diff($dteEnd);
                            $dteDiffNew = $dteDiff->format("%H:%I:%S");
                            //print_r($prevTime.'|'.$time.'|'.$dteDiff);
                            //echo ('</br>');
                            if( $prevStatus=='Reopen') {
                                $connection->createCommand()->insert('ticketflow',['ticketId' => $tid,'agent' =>$prevAgent,'status'=>'Reopen'/*$prevStatus*/,'duration'=>$dteDiffNew,'starttime'=>$prevTime])->execute();
                            }
                            else
                                $connection->createCommand()->insert('ticketflow',['ticketId' => $tid,'agent' =>$prevAgent,'status'=>$prevStatus,'duration'=>$dteDiffNew,'starttime'=>$prevTime])->execute();
                        }
                    }
//latest
                 }
            }
            //If their is no ticket which has the ticketId in the prevInfo with status changed and not null.
            else {
                if($model->agentId!='' || $statusNew!=''){
                    
                    $connection->createCommand()->insert('ticket_previnfo',['ticketId' => $activity['ticket_id'],'prevTime' =>$dateNew])->execute();
                
                    if($model->agentId!=''){ 
                        $sqlagent1="SELECT performername as agentname from performer WHERE performerid=(Select agentid from ticket_lifecycle where performedAt = '$dateNew' && ticketId=$activity[ticket_id])";
                        $connection = Yii::$app->getDb();
                        $results = $connection->createCommand($sqlagent1)->queryAll();
                        if($results){
                            foreach($results as $result){
                                $newagentname=$result['agentname'];
                            }
                            $connection->createCommand()->update('ticket_previnfo',['prevAgent'=>$newagentname],['ticketid'=>$model->ticketId ])->execute();
                
                        }
                    }
                    print_r($activity['ticket_id'].'|'.$statusNew.'|'.$dateNew);
                    echo ('</br>');
                    $connection->createCommand()->update('ticket_previnfo',['prevStatus'=>$statusNew],['ticketid'=>$model->ticketId ])->execute();
                }
                
            }        
        } //end of for loop
    }
}
