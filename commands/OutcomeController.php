<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Scheduleactivity;
//use app\commands\Yii;
use yii\db\Command;



/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class OutcomeController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    /*public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }*/

    public function actionSendoutcomeremindermail(){

        $model = new Scheduleactivity();

        $sql = "SELECT ticketId,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,outcomeReminder,l1.username AS L1Mail,l2.username AS L2Mail,l3.username AS L3Mail,l1.firstname AS L1Name,l2.firstname AS L2Name,l3.firstname AS L3Name FROM scheduleactivity left Join user l1 on l1.id=scheduleactivity.L1Resource left Join user l2 on l2.id=scheduleactivity.L2Resource left Join user l3 on l3.id=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id WHERE DATEDIFF(CURDATE(),date(scheduleTimeSL)) BETWEEN 3 AND 4 AND outcomeReminder=0";

        $connection = Yii::$app->getDb();

        $results = $connection->createCommand($sql)->queryAll();


        if($results){

            foreach ($results as $result) {

                if($result['L1Resource'] && $result['L1Mail']){
                            
                    //print_r($result['L1Mail']);
                    $sql1 = "SELECT o.operatorName AS Customer,o.region AS Region,o.manager AS Manager FROM createactivity left Join operator o on o.operatorId=createactivity.customer WHERE ticketId=$result[ticketId]";

                    $connection = Yii::$app->getDb();

                    $results1 = $connection->createCommand($sql1)->queryAll();

                    if($results1){

                        foreach ($results1 as $result1) {

                            if($result1['Manager'] && $result1['Region']){

                                //print_r($result1['Manager']);
                                //print_r($result1['Region']);
                                if($result['L2Mail'] && $result['L3Mail']){

                                    $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
                                            ->setCc(array($result['L2Mail'],$result['L3Mail']))
                                            ->setSubject('Add Activity Outcome Reminder')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>Please update the outcome of the activity.<br><br>Ticket:$result[ticketId]<br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Wavenet Support")
                                            ->send();
                                }else{

                                    if($result['L2Mail']){

                                        $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
                                            ->setCc($result['L2Mail'])
                                            ->setSubject('Add Activity Outcome Reminder')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>Please update the outcome of the activity.<br><br>Ticket:$result[ticketId]<br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Wavenet Support")
                                            ->send();

                                    }
                                    else if($result['L3Mail']){

                                        $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
                                            ->setCc($result['L3Mail'])
                                            ->setSubject('Add Activity Outcome Reminder')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>Please update the outcome of the activity.<br><br>Ticket:$result[ticketId]<br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Wavenet Support")
                                            ->send();

                                    }
                                    else{

                                        $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
                                            ->setSubject('Add Activity Outcome Reminder')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>Please update the outcome of the activity.<br><br>Ticket:$result[ticketId]<br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Wavenet Support")
                                            ->send();

                                    }
                                }

                                if($send){

                                    $command = $connection->createCommand("UPDATE scheduleactivity SET outcomeReminder=1 WHERE L1Resource=$result[L1Resource] AND scheduleId=$result[scheduleId]");

                                    $command->execute();

                                }
                            }         
                        }
                    }
                }      
            }
        }
    }  
}

