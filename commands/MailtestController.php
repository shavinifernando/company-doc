<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
//use app\models\Scheduleactivity;
//use app\commands\Yii;
use yii\db\Command;



/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MailtestController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    /*public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }*/

    public function actionSendmail(){
        
        /*Yii::$app->mailer->compose()
                        ->setFrom('kasuni@globalwavenet.com')
                        ->setTo('ramal@globalwavenet.com')     
                        ->setSubject('Activity Preparation Reminder')
                        ->setTextBody('sdddgfghg')
                        ->send();*/
	$mailer = Yii::$app->mailer;
	$message = $mailer->compose()
    		->setTo('kasuni@globalwavenet.com')
    		->setFrom('kasuni@globalwavenet.com')
    		->setSubject('Test 01')
    		->setHtmlBody('HTML message')
    		->setTextBody('Text message')
    		->send();
                            
    }          
       
}

