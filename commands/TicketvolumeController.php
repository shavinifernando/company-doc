<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Ticketinfo;
use app\models\Ticketvolume;
//use app\commands\Yii;
use yii\db\Command;



/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TicketvolumeController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    /*public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }*/

    public function actionInsertticketvolumedata(){

        //$model = new Ticketvolume();

        $curMonth = date('m');

        $curYear = date('Y');

        if($curMonth=='01'){

            $prevYear = $curYear - 1;

            $connection = Yii::$app->getDb();

            $command = $connection->createCommand("INSERT INTO ticketvolume (month,received,resolved,unresolved) VALUES ('$prevYear-12-01',(SELECT COUNT(ticketId) from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m')='$prevYear-12'),(SELECT SUM((status='Resolved')+(status='Closed')) from ticketinfo WHERE DATE_FORMAT(resolveTime,'%Y-%m')='$prevYear-12'),(SELECT SUM((status='Open')+(status='Neutralization Pending')+(status='Pending')+(status='Workaround Provided')+(status='MOP Pending')+(status='RCA Pending')+(status='Activity Pending')+(status='Permanent fix Pending')+(status='Waiting on Third Party')+(status='Waiting on Customer')) from ticketinfo))");

            $command->execute();

        }elseif($curMonth=='11' || $curMonth=='12'){

            $prevMonth = $curMonth - 1;

            $connection = Yii::$app->getDb();

            $command = $connection->createCommand("INSERT INTO ticketvolume (month,received,resolved,unresolved) VALUES ('$curYear-$prevMonth-01',(SELECT COUNT(ticketId) from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m')='$curYear-$prevMonth'),(SELECT SUM((status='Resolved')+(status='Closed')) from ticketinfo WHERE DATE_FORMAT(resolveTime,'%Y-%m')='$curYear-$prevMonth'),(SELECT SUM((status='Open')+(status='Neutralization Pending')+(status='Pending')+(status='Workaround Provided')+(status='MOP Pending')+(status='RCA Pending')+(status='Activity Pending')+(status='Permanent fix Pending')+(status='Waiting on Third Party')+(status='Waiting on Customer')) from ticketinfo ))");

            $command->execute();

        }else{

            $prevMonth = $curMonth - 1;

            $connection = Yii::$app->getDb();

            $command = $connection->createCommand("INSERT INTO ticketvolume (month,received,resolved,unresolved) VALUES ('$curYear-0$prevMonth-01',(SELECT COUNT(ticketId) from ticketinfo WHERE DATE_FORMAT(createTime,'%Y-%m')='$curYear-0$prevMonth'),(SELECT SUM((status='Resolved')+(status='Closed')) from ticketinfo WHERE DATE_FORMAT(resolveTime,'%Y-%m')='$curYear-0$prevMonth'),(SELECT SUM((status='Open')+(status='Neutralization Pending')+(status='Pending')+(status='Workaround Provided')+(status='MOP Pending')+(status='RCA Pending')+(status='Activity Pending')+(status='Permanent fix Pending')+(status='Waiting on Third Party')+(status='Waiting on Customer')) from ticketinfo ))");

            $command->execute();
 
        }
        
    }  
}

