<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mop;

/**
 * MopSearch represents the model behind the search form of `app\models\Mop`.
 */
class MopSearch extends Mop
{
    /**
     * {@inheritdoc}
     */
    public $start;
    public $end;
    public $id;
    public $operatorName;
    public $creator;

    public function rules()
    {
        return [
            [['mopId', 'ticketId'], 'integer'],
            [['openDate', 'MOPCusSharedDate','start','end','id','operatorName','creator', 'MOPCreator', 'MOPName', 'operator', 'issueDescription', 'solution', 'activityScheduleDate', 'linkToMOP'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mop::find();

	//$query->joinWith(['creatorsName']);

        //$query->joinWith(['operatorsName']);


        // add conditions that should always apply here

        $this->load($params);
        //echo $this->end;

        if ((!empty($this->start)) && (!empty($this->end)))
        {
            //echo $this->end;
            $query->where("DATE_FORMAT(openDate,'%Y-%m-%d') BETWEEN '$this->start' AND '$this->end'");


        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['openDate'=>SORT_DESC]],
            'pagination' => [
             
                'pageSize' => 10, 
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'mopId' => $this->mopId,
        //     'ticketId' => $this->ticketId,
        //     'openDate' => $this->openDate,
        //     'MOPCusSharedDate' => $this->MOPCusSharedDate,
        //     'activityScheduleDate' => $this->activityScheduleDate,
        // ]);

        $query->andFilterWhere(['like', 'ticketId', $this->id])
            //->andFilterWhere(['like', 'operator', $this->operatorName])
            //->andFilterWhere(['like', 'MOPCreator', $this->creator]);
	    //->andFilterWhere(['like', 'creator.createrName', $this->creator])
            // ->andFilterWhere(['like', 'operator.operatorName', $this->operatorName]);
	      ->andFilterWhere(['MOPCreator' => $this->creator])
              ->andFilterWhere(['operator' => $this->operatorName]);

        return $dataProvider;
    }
}
