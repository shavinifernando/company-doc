<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "level1".
 *
 * @property int $level1Id
 * @property string $level1Name
 * @property string $level1Mail
 */
class Level1 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'level1';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level1Name', 'level1Mail'], 'required'],
            [['level1Name'], 'string', 'max' => 100],
            [['level1Mail'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'level1Id' => 'Level1 ID',
            'level1Name' => 'Level1 Name',
            'level1Mail' => 'Level1 Mail',
        ];
    }

    public static function getLevel1Resource()
    {
        $level1 = ArrayHelper::map(self::find()->all(), 'level1Id', 'level1Name');

        return $level1;
    }
}
