<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "operator".
 *
 * @property int $operatorId
 * @property string $operatorName
 */
class Operator extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operator';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['operatorName'], 'required'],
            [['operatorName'], 'string', 'max' => 100],
	    [['region', 'manager'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'operatorId' => 'Operator ID',
            'operatorName' => 'Operator Name',
	    'region' => 'Region',
            'manager' => 'Manager',
        ];
    }

    public static function getOperator()
    {

        $operator = ArrayHelper::map(self::find()->all(), 'operatorId', 'operatorName');

        return $operator;
    }

    public static function getOperatorByName()
    {

        $operator = ArrayHelper::map(self::find()->all(), 'operatorName', 'operatorName');

        return $operator;
    }
}
