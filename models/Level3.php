<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "level3".
 *
 * @property int $level3Id
 * @property string $level3Name
 * @property string $level3Mail
 */
class Level3 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'level3';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level3Name', 'level3Mail'], 'required'],
            [['level3Name'], 'string', 'max' => 100],
            [['level3Mail'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'level3Id' => 'Level3 ID',
            'level3Name' => 'Level3 Name',
            'level3Mail' => 'Level3 Mail',
        ];
    }

    public static function getLevel3Resource()
    {

        $level3 = ArrayHelper::map(self::find()->all(), 'level3Id', 'level3Name');

        return $level3;
    }
}
