<?php

namespace app\models;

use Yii;
use app\models\Createactivity;

/**
 * This is the model class for table "scheduleactivity".
 *
 * @property int $scheduleId
 * @property string $scheduleTimeSL
 * @property string $scheduleTimeOther
 * @property string $L1Resource
 * @property string $L2Resource
 * @property string $L3Resource
 * @property string $cusApproval
 * @property string $managerApproval
 * @property string $techApproval
 * @property string $engResponsible
 * @property int $id
 * @property string $cusContactPoint
 * @property int $preparationReminder
 * @property int $closingReminder
 * @property int $outcomeReminder
 * @property int $schedulingAlert
 * @property int $statusAlert
 * @property int $managerApproveAlert
 *
 * @property Createactivity $id0
 */
class Scheduleactivity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'scheduleactivity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['scheduleTimeSL', 'scheduleTimeOther', 'L1Resource', /*'L2Resource', 'L3Resource',*/'cusApproval', /*'managerApproval',*/ 'techApproval', 'engResponsible', 'id', 'cusContactPoint'], 'required'],

            [['scheduleTimeSL', 'scheduleTimeOther'], 'safe'],

            [['id', 'preparationReminder', 'closingReminder', 'outcomeReminder', 'schedulingAlert', 'statusAlert','managerApproveAlert'], 'integer'],

            [['L1Resource', 'L2Resource', 'L3Resource', 'cusApproval', 'managerApproval', 'techApproval', 'engResponsible', 'cusContactPoint'], 'string', 'max' => 200],

            [['id'], 'unique'],

            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Createactivity::className(), 'targetAttribute' => ['id' => 'activityId']],

	    [['cusApproval'], 'validateCusApproval'],

	    [['techApproval'], 'validateTechApproval', 'message' => 'Please approve the MOP from the approver'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'scheduleId' => Yii::t('app', 'Schedule ID'),
            'scheduleTimeSL' => Yii::t('app', 'Schedule Time Sl'),
            'scheduleTimeOther' => Yii::t('app', 'Schedule Time Other'),
            'L1Resource' => Yii::t('app', 'L1 Resource'),
            'L2Resource' => Yii::t('app', 'L2 Resource'),
            'L3Resource' => Yii::t('app', 'L3 Resource'),
            'cusApproval' => Yii::t('app', 'Cus Approval'),
            'managerApproval' => Yii::t('app', 'Manager Approval'),
            'techApproval' => Yii::t('app', 'Tech Approval'),
            'engResponsible' => Yii::t('app', 'Eng Responsible'),
            'id' => Yii::t('app', 'ID'),
            'cusContactPoint' => Yii::t('app', 'Cus Contact Point'),
            'preparationReminder' => Yii::t('app', 'Preparation Reminder'),
            'closingReminder' => Yii::t('app', 'Closing Reminder'),
            'outcomeReminder' => Yii::t('app', 'Outcome Reminder'),
            'schedulingAlert' => Yii::t('app', 'Scheduling Alert'),
            'statusAlert' => Yii::t('app', 'Status Alert'),
	    'managerApproveAlert' => Yii::t('app', 'Manager Approve Alert'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Createactivity::className(), ['activityId' => 'id']);
    }

    public function getLevel1ResourceName()
    {

        return $this->hasOne(User::className(),['id'=>'L1Resource']);

    }

    public function getLevel2ResourceName()
    {

        return $this->hasOne(User::className(),['id'=>'L2Resource']);

    }

    public function getLevel3ResourceName()
    {

        return $this->hasOne(User::className(),['id'=>'L3Resource']);

    }

    public function validateCusApproval($attribute, $params){

        if($this->cusApproval!="Approved"){

            $this->addError($attribute,'This should be Approved');

        }
    }

    public function validateTechApproval($attribute, $params){

        if($this->techApproval!="Approved"){

            $this->addError($attribute,'This should be Approved');

        }
    }

    public function getSendScheduleAlert() {


        $sql = "SELECT ticketId,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,schedulingAlert,l1.username AS L1Mail,l2.username AS L2Mail,l3.username AS L3Mail,l1.firstname AS L1Name,l2.firstname AS L2Name,l3.firstname AS L3Name FROM scheduleactivity left Join user l1 on l1.id=scheduleactivity.L1Resource left Join user l2 on l2.id=scheduleactivity.L2Resource left Join user l3 on l3.id=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id WHERE schedulingAlert=0";

        $connection = Yii::$app->getDb();

        $results = $connection->createCommand($sql)->queryAll();

        if ($results) {

            foreach ($results as $result) {

                if($result['L1Resource'] && $result['L1Mail']){

                    $sql1 = "SELECT o.operatorName AS Customer,o.region AS Region,o.manager AS Manager FROM createactivity left Join operator o on o.operatorId=createactivity.customer WHERE ticketId=$result[ticketId]";

                    $connection = Yii::$app->getDb();

                    $results1 = $connection->createCommand($sql1)->queryAll();

                    if($results1){

                        foreach ($results1 as $result1) {

                            if($result1['Manager'] && $result1['Region']){

                                if($result['L2Mail'] && $result['L3Mail']){

                                    $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
                                            ->setCc(array($result['L2Mail'],$result['L3Mail']))
                                            ->setSubject('Activity Alert')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>You have an activity:$result[activity], scheduled at $result[scheduleTimeSL].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                            ->send();
                                }
                                else{

                                    if($result['L2Mail']){
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setCc($result['L2Mail'])
                                                ->setSubject('Activity Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>You have an activity:$result[activity], scheduled at $result[scheduleTimeSL].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                        else if($result['L3Mail']){
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setCc($result['L3Mail'])
                                                ->setSubject('Activity Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>You have an activity:$result[activity], scheduled at $result[scheduleTimeSL].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                        else{
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setSubject('Activity Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>You have an activity:$result[activity], scheduled at $result[scheduleTimeSL].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                }

                                if($send){

                                    $connection = Yii::$app->getDb();

                                    $command = $connection->createCommand("UPDATE scheduleactivity SET schedulingAlert=1 WHERE L1Resource=$result[L1Resource] AND scheduleId=$result[scheduleId]");

                                    $command->execute();

                                    Yii::$app->readWriteLog->readWriteLog("app\models\Scheduleactivity Scheduled Activity Alert Sent",['Receiver'=>[$result['L1Mail']]],2);
                                                    
                                }
                            }
                        }  
                    }                   
                }
            }
        }
    }

    public function getSendManagerApproveAlert() {


        //$sql = "SELECT activity.ticketId AS ticketId,activity.customer as Customer,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,managerApproveAlert,l1.levelMail AS L1Mail,l2.levelMail AS L2Mail,l3.levelMail AS L3Mail,l1.levelName AS L1Name,l2.levelName AS L2Name,l3.levelName AS L3Name FROM scheduleactivity left Join level l1 on l1.levelId=scheduleactivity.L1Resource left Join level l2 on l2.levelId=scheduleactivity.L2Resource left Join level l3 on l3.levelId=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id WHERE managerApproveAlert=0";
        $sql = "SELECT activity.ticketId AS ticketId,activity.customer as Customer,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,managerApproveAlert,l1.username AS L1Mail,l2.username AS L2Mail,l3.username AS L3Mail,l1.firstname AS L1Name,l2.firstname AS L2Name,l3.firstname AS L3Name FROM scheduleactivity left Join user l1 on l1.id=scheduleactivity.L1Resource left Join user l2 on l2.id=scheduleactivity.L2Resource left Join user l3 on l3.id=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id WHERE managerApproveAlert=0";

        $connection = Yii::$app->getDb();

        $results = $connection->createCommand($sql)->queryAll();

        if ($results) {

            foreach ($results as $result) {

                if($result['Customer']){

                    //$sql1 = "SELECT o.operatorName AS Customer,o.region AS Region,o.manager AS Manager,m.managerMail AS ManagerMail FROM createactivity left Join operator o on o.operatorId=createactivity.customer left Join manager m on m.region=o.region WHERE ticketId=$result[ticketId]";
                    $sql1="SELECT o.operatorName AS Customer,o.region AS Region,o.manager AS Manager,u.username AS ManagerMail FROM createactivity left Join operator o on o.operatorId=createactivity.customer left Join manager m on m.region=o.region left join user u on m.user_id=u.id WHERE ticketId=$result[ticketId]";
                    $connection = Yii::$app->getDb();

                    $results1 = $connection->createCommand($sql1)->queryAll();

                    if($results1){

                        foreach ($results1 as $result1) {

                            if($result1['Manager'] && $result1['Region']){

                                    $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result1['ManagerMail'])
                                            ->setSubject('Manager Approval Alert')
                                            ->setHtmlBody("Hi $result1[Manager],<br><br>You have an activity:$result[activity], scheduled at $result[scheduleTimeSL] to be approved.<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                            ->send();

                                if($send){

                                    $connection = Yii::$app->getDb();

                                    $command = $connection->createCommand("UPDATE scheduleactivity SET managerApproveAlert=1 WHERE L1Resource=$result[L1Resource] AND scheduleId=$result[scheduleId]");

                                    $command->execute();

                                    Yii::$app->readWriteLog->readWriteLog("app\models\Scheduleactivity Manager Approval Request Alert Sent",['Receiver'=>[$result1['ManagerMail']]],2);
                                      
                                }
                            }
                        }  
                    }                   
                }
            }
        }
    }
}
