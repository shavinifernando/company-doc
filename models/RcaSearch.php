<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rca;
use yii\db\ActiveQuery;

/**
 * RcaSearch represents the model behind the search form of `app\models\Rca`.
 */
class RcaSearch extends Rca
{
    /**
     * {@inheritdoc}
     */

    public $start;
    public $end;
    public $id;
    public $operatorName;
    public $creator;

    public function rules()
    {
        return [
            [['rcaId', 'ticketId'], 'integer'],
            [['openDate', 'sharedDate','start','end','id','operatorName','creator', 'RCAName', 'operator', 'issueDescription', 'RCACreator', 'linkToRCA'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
       $query = Rca::find();

       //$query->joinWith(['creatorsName']);

       //$query->joinWith(['operatorsName']);

        $this->load($params);
        //echo $this->end;

        if ((!empty($this->start)) && (!empty($this->end)))
        {
            //echo $this->end;
            $query->where("DATE_FORMAT(openDate,'%Y-%m-%d') BETWEEN '$this->start' AND '$this->end'");


        }

            
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	    'sort'=> ['defaultOrder' => ['openDate'=>SORT_DESC]],
            'pagination' => [
             
                'pageSize' => 10, 
            ],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        /*$query->andFilterWhere([
            'ticketId' => $this->ticketId,
            'operator' => $this->operator,
            'RCACreator' => $this->RCACreator,
        ]);*/

        $query->andFilterWhere(['like', 'ticketId', $this->id])
            //->andFilterWhere(['like', 'operator', $this->operatorName])
            //->andFilterWhere(['like', 'RCACreator', $this->creator]);
	    //->andFilterWhere(['like', 'creator.createrName', $this->creator])
            //->andFilterWhere(['like', 'operator.operatorName', $this->operatorName]);
	      ->andFilterWhere(['RCACreator' => $this->creator])
              ->andFilterWhere(['operator' => $this->operatorName]);

        return $dataProvider;

    }
}
