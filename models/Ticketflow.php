<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ticketflow".
 *
 * @property int $id
 * @property string $ticketID
 * @property string $status
 * @property string $duration
 * @property string $agent
 * @property string $starttime
 */
class Ticketflow extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ticketflow';
    }
    const SCENARIO_DIST = 'scn_dist';

    public $date;
    public $start;
    public $end;
    public $region;
    public $severity;
    public $ticket_id;
    public $filter; 
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start','end','region'], 'required', 'on' => [self::SCENARIO_DIST]],
            [['ticketID', 'status', 'duration', 'agent'], 'required'],
            [['duration', 'starttime'], 'safe'],
            [['ticketID', 'status', 'agent'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticketID' => 'Ticket I D',
            'status' => 'Status',
            'duration' => 'Duration',
            'agent' => 'Agent',
            'starttime' => 'Starttime',
        ];
    }
}
