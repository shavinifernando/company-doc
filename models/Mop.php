<?php

namespace app\models;

use Yii;
use app\models\Scheduleactivity;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mop".
 *
 * @property int $mopId
 * @property int $ticketId
 * @property string $openDate
 * @property string $MOPCusSharedDate
 * @property string $MOPCreator
 * @property string $MOPName
 * @property string $operator
 * @property string $issueDescription
 * @property string $solution
 * @property string $linkToMOP
 * @property string $techApproval
 * @property string $techApprovedBy
 * @property int $approvalStatus
 * @property int $approveAlert
 *
 * @property Createactivity[] $createactivities
 */
class Mop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mop';
    }

    /**
     * {@inheritdoc}
     */
    const SCENARIO_SRH_STR = 'srh_str';
    const SCENARIO_SRH_EN = 'srh_en';
    const SCENARIO_SRH_ID = 'srh_id';
    const SCENARIO_SRH_OPR = 'srh_opr';
    const SCENARIO_SRH_CTR = 'srh_ctr';

    const SCENARIO_DIST_MOP = 'dist_mop';

    public $start;
    public $end;
    public $id;
    public $operatorName;
    public $creator;

    public $startDate;
    public $endDate;
    public $filter;

    public function rules()
    {
        return [

            [['start'], 'required', 'on' => [self::SCENARIO_SRH_STR]],

            [['end'], 'required', 'on' => [self::SCENARIO_SRH_EN]],

            [['id'], 'required', 'on' => [self::SCENARIO_SRH_ID]],

            [['operatorName'], 'required', 'on' => [self::SCENARIO_SRH_OPR]],

           // [['creator'], 'required', 'on' => [self::SCENARIO_SRH_CTR]],

	    [['startDate','endDate','filter'], 'required', 'on' => [self::SCENARIO_DIST_MOP]],

            [['ticketId', 'openDate', 'MOPCreator', 'MOPName', 'operator', 'issueDescription', 'solution', 'linkToMOP','techApprovedBy'], 'required'],

            [['ticketId', 'approvalStatus','approveAlert'], 'integer'],

            [['openDate', 'MOPCusSharedDate'], 'safe'],

            [['MOPCreator'], 'string', 'max' => 100],

            //[['MOPName', 'linkToMOP'], 'string', 'max' => 500],
	    [['MOPName', 'linkToMOP', 'approveComment'], 'string', 'max' => 500],

            [['operator'], 'string', 'max' => 200],

            [['issueDescription', 'solution'], 'string', 'max' => 1000],

            [['techApproval', 'techApprovedBy'], 'string', 'max' => 50],

            [['ticketId', 'MOPName'], 'unique', 'targetAttribute' => ['ticketId', 'MOPName']],

	    ['ticketId','match','pattern'=>'/^((?!(0))[0-9]{4,5}$)/'],

	    ['MOPName','match','pattern'=>'/^(MOP)(\_)([A-Z][(A-Z)(a-z)\-]+)(\_)((?!(0))[0-9]{4,5})(\_)([A-Z][a-z\-]+)(\_)(([0-2][0-9]|(3)[0-1])(((0)[0-9])|((1)[0-2]))\d{4})(\_)([v][1-9]$)/'],

	    /*['MOPName','match','pattern'=>'/^(MOP)(\_)(Tigo-Bolivia|Millicom-Tanzania|Hutch-SriLanka|Etisalat-SriLanka|Telecom-SriLanka|Airtel-SriLanka|Ooredoo-Maldives|IAF:ALU-India|TATA-India|DTAC-Thailand|TrueMove-Thailand|Millicom-Ghana|Vini-Tahiti|Millicom-Chad|Millicom-Senagal|Safaricom-Kenya|Vodafone-New Zealand|Tigo-Honduras|Intelli-Australia|Tigo-Colombia|Cellcard-Cambodia|Tigo-ElSalvador|Tigo-Paraguay|Smart-Cambodia|Laogw-Laos)(\_)((?!(0))[0-9]{4,5})(\_)([A-Z][a-z\-]+)(\_)(([0-2][0-9]|(3)[0-1])(((0)[0-9])|((1)[0-2]))\d{4})(\_)([v][1-9]$)/'],*/

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'mopId' => 'Mop ID',
            'ticketId' => 'Ticket ID',
            'openDate' => 'Open Date',
            'MOPCusSharedDate' => 'Mopcus Shared Date',
            'MOPCreator' => 'Mopcreator',
            'MOPName' => 'Mopname',
            'operator' => 'Operator',
            'issueDescription' => 'Issue Description',
            'solution' => 'Solution',
            'linkToMOP' => 'Link To Mop',
            'techApproval' => 'Tech Approval',
            'techApprovedBy' => 'Tech Approved By',
            'approvalStatus' => 'Approval Status',
            'approveComment' => 'Approve Comment',
	    'approveAlert' => 'Approve Alert',
        ];
    }

    public function getCreateactivity(){

        return $this->hasOne(Createactivity::className(), ['id_mop' => 'mopId']);

    }

    public function getOperatorsName(){

        return $this->hasOne(Operator::className(),['operatorId'=>'operator']);

    }

    public static function getMopName()
    {

        $mopName = ArrayHelper::map(self::find()->all(), 'mopId', 'MOPName');

        return $mopName;
    }

    public function getCreatorsName(){

        //return $this->hasOne(Creator::className(),['createrId'=>'MOPCreator']);
        return $this->hasOne(User::className(),['id'=>'MOPCreator']);

    }
	
	public function getApproverName(){

        return $this->hasOne(User::className(),['id'=>'techApprovedBy']);

    }

    public static function getName($ticketId){

        $mop = self::find()->where(['ticketId'=>$ticketId])->all();

        return $mop;
    }

    public static function getTicket(){

        $ticket = self::find()->select(['ticketId'])->groupBy(['ticketId'])->all();

        return ArrayHelper::map($ticket,'ticketId', 'ticketId');
    }
	
	public function getSendApprovalAlert() {


        //Pxx$sql = "SELECT mopId,MOPName AS MOPName,linkToMOP,approvalStatus, c.createrMail AS ApproverMail,c.createrName AS ApproverName,a.createrName AS creatorName FROM mop left Join creator c on c.createrId=mop.techApprovedBy left Join creator a on a.createrId=mop.MOPCreator WHERE approvalStatus=0";
        $sql = "SELECT mopId,MOPName AS MOPName,linkToMOP,approvalStatus, c.username AS ApproverMail,c.firstname AS ApproverName,a.firstname AS creatorName FROM mop left Join user c on c.id=mop.techApprovedBy left Join user a on a.id=mop.MOPCreator WHERE approvalStatus=0";
        $connection = Yii::$app->getDb();

        $results = $connection->createCommand($sql)->queryAll();

        if ($results) {

        foreach ($results as $result) {

                if($result['ApproverName'] && $result['ApproverMail']){

                        $send = Yii::$app->mailer->compose()
                                    ->setFrom('support@globalwavenet.com')
                                    ->setTo($result['ApproverMail'])
                                    ->setSubject('MOP Approval Alert')
                                    ->setHtmlBody("Hi $result[ApproverName],<br><br>You have an MOP:$result[MOPName] for approval.<br><br>MOP Creator:$result[creatorName]<br>Link to MOP:$result[linkToMOP]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                    ->send();

                        if($send){

                            $connection = Yii::$app->getDb();

                            $command = $connection->createCommand("UPDATE mop SET approvalStatus=1 WHERE mopId=$result[mopId]");

                            $command->execute();

			    Yii::$app->readWriteLog->readWriteLog("app\models\MOP Approval Mail Sent",['Receiver'=>$result['ApproverMail']],2);

                         
                        }

                       
                }
            }
        }
    }

    public function getSendApprovalStatusAlert() {


        //Pxx $sql = "SELECT mopId,MOPName AS MOPName,approveComment as Comment,approvalStatus,techApproval, c.createrMail AS CreaterMail,c.createrName AS CreaterName,a.createrMail AS ApproverMail,a.createrName AS ApproverName FROM mop left Join creator c on c.createrId=mop.MOPCreator left Join creator a on a.createrId=mop.techApprovedBy WHERE approveAlert=0";
         $sql = "SELECT mopId,MOPName AS MOPName,approveComment as Comment,approvalStatus,techApproval,c.username AS CreaterMail,c.firstname AS CreaterName,a.username AS ApproverMail,a.firstname AS ApproverName FROM mop left Join user c on c.id=mop.MOPCreator left Join user a on a.id=mop.techApprovedBy WHERE approveAlert=0";
        $connection = Yii::$app->getDb();

        $results = $connection->createCommand($sql)->queryAll();

        if ($results) {

        foreach ($results as $result) {

                if($result['techApproval']=="Approved" OR $result['techApproval']=="Rejected"){

                        $send = Yii::$app->mailer->compose()
                                    ->setFrom('support@globalwavenet.com')
                                    ->setTo(array($result['CreaterMail'],$result['ApproverMail']))
                                    ->setSubject('MOP Approval Alert')
                                    ->setHtmlBody("Hi $result[CreaterName],<br><br>The MOP:$result[MOPName],has been $result[techApproval] by $result[ApproverName].<br><br>Comment:$result[Comment]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                    ->send();

                        if($send){

                            $connection = Yii::$app->getDb();

                            $command = $connection->createCommand("UPDATE mop SET approveAlert=1 WHERE mopId=$result[mopId]");

                            $command->execute();

			    Yii::$app->readWriteLog->readWriteLog("app\models\MOP MOP Approval Alert Sent",['Receiver'=>[$result['CreaterMail'],$result['ApproverMail']]],2);

                         
                        }

                       
                }
            }
        }
    }
}
