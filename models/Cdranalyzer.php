<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
/**
 * Cdr analyzer is the model for Cdr controller.
 *
 *
 * @property string $Cdr
 */
class Cdranalyzer extends Model
{
    public $cdra;
    public $deployment;
    public $cdrtype;
    public $file;
   
    /**
     * @return array the validation rules.
     */
    public function rules()
    {

        return [
            [['cdra'],'string'],
            [['deployment','cdrtype'], 'required'],
            [['file'], 'file', 'extensions' => 'txt'],

            // username and password are both required
            
        ];
    }

   
      
    
}