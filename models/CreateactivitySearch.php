<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Createactivity;

/**
 * CreateactivitySearch represents the model behind the search form of `app\models\Createactivity`.
 */
class CreateactivitySearch extends Createactivity
{
    /**
     * {@inheritdoc}
     */
    public $start;
    public $end;
    public $id;
    public $customerName;
    public $resourceName;
    public $approval;
    public $status;

    public function rules()
    {
        return [
            [['activityId', 'ticketId'], 'integer'],
            [['activityBy', 'customer', 'activity', 'start','end','id','customerName','resourceName','approval','status','comments', 'mopName', 'activityLevel', 'product', 'patch', 'description', 'outcome', 'output','activityStatus'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Createactivity::find();

        $query->joinWith(['scheduleactivity','scheduleactivity.level1ResourceName']);

        $this->load($params);

        // add conditions that should always apply here
        if ((!empty($this->start)) && (!empty($this->end)))
        {
            //echo $this->end;

           $query->where("DATE_FORMAT(scheduleactivity.scheduleTimeSL,'%Y-%m-%d') BETWEEN '$this->start' AND '$this->end'");

        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['activityId'=>SORT_DESC]],
            'pagination' => [
             
                'pageSize' => 10,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        //$query->andFilterWhere([
        //    'activityId' => $this->activityId,
        //   'ticketId' => $this->ticketId,
        //]);

        $query->andFilterWhere(['like', 'ticketId', $this->id])
            //->andFilterWhere(['like', 'operator.operatorName', $this->customerName])
            //->andFilterWhere(['like', 'level.levelName', $this->resourceName])
	    ->andFilterWhere(['customer' => $this->customerName])
	    ->andFilterWhere(['scheduleactivity.L1Resource' => $this->resourceName])
	    ->andFilterWhere(['scheduleactivity.managerApproval' => $this->approval])
            ->andFilterWhere(['activityStatus' => $this->status]);

        return $dataProvider;
    }
}
