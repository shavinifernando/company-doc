<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ticketinfo".
 *
 * @property int $ticketId
 * @property string $subject
 * @property string $status
 * @property string $priority
 * @property string $source
 * @property string $type
 * @property string $agent
 * @property string $groupName
 * @property string $createTime
 * @property string $dueTime
 * @property string $resolveTime
 * @property string $closeTime
 * @property string $lastUpdateTime
 * @property string $initialResponseTime
 * @property string $timeTracked
 * @property string $firstResponseTime
 * @property string $resolutionTime
 * @property int $agentInt
 * @property int $cusInt
 * @property string $resolutionstatus
 * @property string $responseStatus
 * @property string $tags
 * @property string $surveyResult
 * @property string $internalAgent
 * @property string $product
 * @property string $internalGroup
 * @property string $effectiveFirstResponse
 * @property string $mgmtEscalated
 * @property string $acknowledger
 * @property string $productName
 * @property string $severity
 * @property string $operator
 * @property string $issueCategory
 * @property string $workingTimeOfDay
 * @property string $neutralize
 * @property string $resolution
 * @property string $fullName
 * @property string $title
 * @property string $email
 * @property string $workPhone
 * @property string $mobilePhone
 * @property string $twitterId
 * @property string $timezone
 * @property string $language
 * @property string $uniqueExternal
 * @property string $facebook
 * @property string $contactId
 * @property string $companyName
 * @property string $companyDomain
 * @property string $healthScore
 * @property string $accountTier
 * @property string $AMCPeriodStart
 * @property string $AMCPeriodEnd
 * @property string $renewalDate
 * @property string $SLAStatus
 * @property string $SLACusContact
 * @property string $industry
 * @property string $resolutionSummary
 * @property string $escalationSummary
 * @property int $outage_time
 */
class Ticketinfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ticketinfo';
    }

    const SCENARIO_DIST = 'scn_dist';

    public $date;
    public $start;
    public $end;
    public $region;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['date','start','end','region'], 'required', 'on' => [self::SCENARIO_DIST]],

            [['ticketId'], 'required'],

            [['ticketId', 'agentInt', 'cusInt'], 'integer'],

            [['createTime', 'dueTime', 'resolveTime', 'closeTime', 'lastUpdateTime', 'initialResponseTime', 'timeTracked', 'firstResponseTime', 'resolutionTime', 'AMCPeriodStart', 'AMCPeriodEnd', 'renewalDate'], 'safe'],

            [['subject', 'internalAgent', 'internalGroup', 'fullName', 'email', 'industry'], 'string', 'max' => 200],

            [['status', 'priority', 'type', 'groupName', 'product', 'effectiveFirstResponse', 'mgmtEscalated', 'productName', 'severity', 'operator', 'issueCategory', 'workingTimeOfDay', 'neutralize', 'resolution', 'workPhone', 'mobilePhone', 'twitterId', 'timezone', 'language', 'facebook', 'contactId', 'companyDomain', 'healthScore', 'accountTier', 'SLAStatus', 'resolutionSummary', 'escalationSummary'], 'string', 'max' => 50],

            [['source', 'resolutionstatus', 'responseStatus', 'tags', 'surveyResult', 'title'], 'string', 'max' => 20],

            [['agent', 'acknowledger', 'uniqueExternal', 'companyName', 'SLACusContact'], 'string', 'max' => 100],

            [['ticketId'], 'unique'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ticketId' => 'Ticket ID',
            'subject' => 'Subject',
            'status' => 'Status',
            'priority' => 'Priority',
            'source' => 'Source',
            'type' => 'Type',
            'agent' => 'Agent',
            'groupName' => 'Group Name',
            'createTime' => 'Create Time',
            'dueTime' => 'Due Time',
            'resolveTime' => 'Resolve Time',
            'closeTime' => 'Close Time',
            'lastUpdateTime' => 'Last Update Time',
            'initialResponseTime' => 'Initial Response Time',
            'timeTracked' => 'Time Tracked',
            'firstResponseTime' => 'First Response Time',
            'resolutionTime' => 'Resolution Time',
            'agentInt' => 'Agent Int',
            'cusInt' => 'Cus Int',
            'resolutionstatus' => 'Resolutionstatus',
            'responseStatus' => 'Response Status',
            'tags' => 'Tags',
            'surveyResult' => 'Survey Result',
            'internalAgent' => 'Internal Agent',
            'product' => 'Product',
            'internalGroup' => 'Internal Group',
            'effectiveFirstResponse' => 'Effective First Response',
            'mgmtEscalated' => 'Mgmt Escalated',
            'acknowledger' => 'Acknowledger',
            'productName' => 'Product Name',
            'severity' => 'Severity',
            'operator' => 'Operator',
            'issueCategory' => 'Issue Category',
            'workingTimeOfDay' => 'Working Time Of Day',
            'neutralize' => 'Neutralize',
            'resolution' => 'Resolution',
            'fullName' => 'Full Name',
            'title' => 'Title',
            'email' => 'Email',
            'workPhone' => 'Work Phone',
            'mobilePhone' => 'Mobile Phone',
            'twitterId' => 'Twitter ID',
            'timezone' => 'Timezone',
            'language' => 'Language',
            'uniqueExternal' => 'Unique External',
            'facebook' => 'Facebook',
            'contactId' => 'Contact ID',
            'companyName' => 'Company Name',
            'companyDomain' => 'Company Domain',
            'healthScore' => 'Health Score',
            'accountTier' => 'Account Tier',
            'AMCPeriodStart' => 'Amcperiod Start',
            'AMCPeriodEnd' => 'Amcperiod End',
            'renewalDate' => 'Renewal Date',
            'SLAStatus' => 'Slastatus',
            'SLACusContact' => 'Slacus Contact',
            'industry' => 'Industry',
	    'resolutionSummary' => 'Resolution Summary',
            'escalationSummary' => 'Escalation Summary',
	    'outage_time' =>'Outage Time',
        ];
    }
}
