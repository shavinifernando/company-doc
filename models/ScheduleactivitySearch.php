<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Scheduleactivity;

/**
 * ScheduleactivitySearch represents the model behind the search form of `app\models\Scheduleactivity`.
 */
class ScheduleactivitySearch extends Scheduleactivity
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['scheduleId', 'id'], 'integer'],
            [['scheduleTimeSL', 'scheduleTimeOther', 'L1Resource', 'L2Resource', 'L3Resource', 'cusApproval', 'managerApproval', 'techApproval', 'engResponsible', 'cusContactPoint'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Scheduleactivity::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'scheduleId' => $this->scheduleId,
            'scheduleTimeSL' => $this->scheduleTimeSL,
            'scheduleTimeOther' => $this->scheduleTimeOther,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'L1Resource', $this->L1Resource])
            ->andFilterWhere(['like', 'L2Resource', $this->L2Resource])
            ->andFilterWhere(['like', 'L3Resource', $this->L3Resource])
            ->andFilterWhere(['like', 'cusApproval', $this->cusApproval])
            ->andFilterWhere(['like', 'managerApproval', $this->managerApproval])
            ->andFilterWhere(['like', 'techApproval', $this->techApproval])
            ->andFilterWhere(['like', 'engResponsible', $this->engResponsible])
            ->andFilterWhere(['like', 'activityStatus', $this->activityStatus])
            ->andFilterWhere(['like', 'cusContactPoint', $this->cusContactPoint]);

        return $dataProvider;
    }
}
