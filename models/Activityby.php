<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "activityby".
 *
 * @property int $activityById
 * @property string $activityByName
 */
class Activityby extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'activityby';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activityByName'], 'required'],
            [['activityByName'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'activityById' => 'Activity By ID',
            'activityByName' => 'Activity By Name',
        ];
    }

    public static function getActivityby()
    {

        $activityBy = ArrayHelper::map(self::find()->all(), 'activityById', 'activityByName');

        return $activityBy;
    }
}
