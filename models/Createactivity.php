<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "createactivity".
 *
 * @property int $activityId
 * @property int $ticketId
 * @property string $activityBy
 * @property string $customer
 * @property string $activity
 * @property string $comments
 * @property string $mopName
 * @property string $activityLevel
 * @property string $product
 * @property string $patch
 * @property string $description
 * @property string $outcome
 * @property string $output
 * @property string $activityStatus
 * @property int $id_mop
 * @property string $managerApproval
 * @property string $approvalComment
 *
 * @property Mop $mop
 * @property Scheduleactivity $scheduleactivity
 */
class Createactivity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    const SCENARIO_SRH_ACT = 'srh_act';

    const SCENARIO_DIST_ACT = 'dist_act';

    public $start;
    public $end;
    public $id;
    public $customerName;
    public $resourceName;
    public $approval;
    public $status;

    public $startDate;
    public $endDate;
    public $filter;

    public $date;

    public static function tableName()
    {
        return 'createactivity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

	    [['startDate','endDate','filter','date'], 'required', 'on' => [self::SCENARIO_DIST_ACT]],

            [['start','end','id','customerName','resourceName','approval','status'], 'required', 'on' => [self::SCENARIO_SRH_ACT]],

            [['ticketId', 'activityBy', 'customer', 'activity', 'activityLevel', 'product', 'description'], 'required'],

            [['ticketId', 'id_mop'], 'integer'],

            [['activityBy', 'customer', 'mopName', 'product', 'activityStatus','managerApproval'], 'string', 'max' => 200],

            [['activity', 'comments', 'description', 'outcome', 'output'], 'string', 'max' => 1000],

            [['activityLevel', 'patch'], 'string', 'max' => 100],

            [['ticketId', 'mopName'], 'unique', 'targetAttribute' => ['ticketId', 'mopName']],

            [['id_mop'], 'unique'],

	    [['approvalComment'], 'string', 'max' => 500],

	    ['ticketId','match','pattern'=>'/^((?!(0))[0-9]{4,5}$)/'],

	    ['activity','match','pattern'=>'/^[A-Z a-z 0-9.]{1,100}$/'],

            ['mopName', 'required', 'when' => function ($model) {
                return $model->activityBy == '1';
            }, 'whenClient' => "function (attribute, value) {
                    //alert($('#createactivity-activityby').val());
                    return $('#createactivity-activityby').val() == '1';
            }"],

            [['id_mop'], 'exist', 'skipOnError' => true, 'targetClass' => Mop::className(), 'targetAttribute' => ['id_mop' => 'mopId']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'activityId' => 'Activity ID',
            'ticketId' => 'Ticket ID',
            'activityBy' => 'Activity By',
            'customer' => 'Customer',
            'activity' => 'Activity',
            'comments' => 'Comments',
            'mopName' => 'Mop Name',
            'activityLevel' => 'Activity Level',
            'product' => 'Product',
            'patch' => 'Patch',
            'description' => 'Description',
            'outcome' => 'Outcome',
            'output' => 'Output',
            'activityStatus' => 'Activity Status',
            'id_mop' => 'Id Mop',
	    'managerApproval' => 'Manager Approval',
            'approvalComment' => 'Approval Comment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	public function getMop(){

        return $this->hasOne(Mop::className(), ['mopId' => 'id_mop']);
    }
	
    public function getScheduleactivity(){
        
        return $this->hasOne(Scheduleactivity::className(), ['id' => 'activityId']);

    }

    public function getOperatorsName(){

        return $this->hasOne(Operator::className(),['operatorId'=>'customer']);

    }
	
	public function getMopsName(){

        return $this->hasOne(Mop::className(),['mopId'=>'mopName']);

    }

    public function getActivitybyNames(){

        return $this->hasOne(Activityby::className(),['activityById'=>'activityBy']);

    }

    public function sendStatusUpdateAlert() {


        $sql = "SELECT ticketId,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,statusAlert,l1.username AS L1Mail,l2.username AS L2Mail,l3.username AS L3Mail,l1.firstname AS L1Name,l2.firstname AS L2Name,l3.firstname AS L3Name,activity.activityStatus as activityStatus FROM scheduleactivity left Join user l1 on l1.id=scheduleactivity.L1Resource left Join user l2 on l2.id=scheduleactivity.L2Resource left Join user l3 on l3.id=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id WHERE statusAlert=0";
        //$sql = "SELECT ticketId,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,statusAlert,l1.levelMail AS L1Mail,l2.levelMail AS L2Mail,l3.levelMail AS L3Mail,l1.levelName AS L1Name,l2.levelName AS L2Name,l3.levelName AS L3Name,activity.activityStatus as activityStatus FROM scheduleactivity left Join level l1 on l1.levelId=scheduleactivity.L1Resource left Join level l2 on l2.levelId=scheduleactivity.L2Resource left Join level l3 on l3.levelId=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id WHERE statusAlert=0";

        $connection = Yii::$app->getDb();

        $results = $connection->createCommand($sql)->queryAll();

        if ($results) {

            foreach ($results as $result) {

            $status = $result['activityStatus'];

            if ($status=="Successfully Completed" or $status=="Partially Successful" or $status=="Failed") {

                if($result['L1Resource'] && $result['L1Mail']){
                            
                    $sql1 = "SELECT o.operatorName AS Customer,o.region AS Region,o.manager AS Manager,u.username AS ManagerMail FROM createactivity left Join operator o on o.operatorId=createactivity.customer left Join manager m on m.region=o.region left join user u on m.user_id=u.id WHERE ticketId=$result[ticketId]";

                    $connection = Yii::$app->getDb();

                    $results1 = $connection->createCommand($sql1)->queryAll();

                    if($results1){

                        foreach ($results1 as $result1) {

                            if($result1['Manager'] && $result1['Region']){

                                if($result['L2Mail'] && $result['L3Mail']){

                                    $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setCc(array($result['L2Mail'],$result['L3Mail'],$result1['ManagerMail']))
                                                ->setSubject('Activity Completion Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity], scheduled at $result[scheduleTimeSL] was completed with the status of $result[activityStatus].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                }
                                else{

                                    if($result['L2Mail']){
                                        $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
                                            ->setCc(array($result['L2Mail'],$result1['ManagerMail']))
                                            ->setSubject('Activity Completion Alert')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity], scheduled at $result[scheduleTimeSL] was completed with the status of $result[activityStatus].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                            ->send();
                                    }
                                    else if($result['L3Mail']){
                                        $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
                                            ->setCc(array($result['L3Mail'],$result1['ManagerMail']))
                                            ->setSubject('Activity Completion Alert')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity], scheduled at $result[scheduleTimeSL] was completed with the status of $result[activityStatus].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                            ->send();
                                }
                                    else{
                                        $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
					    ->setCc($result1['ManagerMail'])
                                            ->setSubject('Activity Completion Alert')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity], scheduled at $result[scheduleTimeSL] was completed with the status of $result[activityStatus].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                            ->send();
                                    }
                                }

                                if($send){

                                    $connection = Yii::$app->getDb();

                                    $command = $connection->createCommand("UPDATE scheduleactivity SET statusAlert=1 WHERE L1Resource=$result[L1Resource] AND scheduleId=$result[scheduleId]");

                                    $command->execute();

				    Yii::$app->readWriteLog->readWriteLog("app\models\Createactivity Activity Completion Alert Sent",['Receiver'=>[$result['L1Mail'],$result['L2Mail'],$result['L3Mail'],$result1['ManagerMail']]],2);
                                }
                            }
                        }
                    }                       
                } 
            }
        }
    }
}

    public function sendActivityCancelAlert() {


        $sql = "SELECT ticketId,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,statusAlert,l1.username AS L1Mail,l2.username AS L2Mail,l3.username AS L3Mail,l1.firstname AS L1Name,l2.firstname AS L2Name,l3.firstname AS L3Name,activity.activityStatus as activityStatus FROM scheduleactivity left Join user l1 on l1.id=scheduleactivity.L1Resource left Join user l2 on l2.id=scheduleactivity.L2Resource left Join user l3 on l3.id=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id";
        //$sql = "SELECT ticketId,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,statusAlert,l1.levelMail AS L1Mail,l2.levelMail AS L2Mail,l3.levelMail AS L3Mail,l1.levelName AS L1Name,l2.levelName AS L2Name,l3.levelName AS L3Name,activity.activityStatus as activityStatus FROM scheduleactivity left Join level l1 on l1.levelId=scheduleactivity.L1Resource left Join level l2 on l2.levelId=scheduleactivity.L2Resource left Join level l3 on l3.levelId=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id";

        $connection = Yii::$app->getDb();

        $results = $connection->createCommand($sql)->queryAll();

        if ($results) {

            foreach ($results as $result) {

            $status = $result['activityStatus'];

                if ($status=="To Be Scheduled") {

                    if($result['L1Resource'] && $result['L1Mail']){
                            
                        $sql1 = "SELECT o.operatorName AS Customer,o.region AS Region,o.manager AS Manager,u.username AS ManagerMail FROM createactivity left Join operator o on o.operatorId=createactivity.customer left Join manager m on m.region=o.region left join user u on m.user_id=u.id WHERE ticketId=$result[ticketId]";

                        $connection = Yii::$app->getDb();

                        $results1 = $connection->createCommand($sql1)->queryAll();

                        if($results1){

                            foreach ($results1 as $result1) {

                                if($result1['Manager'] && $result1['Region']){

                                    if($result['L2Mail'] && $result['L3Mail']){

                                    $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
                                            ->setCc(array($result['L2Mail'],$result['L3Mail'],$result1['ManagerMail']))
                                            ->setSubject('Activity Cancelling Alert')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity], scheduled at $result[scheduleTimeSL] was cancelled.<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                            ->send();
                                    }
                                    else{

                                        if($result['L2Mail']){
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setCc(array($result['L2Mail'],$result1['ManagerMail']))
                                                ->setSubject('Activity Cancelling Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity], scheduled at $result[scheduleTimeSL] was cancelled.<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                        else if($result['L3Mail']){
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setCc(array($result['L3Mail'],$result1['ManagerMail']))
                                                ->setSubject('Activity Cancelling Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity], scheduled at $result[scheduleTimeSL] was cancelled.<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                        else{
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
						->setCc($result1['ManagerMail'])
                                                ->setSubject('Activity Cancelling Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity], scheduled at $result[scheduleTimeSL] was cancelled.<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                    }
				    if($send){

				    	Yii::$app->readWriteLog->readWriteLog("app\models\Createactivity Activity Cancelling Alert Sent",['Receiver'=>[$result['L1Mail'],$result['L2Mail'],$result['L3Mail']]],2);
									
				    }
                                }
                            }
                        }       
                    }
                }
            }
        }
    }

    public function sendActivityRescheduleAlert() {


        //$sql = "SELECT ticketId,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,statusAlert,l1.levelMail AS L1Mail,l2.levelMail AS L2Mail,l3.levelMail AS L3Mail,l1.levelName AS L1Name,l2.levelName AS L2Name,l3.levelName AS L3Name,activity.activityStatus as activityStatus FROM scheduleactivity left Join level l1 on l1.levelId=scheduleactivity.L1Resource left Join level l2 on l2.levelId=scheduleactivity.L2Resource left Join level l3 on l3.levelId=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id";
        $sql = "SELECT ticketId,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,statusAlert,l1.username AS L1Mail,l2.username AS L2Mail,l3.username AS L3Mail,l1.firstname AS L1Name,l2.firstname AS L2Name,l3.firstname AS L3Name,activity.activityStatus as activityStatus FROM scheduleactivity left Join user l1 on l1.id=scheduleactivity.L1Resource left Join user l2 on l2.id=scheduleactivity.L2Resource left Join user l3 on l3.id=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id";

        $connection = Yii::$app->getDb();

        $results = $connection->createCommand($sql)->queryAll();

        if ($results) {

            foreach ($results as $result) {

            $status = $result['activityStatus'];

                if ($status=="Scheduled") {

                    if($result['L1Resource'] && $result['L1Mail']){
                            
                        $sql1 = "SELECT o.operatorName AS Customer,o.region AS Region,o.manager AS Manager,u.username AS ManagerMail FROM createactivity left Join operator o on o.operatorId=createactivity.customer left Join manager m on m.region=o.region left join user u on m.user_id=u.id WHERE ticketId=$result[ticketId]";

                        $connection = Yii::$app->getDb();

                        $results1 = $connection->createCommand($sql1)->queryAll();

                        if($results1){

                            foreach ($results1 as $result1) {

                                if($result1['Manager'] && $result1['Region']){

                                    if($result['L2Mail'] && $result['L3Mail']){

                                    $send = Yii::$app->mailer->compose()
                                            ->setFrom('support@globalwavenet.com')
                                            ->setTo($result['L1Mail'])
                                            ->setCc(array($result['L2Mail'],$result['L3Mail'],$result1['ManagerMail']))
                                            ->setSubject('Activity Rescheduling Alert')
                                            ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity] is rescheduled to $result[scheduleTimeSL].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                            ->send();
                                    }
                                    else{

                                        if($result['L2Mail']){
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setCc(array($result['L2Mail'],$result1['ManagerMail']))
                                                ->setSubject('Activity Rescheduling Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity] is rescheduled to $result[scheduleTimeSL].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                        else if($result['L3Mail']){
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setCc(array($result['L3Mail'],$result1['ManagerMail']))
                                                ->setSubject('Activity Rescheduling Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity] is rescheduled to $result[scheduleTimeSL].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                        else{
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
						->setCc($result1['ManagerMail'])
                                                ->setSubject('Activity Rescheduling Alert')
                                                ->setHtmlBody("Hi $result[L1Name],<br><br>The activity:$result[activity] is rescheduled to $result[scheduleTimeSL].<br><br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                    }

				    if($send){

				    Yii::$app->readWriteLog->readWriteLog("app\models\Createactivity Activity Rescheduling Alert Sent",['Receiver'=>[$result['L1Mail'],$result['L2Mail'],$result['L3Mail']]],2);
									
				    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function sendCompletionStatusUpdate() {


        $sql = "SELECT ticketId,scheduleId AS scheduleId ,scheduleTimeSL AS scheduleTimeSL,scheduleTimeOther AS scheduleTimeOther,L1Resource,L2Resource,L3Resource,activity.activity as activity,activity.prvStatus as prvStatus,statusAlert,l1.username AS L1Mail,l2.username AS L2Mail,l3.username AS L3Mail,l1.firstname AS L1Name,l2.firstname AS L2Name,l3.firstname AS L3Name,activity.activityStatus as activityStatus FROM scheduleactivity left Join user l1 on l1.id=scheduleactivity.L1Resource left Join user l2 on l2.id=scheduleactivity.L2Resource left Join user l3 on l3.id=scheduleactivity.L3Resource left Join createactivity activity on activity.activityId=scheduleactivity.id WHERE statusAlert=0";

        $connection = Yii::$app->getDb();

        $results = $connection->createCommand($sql)->queryAll();

        if ($results) {

            foreach ($results as $result) {

            $status = $result['activityStatus'];

                if ($status=="Failed" or $status=="Successfully Completed" or $status=="Partially Successful") {

                    if($result['L1Resource'] && $result['L1Mail']){

                        $sql1 = "SELECT o.operatorName AS Customer,o.region AS Region,o.manager AS Manager,u.username AS ManagerMail FROM createactivity left Join operator o on o.operatorId=createactivity.customer left Join manager m on m.region=o.region left join user u on m.user_id=u.id WHERE ticketId=$result[ticketId]";

                        $connection = Yii::$app->getDb();

                        $results1 = $connection->createCommand($sql1)->queryAll();

                        if($results1){

                            foreach ($results1 as $result1) {

                                if($result1['Manager'] && $result1['Region']){

                                    if($result['L2Mail'] && $result['L3Mail']){

                                        $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
						->setCc(array($result['L2Mail'],$result['L3Mail'],$result1['ManagerMail']))
                                                ->setSubject('Activity Completion Status Update Alert')
                                                ->setHtmlBody("Hi,<br><br>You have changed the status of the completed activity:$result[activity] to $result[activityStatus].<br><br>Previous Status:$result[prvStatus]<br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                    }
                                    else{

                                        if($result['L2Mail']){
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setCc(array($result['L2Mail'],$result1['ManagerMail']))
                                                ->setSubject('Activity Completion Status Update Alert')
                                                ->setHtmlBody("Hi,<br><br>You have changed the status of the completed activity:$result[activity] to $result[activityStatus].<br><br>Previous Status:$result[prvStatus]<br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                        else if($result['L3Mail']){
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
                                                ->setCc(array($result['L3Mail'],$result1['ManagerMail']))
                                                ->setSubject('Activity Completion Status Update Alert')
                                                ->setHtmlBody("Hi,<br><br>You have changed the status of the completed activity:$result[activity] to $result[activityStatus].<br><br>Previous Status:$result[prvStatus]<br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                        else{
                                            $send = Yii::$app->mailer->compose()
                                                ->setFrom('support@globalwavenet.com')
                                                ->setTo($result['L1Mail'])
						->setCc($result1['ManagerMail'])
                                                ->setSubject('Activity Completion Status Update Alert')
                                                ->setHtmlBody("Hi,<br><br>You have changed the status of the completed activity:$result[activity] to $result[activityStatus].<br><br>Previous Status:$result[prvStatus]<br>L1 Resource:$result[L1Name]<br>L2 Resource:$result[L2Name]<br>L3 Resource:$result[L3Name]<br>Manager:$result1[Manager]<br><br>Regards,<br>Operation Assistance<br>Wavenet Support")
                                                ->send();
                                        }
                                    }

                                    if($send){

                                        $connection = Yii::$app->getDb();

                                        $command = $connection->createCommand("UPDATE scheduleactivity SET statusAlert=1 WHERE L1Resource=$result[L1Resource] AND scheduleId=$result[scheduleId]");

                                        $command->execute();

					Yii::$app->readWriteLog->readWriteLog("app\models\Createactivity Activity Completion Status Update Alert Sent",['Receiver'=>[$result['L1Mail'],$result['L2Mail'],$result['L3Mail']]],2);

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
