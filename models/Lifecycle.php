<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lifecycle".
 *
 * @property int $id
 * @property string $performedAt
 * @property int $ticketId
 * @property string $performerType
 * @property string $performerId
 * @property string $agentId
 * @property string $performerName
 * @property string $status
 * @property int $source
 * @property int $priority
 * @property string $managementEscalated
 * @property string $acknowledger
 * @property string $product
 * @property string $severity
 * @property string $operator
 * @property string $issueCategory
 * @property string $workingTimeOfDay
 * @property string $neutralize
 * @property string $resolution
 * @property string $groupName
 * @property string $ticketType
 * @property string $effectiveFirstResponse
 * @property string $noteId
 * @property int $noteType
 */
class Lifecycle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ticket_lifecycle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['performedAt', 'ticketId', 'performerType'], 'required'],
            [['performedAt'], 'safe'],
            [['ticketId', 'performerId','agentId', 'source', 'priority', 'noteId', 'noteType'], 'integer'],
            [['performerType', 'status', 'managementEscalated', 'acknowledger', 'severity', 'groupName', 'ticketType', 'effectiveFirstResponse'], 'string', 'max' => 50],
            [['performerName'], 'string', 'max' => 500],
            [['product', 'operator', 'issueCategory', 'workingTimeOfDay', 'neutralize', 'resolution'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'performedAt' => 'Performed At',
            'ticketId' => 'Ticket ID',
            'performerType' => 'Performer Type',
            'performerId' => 'Performer ID',
            'agent_id' => 'Agent ID',
            'performerName' => 'Performer Name',
            'status' => 'Status',
            'source' => 'Source',
            'priority' => 'Priority',
            'managementEscalated' => 'Management Escalated',
            'acknowledger' => 'Acknowledger',
            'product' => 'Product',
            'severity' => 'Severity',
            'operator' => 'Operator',
            'issueCategory' => 'Issue Category',
            'workingTimeOfDay' => 'Working Time Of Day',
            'neutralize' => 'Neutralize',
            'resolution' => 'Resolution',
            'groupName' => 'Group Name',
            'ticketType' => 'Ticket Type',
            'effectiveFirstResponse' => 'Effective First Response',
            'noteId' => 'Note ID',
            'noteType' => 'Note Type',
        ];
    }
}
