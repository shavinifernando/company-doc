<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "previnfo".
 *
 * @property int $ticketId
 * @property string $prevAgent
 * @property string $prevStatus
 * @property string $prevTime
 */
class Previnfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ticket_previnfo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ticketId', 'prevAgent'], 'required'],
            [['ticketId'], 'integer'],
            [['prevTime'], 'safe'],
            [['prevAgent'], 'string', 'max' => 100],
            [['prevStatus'], 'string', 'max' => 50],
            [['ticketId'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ticketId' => 'Ticket ID',
            'prevAgent' => 'Prev Agent',
            'prevStatus' => 'Prev Status',
            'prevTime' => 'Prev Time',
        ];
    }
}
