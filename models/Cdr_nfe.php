<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cdr_nfe".
 *
 * @property int $id
 * @property int $NETid
 * @property int $NET
 * @property string $NET_des
 * @property int $NERid
 * @property int $NER
 * @property string $NER_des
 * @property int $NFCid
 * @property int $NFC
 * @property string $NFC_des
 */
class Cdr_nfe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cdr_nfe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NETid', 'NET', 'NERid', 'NER', 'NFCid', 'NFC'], 'integer'],
            [['NET', 'NET_des', 'NER', 'NER_des', 'NFC', 'NFC_des'], 'required'],
            [['NET_des', 'NER_des', 'NFC_des'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'NETid' => 'NETid',
            'NET' => 'NET',
            'NET_des' => 'NET_des',
            'NERid' => 'NERid',
            'NER' => 'NER',
            'NER_des' => 'NER_des',
            'NFCid' => 'NFCid',
            'NFC' => 'NFC',
            'NFC_des' => 'NFC_des',
        ];
    }
}
