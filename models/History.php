<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fileHistory".
 *
 * @property int $id
 * @property string $fileName
 * @property string $date
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fileHistory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fileName'], 'required'],
            [['date'], 'safe'],
            [['fileName'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fileName' => 'File Name',
            'date' => 'Date',
        ];
    }
}
