<?php

namespace app\models;

use Yii;
use app\models\Ticketinfo;
use app\models\RcaSearch;

/**
 * This is the model class for table "rca".
 *
 * @property int $rcaId
 * @property int $ticketId
 * @property string $openDate
 * @property string $sharedDate
 * @property string $RCAName
 * @property string $operator
 * @property string $issueDescription
 * @property string $RCACreator
 * @property string $linkToRCA
 */
class Rca extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    const SCENARIO_SRH_STR = 'srh_str';
    const SCENARIO_SRH_EN = 'srh_en';
    const SCENARIO_SRH_ID = 'srh_id';
    const SCENARIO_SRH_OPR = 'srh_opr';
    const SCENARIO_SRH_CTR = 'srh_ctr';

    const SCENARIO_DIST_RCA = 'dist_rca';

    public $start;
    public $end;
    public $id;
    public $operatorName;
    public $creator;
    public $startDate;
    public $endDate;
    public $filter;

    public static function tableName()
    {
        return 'rca';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['start'], 'required', 'on' => [self::SCENARIO_SRH_STR]],

            [['end'], 'required', 'on' => [self::SCENARIO_SRH_EN]],

            [['id'], 'required', 'on' => [self::SCENARIO_SRH_ID]],

            [['operatorName'], 'required', 'on' => [self::SCENARIO_SRH_OPR]],

           // [['creator'], 'required', 'on' => [self::SCENARIO_SRH_CTR]],

	    [['startDate','endDate','filter'], 'required', 'on' => [self::SCENARIO_DIST_RCA]],

            [['ticketId', 'openDate', 'RCAName', 'operator', 'issueDescription', /*'RCACreator',*/ 'linkToRCA'], 'required'],

            [['ticketId'], 'integer'],

            //[['ticketId'], 'validateticketId'],

            [['openDate', 'sharedDate'], 'safe'],

            [['RCAName', 'issueDescription', 'linkToRCA'], 'string', 'max' => 500],

            [['operator'], 'string', 'max' => 200],

            [['RCACreator'], 'string', 'max' => 100],

	    [['ticketId', 'RCAName'], 'unique', 'targetAttribute' => ['ticketId', 'RCAName']],

	    ['ticketId','match','pattern'=>'/^((?!(0))[0-9]{4,5}$)/'],

	    ['RCAName','match','pattern'=>'/^(RCA)(\_)([A-Z][(A-Z)(a-z)\-]+)(\_)((?!(0))[0-9]{4,5})(\_)([A-Z][a-z\-]+)(\_)(([0-2][0-9]|(3)[0-1])(((0)[0-9])|((1)[0-2]))\d{4})(\_)([v][1-9]$)/'],

	    /*['RCAName','match','pattern'=>'/^(RCA)(\_)(Tigo-Bolivia|Millicom-Tanzania|Hutch-SriLanka|Etisalat-SriLanka|Telecom-SriLanka|Airtel-SriLanka|Ooredoo-Maldives|IAF:ALU-India|TATA-India|DTAC-Thailand|TrueMove-Thailand|Millicom-Ghana|Vini-Tahiti|Millicom-Chad|Millicom-Senagal|Safaricom-Kenya|Vodafone-New Zealand|Tigo-Honduras|Intelli-Australia|Tigo-Colombia|Cellcard-Cambodia|Tigo-ElSalvador|Tigo-Paraguay|Smart-Cambodia|Laogw-Laos)(\_)((?!(0))[0-9]{4,5})(\_)([A-Z][a-z\-]+)(\_)(([0-2][0-9]|(3)[0-1])(((0)[0-9])|((1)[0-2]))\d{4})(\_)([v][1-9]$)/'],*/

        ];
    }

    

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rcaId' => 'Rca ID',
            'ticketId' => 'Ticket ID',
            'openDate' => 'Open Date',
            'sharedDate' => 'Shared Date',
            'RCAName' => 'Rcaname',
            'operator' => 'Operator',
            'issueDescription' => 'Issue Description',
            'RCACreator' => 'Rcacreator',
            'linkToRCA' => 'Link To Rca',
        ];
    }

    public function getOperatorsName(){
        
        return $this->hasOne(Operator::className(),['operatorId'=>'operator']);

    }

    public function getCreatorsName(){
         return $this->hasOne(User::className(),['id'=>'RCACreator']);
       //Pxx return $this->hasOne(Creator::className(),['createrId'=>'RCACreator']);

    }

    
}
