<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "level2".
 *
 * @property int $level2Id
 * @property string $level2Name
 * @property string $level2Mail
 */
class Level2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'level2';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level2Name', 'level2Mail'], 'required'],
            [['level2Name'], 'string', 'max' => 100],
            [['level2Mail'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'level2Id' => 'Level2 ID',
            'level2Name' => 'Level2 Name',
            'level2Mail' => 'Level2 Mail',
        ];
    }

    public static function getLevel2Resource()
    {
        $level2 = ArrayHelper::map(self::find()->all(), 'level2Id', 'level2Name');

        return $level2;
    }
}
