<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "cdr_division".
 *
 * @property int $id
 * @property int $depid
 * @property string $deployment
 * @property int $cdrid
 * @property string $cdrtype
 * @property string $mode
 * @property string $f1
 * @property string $f2
 * @property string $f3
 * @property string $f4
 * @property string $f5
 * @property string $f6
 * @property string $f7
 * @property string $f8
 * @property string $f9
 * @property string $f10
 * @property string $f11
 * @property string $f12
 * @property string $f13
 * @property string $f14
 * @property string $f15
 * @property string $f16
 * @property string $f17
 * @property string $f18
 * @property string $f19
 * @property string $f20
 * @property string $f21
 * @property string $f22
 * @property string $f23
 * @property string $f24
 * @property string $f25
 * @property string $f26
 * @property string $f27
 * @property string $f28
 * @property string $f29
 * @property string $f30
 * @property string $f31
 * @property string $f32
 * @property string $f33
 * @property string $f34
 * @property string $f35
 * @property string $f36
 * @property string $f37
 * @property string $f38
 * @property string $f39
 * @property string $f40
 * @property string $f41
 * @property string $f42
 * @property string $f43
 * @property string $f44
 * @property string $f45
 * @property string $f46
 * @property string $f47
 * @property string $f48
 * @property string $f49
 * @property string $f50
 * @property string $f51
 * @property string $f52
 * @property string $f53
 * @property string $f54
 * @property string $f55
 * @property string $f56
 * @property string $f57
 * @property string $f58
 * @property string $f59
 * @property string $f60
 * @property string $f61
 * @property string $f62
 * @property string $f63
 * @property string $f64
 * @property string $f65
 * @property string $f66
 * @property string $f67
 * @property string $f68
 * @property string $f69
 * @property string $f70
 */
class CdrDivision extends \yii\db\ActiveRecord
{
    public $cdr;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cdr_division';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['depid', 'deployment', 'cdrid', 'cdrtype', 'mode', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10', 'f11', 'f12', 'f13', 'f14', 'f15', 'f16', 'f17', 'f18', 'f19', 'f20', 'f21', 'f22', 'f23', 'f24', 'f25', 'f26', 'f27', 'f28', 'f29', 'f30', 'f31', 'f32', 'f33', 'f34', 'f35', 'f36', 'f37', 'f38', 'f39', 'f40', 'f41', 'f42', 'f43', 'f44', 'f45', 'f46', 'f47', 'f48', 'f49', 'f50', 'f51', 'f52', 'f53', 'f54', 'f55', 'f56', 'f57', 'f58', 'f59', 'f60', 'f61', 'f62', 'f63', 'f64', 'f65', 'f66', 'f67', 'f68', 'f69', 'f70'], 'required'],
            [['depid', 'cdrid'], 'integer'],
            [['deployment', 'cdrtype'], 'string', 'max' => 200],
            [['mode'], 'string', 'max' => 1],
            [['f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10', 'f11', 'f12', 'f13', 'f14', 'f15', 'f16', 'f17', 'f18', 'f19', 'f20', 'f21', 'f22', 'f23', 'f24', 'f25', 'f26', 'f27', 'f28', 'f29', 'f30', 'f31', 'f32', 'f33', 'f34', 'f35', 'f36', 'f37', 'f38', 'f39', 'f40', 'f41', 'f42', 'f43', 'f44', 'f45', 'f46', 'f47', 'f48', 'f49', 'f50', 'f51', 'f52', 'f53', 'f54', 'f55', 'f56', 'f57', 'f58', 'f59', 'f60', 'f61', 'f62', 'f63', 'f64', 'f65', 'f66', 'f67', 'f68', 'f69', 'f70'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'depid' => 'Depid',
            'deployment' => 'Deployment',
            'cdrid' => 'Cdrid',
            'cdrtype' => 'Cdrtype',
            'mode' => 'Mode',
            'f1' => 'F1',
            'f2' => 'F2',
            'f3' => 'F3',
            'f4' => 'F4',
            'f5' => 'F5',
            'f6' => 'F6',
            'f7' => 'F7',
            'f8' => 'F8',
            'f9' => 'F9',
            'f10' => 'F10',
            'f11' => 'F11',
            'f12' => 'F12',
            'f13' => 'F13',
            'f14' => 'F14',
            'f15' => 'F15',
            'f16' => 'F16',
            'f17' => 'F17',
            'f18' => 'F18',
            'f19' => 'F19',
            'f20' => 'F20',
            'f21' => 'F21',
            'f22' => 'F22',
            'f23' => 'F23',
            'f24' => 'F24',
            'f25' => 'F25',
            'f26' => 'F26',
            'f27' => 'F27',
            'f28' => 'F28',
            'f29' => 'F29',
            'f30' => 'F30',
            'f31' => 'F31',
            'f32' => 'F32',
            'f33' => 'F33',
            'f34' => 'F34',
            'f35' => 'F35',
            'f36' => 'F36',
            'f37' => 'F37',
            'f38' => 'F38',
            'f39' => 'F39',
            'f40' => 'F40',
            'f41' => 'F41',
            'f42' => 'F42',
            'f43' => 'F43',
            'f44' => 'F44',
            'f45' => 'F45',
            'f46' => 'F46',
            'f47' => 'F47',
            'f48' => 'F48',
            'f49' => 'F49',
            'f50' => 'F50',
            'f51' => 'F51',
            'f52' => 'F52',
            'f53' => 'F53',
            'f54' => 'F54',
            'f55' => 'F55',
            'f56' => 'F56',
            'f57' => 'F57',
            'f58' => 'F58',
            'f59' => 'F59',
            'f60' => 'F60',
            'f61' => 'F61',
            'f62' => 'F62',
            'f63' => 'F63',
            'f64' => 'F64',
            'f65' => 'F65',
            'f66' => 'F66',
            'f67' => 'F67',
            'f68' => 'F68',
            'f69' => 'F69',
            'f70' => 'F70',
        ];
    }
    public static function getDeployment()
    {
     
        $deployment = ArrayHelper::map(self::find()->all(), 'deployment', 'deployment');
        return $deployment;  
    }

    public static function getCdrtype()
    {
     
        $cdrtype = ArrayHelper::map(self::find()->all(), 'cdrtype', 'cdrtype');

        return $cdrtype;  
    }
}
