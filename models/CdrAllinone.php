<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cdr_allinone".
 *
 * @property int $id
 * @property int $Value
 * @property string $event
 * @property int $Code
 */
class CdrAllinone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cdr_allinone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Value', 'event', 'Code'], 'required'],
            [['Value', 'Code'], 'integer'],
            [['event'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Value' => 'Value',
            'event' => 'event',
            'Code' => 'Code',
        ];
    }
}
