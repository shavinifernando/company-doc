<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $role
 * @property string $level
 * @property string $status
 * @property string $password
 * @property string $fullname
 * @property string $firstname
 * @property string $lastname
 * @property string $department
 * @property string $authKey
 * @property string $accessToken
 * @property string $password_reset_token
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }
    public static function primaryKey()
    {
        return ['id'];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'username', 'password','firstname','lastname', 'department'], 'required'],
            [['id','level'], 'integer'],
            [['role'], 'string', 'max' => 20],
            [['username', 'password', 'department'], 'string', 'max' => 50],
            [['firstname','lastname','authKey', 'accessToken','password_reset_token'], 'string', 'max' => 100],
            ['username','unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'department' => 'Department',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
        ];
    }
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
        //return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
        //throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
        /*foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }
        return null;*/
    }
    public function generateAccessToken($expireInSeconds)
    {
        $this->access_token = Yii::$app->security->generateRandomString() . '_' . (time() + $expireInSeconds);
    }

    public function isAccessTokenValid($token)
    {
        if (!empty($this->access_token)) {
            $timestamp = (int) substr($this->access_token, strrpos($this->access_token, '_') + 1);
            return $timestamp > time();
        }
        return false;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
       /*foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                print_r($user);
                return new static($user);
            }
        }
        return null;
        /*n 
        $users = self::find()->where([ "username" => $username ])->one();
        print_r($users);
   
    return new static($users);*/
    //print_r(static::findOne(['username' => $username]));
    return static::findOne(['username' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }
//Pxx
    public static function getUser()
    {
        $creator = ArrayHelper::map(self::find()->all(), 'id', 'firstname');

        return $creator;
        
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
    

    //Password Recovery

        public static function findByPasswordResetToken($token)
        {
            if (!static::isPasswordResetTokenValid($token)) {
                return null;
            }
            return static::findOne([
                'password_reset_token' => $token,
            ]);
        }
        public static function isPasswordResetTokenValid($token)
        {
            if (empty($token)) {
                return false;
            }
            $timestamp = (int) substr($token, strrpos($token, '_') + 1);
            $expire = Yii::$app->params['user.passwordResetTokenExpire'];
            return $timestamp + $expire >= time();
        }
        public function generatePasswordResetToken()
        {
            $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        }
        public function removePasswordResetToken()
        {
            $this->password_reset_token = null;
        }
        public function setPassword($password)
        {
            $this->password =md5($password);
        }
        public function setStatus($status)
        {
            $this->status =$status;
        }
        public function beforeSave($insert)
        {
            if (parent::beforeSave($insert)) {
                if ($this->isNewRecord) {
                    $this->authKey = \Yii::$app->security->generateRandomString();
                }
                return true;
            }
            return false;
        }

        public function sendEmail($usermail)
        {
            /* @var $user User */
            $user = User::findOne([
                'username' => $usermail,
            ]);
    
            if (!$user) {
                return false;
            }
    
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
                if (!$user->save()) {
                    return false;
                }
            }

            return Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'welcome-html', 'text' => 'welcome-text'],
                    ['user' => $user]
                )
                ->setFrom('support@globalwavenet.com')
                //->setFrom('prajwal@globalwavenet.com')
                ->setTo($this->username)
                ->setSubject('Welcome to OPERATIONSassistance| Wavenet')
                ->send();
        }

        public static function getLevelInfo($level_no)
        {
            
            $level = ArrayHelper::map(self::find()->where(['level'=>$level_no])->all(), 'id', 'firstname');
            return $level;

        }
     
}
