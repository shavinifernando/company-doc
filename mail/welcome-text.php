<?php
 
  use yii\helpers\Html;
  $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/resetpassword', 'token' => $user->password_reset_token]);
  ?>
<p>Welcome to Operation Assistant |Wavenet</p>
    <p>Username: <?= Html::encode($user->username) ?>,</p>
    <p>Please create your own password to login by following the link below.</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    