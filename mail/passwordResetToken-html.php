<?php
 
use yii\helpers\Html;
 
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/resetpassword', 'token' => $user->password_reset_token]);
?>
 <?php
  $first_name=$user->firstname;
  /*$name = trim($user->fullname);
  $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
  $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );*/
 ?>
 
<div class="password-reset">
    <p>Hi <?= Html::encode($first_name) ?>,</p>
    <p>Please follow the link below to reset your password:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>Regards,</p>
    <p>Wavenet Support</p>
</div>
