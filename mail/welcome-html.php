<?php
 
  use yii\helpers\Html;
  $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/resetpassword', 'token' => $user->password_reset_token]);
  $first_name=$user->firstname;
  /*$name = trim($user->fullname);
  $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
  $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );*/
 ?>
 
<div class="password-reset">
    <p>Hi <?= Html::encode($first_name) ?>,</p>
    <p>Welcome to Operation Assistant |Wavenet</p>
    <p>Username: <?= Html::encode($user->username) ?>,</p>
    <p>Please create your own password to login by following the link below.</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>Regards,</p>
    <p>Wavenet Support</p>
</div>