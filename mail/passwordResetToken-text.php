<?php
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/resetpassword', 'token' => $user->password_reset_token]);
?>
Hi <?= $user->username ?>,
Follow the link below to reset your password:
<?= $resetLink ?>